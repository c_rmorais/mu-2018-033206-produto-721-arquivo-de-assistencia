/*
-- Ricardo Oliveira (Nova Tendencia) - 30/07/2018
-- Descrição: Inclusão das procedures de extração de assistências do Produto 721
-- objetivo: Incluir na tabela de parametrização as novas procedures de extração
-- Base: assistencia_uss_db
*/

-- VARIAVEIS DE CONTROLE
DECLARE @TOTAL_REGISTRO INT
DECLARE @TOTAL_REGISTRO_AFETADOS INT
DECLARE @MENSAGEM VARCHAR(500)

-- VARIAVEIS DE DESENVOLVIMENTO
DECLARE @DT_INCLUSAO SMALLDATETIME
DECLARE @DT_ALTERACAO SMALLDATETIME
DECLARE @USUARIO VARCHAR(20)

SELECT @DT_INCLUSAO = GETDATE()
	  ,@DT_ALTERACAO = NULL
      ,@USUARIO = 'MU-2018-033206'

-- TOTALIZADOR DE REGISTRO
SELECT @TOTAL_REGISTRO_AFETADOS = 0
	 , @TOTAL_REGISTRO = 2

       INSERT INTO ASSISTENCIA_USS_DB.DBO.TP_ASSISTENCIA_TB 
	   (TP_ASSISTENCIA_ID, 
	    NOME, 
		DT_INCLUSAO, 
		DT_ALTERACAO, 
		USUARIO, 
		TP_ASSISTENCIA_SAIDA_ID
		)
		
	   SELECT 
			  TIPO_ASSISTENCIA_TB.TIPO_ASSISTENCIA_ID AS TP_ASSISTENCIA_ID, 
			  TXT_TIPO_ASSISTENCIA AS NOME, 
			  @dt_inclusao AS DT_INCLUSAO, 
			  @dt_alteracao AS DT_ALTERACAO, 
			  @usuario AS USUARIO,
			  NULL
		 FROM ASSISTENCIA_DB.DBO.TIPO_ASSISTENCIA_TB TIPO_ASSISTENCIA_TB WITH(NOLOCK)
   INNER JOIN ASSISTENCIA_DB.DBO.PLANO_ASSISTENCIA_TB PLANO_ASSISTENCIA_TB WITH(NOLOCK)
           ON TIPO_ASSISTENCIA_TB.TIPO_ASSISTENCIA_ID = PLANO_ASSISTENCIA_TB.TIPO_ASSISTENCIA_ID
        WHERE PLANO_ASSISTENCIA_TB.PLANO_ASSISTENCIA_ID IN (4073,4074)

SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT
	  
	  
-- Verificação
		SELECT @total_registro_afetados

		SELECT @total_registro

IF ( @@SERVERNAME = 'SISAB003' /* AB */  AND @total_registro <> @total_registro_afetados )
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) 
	              + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

	RAISERROR (@mensagem, 16, 1)
END
GO	     
