/*
-- Ricardo Oliveira (Nova Tendencia) - 30/07/2018
-- Descrição: Inclusão das procedures de extração de assistências do Produto 721
-- objetivo: Incluir na tabela de parametrização as novas procedures de extração
-- Base: assistencia_uss_db
*/

-- variaveis de controle
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

-- variaveis de desenvolvimento
DECLARE @dt_inclusao SMALLDATETIME
DECLARE @dt_alteracao SMALLDATETIME
DECLARE @usuario varchar(20)

SELECT @dt_inclusao = GETDATE()
	  ,@dt_alteracao = NULL
      ,@usuario = 'MU-2018-033206'

-- totalizador de registro
SELECT @total_registro_afetados = 0
	 , @total_registro = 2

       INSERT INTO ASSISTENCIA_USS_DB.DBO.SUBRAMO_TP_ASSISTENCIA_TB
	               (ramo_id,
				    subramo_id,
					dt_inicio_vigencia_sbr,
					dt_inicio_vigencia_ass,
					tp_assistencia_id,
					dt_fim_vigencia_ass,
					produto_id,
					dt_ult_processamento,
					obrigatoria,
					nome_procedure,
					val_assistencia,
					descricao_processo,
					dt_inclusao,
					dt_alteracao,
					usuario,
					plano_assistencia_id,
					tp_assistencia_bb
				    )
	        SELECT  93 as ramo_id,
					9320 as subramo_id,
					PLANO_ASSISTENCIA_TB.dt_inicio_vigencia as dt_inicio_vigencia_sbr,
					PLANO_ASSISTENCIA_TB.dt_inicio_vigencia as dt_inicio_vigencia_ass,
					PLANO_ASSISTENCIA_TB.tipo_assistencia_id as tp_assistencia_id,
					PLANO_ASSISTENCIA_TB.dt_fim_vigencia as dt_fim_vigencia_ass,
					721 as produto_id,
					getdate() as dt_ult_processamento,
					CASE PLANO_ASSISTENCIA_TB.num_contrato WHEN '40000142525' THEN 'S' ELSE 'N' END as obrigatoria,
					CASE PLANO_ASSISTENCIA_TB.num_contrato WHEN '40000142525' THEN 'SEGS13877_SPI' ELSE 'SEGS13878_SPI' END as nome_procedure,
					0.00 as val_assistencia,
					NULL as descricao_processo,
					@dt_inclusao as dt_inclusao,
					@dt_alteracao as dt_alteracao,
					@usuario as usuario,
					PLANO_ASSISTENCIA_TB.plano_assistencia_id,
					NULL as tp_assistencia_bb
			  FROM  ASSISTENCIA_DB.DBO.PLANO_ASSISTENCIA_TB PLANO_ASSISTENCIA_TB WITH (NOLOCK)
			 WHERE  plano_assistencia_id in (4073,4074)

SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT
	  
	  
-- Verificação
		SELECT @total_registro_afetados

		SELECT @total_registro

IF ( @@SERVERNAME = 'SISAB003' /* AB */  AND @total_registro <> @total_registro_afetados )
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) 
	              + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

	RAISERROR (@mensagem, 16, 1)
END
GO	     
