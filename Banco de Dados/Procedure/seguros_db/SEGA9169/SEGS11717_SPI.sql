CREATE PROCEDURE dbo.SEGS11717_SPI
    @layout_id INT,              
    @arquivo_remessa_grf VARCHAR(50),  
    @dt_geracao_arquivo SMALLDATETIME,  
    @num_remessa INT,  
    @producao CHAR(1) = 'N' ,             
    @usuario VARCHAR(20)      

-------------------------------------------------------              
-----PARAMETROS DE TESTE
       
--    DECLARE @layout_id INT,              
--    @arquivo_remessa_grf VARCHAR(50),  
--    @dt_geracao_arquivo SMALLDATETIME,  
--    @num_remessa INT,  
--    @producao CHAR(1) ,             
--    @usuario VARCHAR(20)      
--
--SET    @layout_id = 1690              
--SET    @arquivo_remessa_grf = 'SEGA9169.0001'
--SET    @dt_geracao_arquivo = GETDATE()
--SET    @num_remessa = 1
--SET    @producao = 'S' ,             
--SET    @usuario = 'TESTE'
-------------------------------------------------------              
              
AS              
              
--@dt_geracao_arquivo SMALLDATETIME, ------------------
--Procedure de p�s processamento do arquivo SEGA9169 --              
-------------------------------------------------------       
BEGIN      
BEGIN TRY              
	SET NOCOUNT ON              
	              
	IF @producao = 's' 
	BEGIN

		UPDATE movimento_assistencia_vida_atual_tb
		   SET layout_id = @layout_id
			 , versao = @num_remessa
			 , dt_alteracao = getdate()
			 , usuario = @usuario
		  FROM  interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb  with (nolock)     
		  JOIN assistencia_db..movimento_assistencia_vida_atual_tb movimento_assistencia_vida_atual_tb with (nolock)      
			ON movimento_assistencia_vida_atual_tb.proposta_id = SEGA9169_processar_tb.proposta_id      
		   AND movimento_assistencia_vida_atual_tb.apolice_id = SEGA9169_processar_tb.apolice_id      
		   AND movimento_assistencia_vida_atual_tb.ramo_id = SEGA9169_processar_tb.ramo_id      
		   AND movimento_assistencia_vida_atual_tb.sub_grupo_id = SEGA9169_processar_tb.sub_grupo_id      
		   AND movimento_assistencia_vida_atual_tb.prop_cliente_id = SEGA9169_processar_tb.prop_cliente_id   
           AND movimento_assistencia_vida_atual_tb.plano_assistencia_id = SEGA9169_processar_tb.plano_assistencia_id
		   AND movimento_assistencia_vida_atual_tb.enviar = 'S'   
		   AND movimento_assistencia_vida_atual_tb.layout_id IS NULL
		   AND movimento_assistencia_vida_atual_tb.versao IS NULL

  -- altera��es 18720075 - produto 1206
				   
		  UPDATE assistencia_db.dbo.movimento_assistencia_atual_tb  
		   SET 
			  ult_movimento_Assistencia_id = @num_remessa 
			 , dt_alteracao = getdate()  
			 , dt_envio = getdate()
			 , dt_registro_movimento =  @dt_geracao_arquivo
			 , usuario = @usuario
		  FROM  interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb  with (nolock)     
		  join assistencia_db..movimento_assistencia_atual_tb maa with (nolock)
			on maa.proposta_id = SEGA9169_processar_tb.proposta_id
		   and maa.cliente_id = SEGA9169_processar_tb.prop_cliente_id
		   and maa.produto_id = SEGA9169_processar_tb.produto_id
		   and maa.tp_movimento_id = SEGA9169_processar_tb.tp_movimento_id
		  where maa.ult_movimento_assistencia_id is null
			and maa.dt_registro_movimento is null		   
		   

-------------------------------------------------------------------------------------------- 
-- In�cio  - MU-2018-033206 - Produto 721 - Arquivo de Assist�ncia
-------------------------------------------------------------------------------------------- 
				   
	    UPDATE movimento_assistencia_atual_tb  
		   SET 
			   ult_movimento_Assistencia_id = @num_remessa 
		     , dt_alteracao = getdate()  
		     , dt_envio = getdate()
		     , dt_registro_movimento =  @dt_geracao_arquivo
		     , usuario = @usuario
		  FROM interface_dados_db.dbo.SEGA9169_processar_tb SEGA9169_processar_tb  with (nolock)     
    INNER JOIN assistencia_uss_db.dbo.movimento_assistencia_atual_tb movimento_assistencia_atual_tb with (nolock)
			ON movimento_assistencia_atual_tb.proposta_id = SEGA9169_processar_tb.proposta_id
		   AND movimento_assistencia_atual_tb.cliente_id = SEGA9169_processar_tb.prop_cliente_id
		   AND movimento_assistencia_atual_tb.produto_id = SEGA9169_processar_tb.produto_id
		   AND movimento_assistencia_atual_tb.tp_movimento_id = SEGA9169_processar_tb.tp_movimento_id
		 WHERE movimento_assistencia_atual_tb.ult_movimento_assistencia_id is null
		   AND movimento_assistencia_atual_tb.dt_registro_movimento is null		   
		   
-------------------------------------------------------------------------------------------- 
-- Fim  - MU-2018-033206 - Produto 721 - Arquivo de Assist�ncia
-------------------------------------------------------------------------------------------- 
  
		   
		IF @@ERROR <> 0                      
		BEGIN          
			-- Desviando para o bloco CATCH.            
			RAISERROR ('Erro na atualiza��o assistencia_db..movimento_assistencia_vida_atual_tb', -- Message text.
			       16, -- Severity.
				   1 -- State.
				   );
		  END 
        
	END

	-- Log de evento de assist�ncia
	EXEC SEGS11718_SPI @layout_id,   
		   @arquivo_remessa_grf,  
		   @dt_geracao_arquivo,  
		   @num_remessa,    
		   @producao,  
		   @usuario             
	              
	IF @@ERROR <> 0              
	BEGIN              
		RAISERROR ('Erro na atualiza��o da impress�o', -- Message text.
			       16, -- Severity.
				   1 -- State.
				   );              
	END              
	              
	--Expurgo de Dados (copiar das tabelas processar para as tabelas processado)              
	EXEC SEGS11719_SPI @usuario, @num_remessa           
	              
	IF @@ERROR <> 0              
	BEGIN
		RAISERROR ('Erro na procedure SEGS11719_SPI (pos processamento)', -- Message text.
			       16, -- Severity.
				   1 -- State.
				   );                            
	END              
	              
	SET NOCOUNT OFF              
	              
	RETURN              
	              
END TRY
BEGIN CATCH

  DECLARE @ErrorMessage NVARCHAR(4000)
  DECLARE @ErrorSeverity INT
  DECLARE @ErrorState INT

  SELECT 
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE() + ' - ' + 'Erro ao inserir registros em interface_dados_db..SEGA9169_processar_tb',
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE()

      -- Use RAISERROR inside the CATCH block to return error
      -- information about the original error that caused
      -- execution to jump to the CATCH block.
      RAISERROR (@ErrorMessage, -- Message text.
                 @ErrorSeverity, -- Severity.
                 @ErrorState -- State.
                 )

END CATCH
END
GO

