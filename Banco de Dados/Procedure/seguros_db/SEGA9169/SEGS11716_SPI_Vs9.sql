CREATE PROCEDURE Segs11716_spi @usuario VARCHAR(20)
AS
  -----------------------------------------------------------                                              
  -- Procedure Detalhe 10 - Segurado SEGA9169              --                                              
  -----------------------------------------------------------                                              
  BEGIN TRY
      SET NOCOUNT ON

      DECLARE @dt_sistema SMALLDATETIME

      SELECT @dt_sistema = dt_operacional
      FROM   parametro_geral_tb (NOLOCK)

      SELECT SEGA9169_processar_tb.sega9169_processar_id,
             SEGA9169_processar_tb.processar_id,
             '40001172904'                                                                                                                                                           num_contrato,
             0                                                                                                                                                                       versao_contrato,
             movimento_assistencia_vida_atual_tb.apolice_id,
             Isnull(CONVERT(VARCHAR(15), movimento_assistencia_vida_atual_tb.proposta_bb), '')                                                                                       proposta_externa,
             CASE movimento_assistencia_vida_atual_tb.tp_movimentacao_id
               WHEN 1 THEN 'I'
               ELSE 'E'
             END                                                                                                                                                                     tp_operacao,
             movimento_assistencia_vida_atual_tb.nome_segurado COLLATE sql_latin1_general_cp1251_ci_as                                                                               nome_segurado,
             Isnull(Cast(CONVERT(CHAR, movimento_assistencia_vida_atual_tb.dt_inicio_vigencia, 112) AS VARCHAR), '')                                                                 dt_inicio_vigencia,
             Isnull(Cast(CONVERT(CHAR, movimento_assistencia_vida_atual_tb.dt_fim_vigencia, 112) AS VARCHAR), '')                                                                    dt_fim_vigencia,
             ( Replicate('0', 14 - Len(movimento_assistencia_vida_atual_tb.cpf_cnpj_estipulante)) ) + CONVERT(VARCHAR(14), movimento_assistencia_vida_atual_tb.cpf_cnpj_estipulante) cpf_cnpj_estipulante,
             ( Replicate('0', 11 - Len(movimento_assistencia_vida_atual_tb.cpf_segurado)) ) + CONVERT(VARCHAR(11), movimento_assistencia_vida_atual_tb.cpf_segurado)                 cpf_segurado,
             Isnull(seguros_db..endereco_corresp_tb.endereco, '')                                                                                                                    endereco,
             Isnull(seguros_db..endereco_corresp_tb.estado, '')                                                                                                                      estado,
             Isnull(seguros_db..endereco_corresp_tb.municipio, '')                                                                                                                   municipio,
             Isnull(seguros_db..endereco_corresp_tb.bairro, '')                                                                                                                      bairro,
             Isnull(seguros_db..endereco_corresp_tb.cep, '')                                                                                                                         cep,
             Isnull(seguros_db..endereco_corresp_tb.ddd
                    + seguros_db..endereco_corresp_tb.telefone, 0)                                                                                                                   telefone,
             ''                                                                                                                                                                      endereco_risco,
             ''                                                                                                                                                                      uf_risco,
             ''                                                                                                                                                                      municipio_risco,
             ''                                                                                                                                                                      bairro_risco,
             ''                                                                                                                                                                      cep_risco,
             0                                                                                                                                                                       telefone_risco,
             ''                                                                                                                                                                      placa_veiculo,
             ''                                                                                                                                                                      chassi_veiculo,
             ''                                                                                                                                                                      cor_veiculo,
             ''                                                                                                                                                                      ano_fab_veiculo,
             ''                                                                                                                                                                      modelo_veiculo,
             ''                                                                                                                                                                      marca_veiculo,
             ''                                                                                                                                                                      dias_reserva_pparcial,
             ''                                                                                                                                                                      dias_reserva_ptotal,
             ''                                                                                                                                                                      dias_reserva_roubo,
             Isnull(movimento_assistencia_vida_atual_tb.nome_estipulante, '')                                                                                                        nome_estipulante,
             ''                                                                                                                                                                      parentesco,
             Cast (0.00 AS NUMERIC(15, 2))                                                                                                                                           val_lim_funeral,
             Cast (0.00 AS NUMERIC(15, 2))                                                                                                                                           val_lim_dmho,
             0                                                                                                                                                                       qtd_cestas,
             0                                                                                                                                                                       val_cestas,
             CONVERT(CHAR, Isnull(seguros_db..pessoa_fisica_tb.dt_nascimento, CONVERT(DATETIME, '1900-01-01')), 112)                                                                 dt_nasc_usuario,
             chave_principal = Isnull(RIGHT('' + Cast( movimento_assistencia_vida_atual_tb.apolice_id AS VARCHAR), 9)
                                      + RIGHT('0000' + Cast( movimento_assistencia_vida_atual_tb.ramo_id AS VARCHAR), 4)
                                      + RIGHT('000' + Cast( movimento_assistencia_vida_atual_tb.sub_grupo_id AS VARCHAR), 3)
                                      + RIGHT('00000000000000' + Cast( movimento_assistencia_vida_atual_tb.cpf_segurado AS VARCHAR), 14), 0),
             movimento_assistencia_vida_atual_tb.tp_movimentacao_id,
             movimento_assistencia_vida_atual_tb.sucursal_seguradora_id,
             movimento_assistencia_vida_atual_tb.seguradora_cod_susep,
             movimento_assistencia_vida_atual_tb.ramo_id,
             movimento_assistencia_vida_atual_tb.sub_grupo_id
      INTO   #temp_SEGA9169_processar_tb
      FROM   interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK)
             JOIN assistencia_db..movimento_assistencia_vida_atual_tb movimento_assistencia_vida_atual_tb WITH (NOLOCK)
               ON movimento_assistencia_vida_atual_tb.apolice_id = SEGA9169_processar_tb.apolice_id
                  AND movimento_assistencia_vida_atual_tb.ramo_id = SEGA9169_processar_tb.ramo_id
                  AND movimento_assistencia_vida_atual_tb.sub_grupo_id = SEGA9169_processar_tb.sub_grupo_id
                  AND movimento_assistencia_vida_atual_tb.prop_cliente_id = SEGA9169_processar_tb.prop_cliente_id
                  AND movimento_assistencia_vida_atual_tb.tp_movimentacao_id = SEGA9169_processar_tb.tp_movimento_id
             JOIN seguros_db..pessoa_fisica_tb WITH (NOLOCK)
               ON seguros_db..pessoa_fisica_tb.pf_cliente_id = movimento_assistencia_vida_atual_tb.prop_cliente_id
             JOIN seguros_db..endereco_corresp_tb WITH (NOLOCK)
               ON seguros_db..endereco_corresp_tb.proposta_id = movimento_assistencia_vida_atual_tb.proposta_id
      WHERE  movimento_assistencia_vida_atual_tb.plano_assistencia_id = 2904 --assistencia escolar             
             AND movimento_assistencia_vida_atual_tb.enviar = 'S'
             AND movimento_assistencia_vida_atual_tb.layout_id IS NULL
             AND movimento_assistencia_vida_atual_tb.versao IS NULL

      CREATE INDEX AK001_temp_SEGA9169_processar_tb
        ON #temp_SEGA9169_processar_tb (tp_movimentacao_id)

      UPDATE tmp
      SET    tmp.tp_operacao = mov.tp_operacao
      FROM   #temp_SEGA9169_processar_tb tmp
             JOIN assistencia_db..tp_movimento_tb mov WITH (NOLOCK)
               ON mov.tp_movimento_id = tmp.tp_movimentacao_id

      CREATE INDEX AK002_temp_SEGA9169_processar_tb
        ON #temp_SEGA9169_processar_tb (apolice_id, sucursal_seguradora_id, seguradora_cod_susep, ramo_id, sub_grupo_id)

      CREATE TABLE #temp_apolice
        (
           apolice_id             NUMERIC(9),
           sucursal_seguradora_id NUMERIC(5),
           seguradora_cod_susep   NUMERIC(5),
           ramo_id                INT,
           sub_grupo_id           INT,
           val_is                 NUMERIC(15, 2) NULL,
           perc_basica            NUMERIC(9, 6) NULL
        )

      INSERT INTO #temp_apolice
                  (apolice_id,
                   sucursal_seguradora_id,
                   seguradora_cod_susep,
                   ramo_id,
                   sub_grupo_id)
      SELECT apolice_id,
             sucursal_seguradora_id,
             seguradora_cod_susep,
             ramo_id,
             sub_grupo_id
      FROM   #temp_SEGA9169_processar_tb
      GROUP  BY apolice_id,
                sucursal_seguradora_id,
                seguradora_cod_susep,
                ramo_id,
                sub_grupo_id

      --CALCULANDO O VALOR DA COBERTURA DMHO
      SELECT e.apolice_id,
             e.sucursal_seguradora_id,
             e.seguradora_cod_susep,
             e.ramo_id,
             e.sub_grupo_id,
             CASE a.tp_is
               WHEN 'F' THEN e.val_is
               ELSE 0
             END val_is_fixo
      INTO   #temp_dmho_1
      FROM   escolha_sub_grp_tp_cob_comp_tb e
             JOIN sub_grupo_apolice_tb a
               ON a.apolice_id = e.apolice_id
                  AND a.sucursal_seguradora_id = e.sucursal_seguradora_id
                  AND a.seguradora_cod_susep = e.seguradora_cod_susep
                  AND a.ramo_id = e.ramo_id
                  AND a.sub_grupo_id = e.sub_grupo_id
                  AND a.dt_inicio_vigencia_sbg = e.dt_inicio_vigencia_sbg
             JOIN #temp_apolice td
               ON e.apolice_id = td.apolice_id
                  AND e.sucursal_seguradora_id = td.sucursal_seguradora_id
                  AND e.seguradora_cod_susep = td.seguradora_cod_susep
                  AND e.ramo_id = td.ramo_id
                  AND e.sub_grupo_id = td.sub_grupo_id
      WHERE  e.class_tp_cobertura = 'B'
             AND e.val_is > 0

      UPDATE #temp_apolice
      SET    val_is = td.val_is_fixo
      FROM   #temp_dmho_1 td
      WHERE  td.apolice_id = #temp_apolice.apolice_id
             AND td.sucursal_seguradora_id = #temp_apolice.sucursal_seguradora_id
             AND td.seguradora_cod_susep = #temp_apolice.seguradora_cod_susep
             AND td.ramo_id = #temp_apolice.ramo_id
             AND td.sub_grupo_id = #temp_apolice.sub_grupo_id

      SELECT e.apolice_id,
             e.sucursal_seguradora_id,
             e.seguradora_cod_susep,
             e.ramo_id,
             e.sub_grupo_id,
             e.perc_basica
      INTO   #temp_dmho_2
      FROM   escolha_sub_grp_tp_cob_comp_tb e
             JOIN tp_cob_comp_tb c
               ON e.tp_cob_comp_id = c.tp_cob_comp_id
             JOIN #temp_apolice td
               ON e.apolice_id = td.apolice_id
                  AND e.sucursal_seguradora_id = td.sucursal_seguradora_id
                  AND e.seguradora_cod_susep = td.seguradora_cod_susep
                  AND e.ramo_id = td.ramo_id
                  AND e.sub_grupo_id = td.sub_grupo_id
      WHERE  c.tp_cobertura_id = 830 -- Cobertura DMHO
      UPDATE #temp_apolice
      SET    perc_basica = td.perc_basica
      FROM   #temp_dmho_2 td
      WHERE  td.apolice_id = #temp_apolice.apolice_id
             AND td.sucursal_seguradora_id = #temp_apolice.sucursal_seguradora_id
             AND td.seguradora_cod_susep = #temp_apolice.seguradora_cod_susep
             AND td.ramo_id = #temp_apolice.ramo_id
             AND td.sub_grupo_id = #temp_apolice.sub_grupo_id

      UPDATE #temp_SEGA9169_processar_tb
      SET    val_lim_dmho = Cast (td.val_is * ( Isnull(td.perc_basica, 0) / 100.00 ) AS NUMERIC (15, 2))
      FROM   #temp_SEGA9169_processar_tb
             JOIN #temp_apolice td
               ON td.apolice_id = #temp_SEGA9169_processar_tb.apolice_id
                  AND td.sucursal_seguradora_id = #temp_SEGA9169_processar_tb.sucursal_seguradora_id
                  AND td.seguradora_cod_susep = #temp_SEGA9169_processar_tb.seguradora_cod_susep
                  AND td.ramo_id = #temp_SEGA9169_processar_tb.ramo_id
                  AND td.sub_grupo_id = #temp_SEGA9169_processar_tb.sub_grupo_id

      --CALCULANDO O VALOR DA COBERTURA FUNERAL
      SELECT e.apolice_id,
             e.sucursal_seguradora_id,
             e.seguradora_cod_susep,
             e.ramo_id,
             e.sub_grupo_id,
             e.perc_basica,
             c.val_fixo_cobertura,
             c.tp_val_cobertura
      INTO   #temp_funeral_1
      FROM   escolha_sub_grp_tp_cob_comp_tb e
             JOIN tp_cob_comp_tb c
               ON e.tp_cob_comp_id = c.tp_cob_comp_id
             JOIN #temp_apolice td
               ON e.apolice_id = td.apolice_id
                  AND e.sucursal_seguradora_id = td.sucursal_seguradora_id
                  AND e.seguradora_cod_susep = td.seguradora_cod_susep
                  AND e.ramo_id = td.ramo_id
                  AND e.sub_grupo_id = td.sub_grupo_id
      WHERE  c.tp_cobertura_id IN ( 313, 819 ) -- Cobertura Auxilio funeral
      UPDATE #temp_apolice
      SET    perc_basica = td.perc_basica
      FROM   #temp_funeral_1 td
      WHERE  td.apolice_id = #temp_apolice.apolice_id
             AND td.sucursal_seguradora_id = #temp_apolice.sucursal_seguradora_id
             AND td.seguradora_cod_susep = #temp_apolice.seguradora_cod_susep
             AND td.ramo_id = #temp_apolice.ramo_id
             AND td.sub_grupo_id = #temp_apolice.sub_grupo_id

      UPDATE #temp_SEGA9169_processar_tb
      SET    val_lim_funeral = CASE
                                 WHEN Isnull (td.tp_val_cobertura, 'P') = 'F' THEN td.val_fixo_cobertura
                                 ELSE Cast (ta.val_is * ( Isnull(ta.perc_basica, 0) / 100.00 ) AS NUMERIC (15, 2))
                               END
      FROM   #temp_SEGA9169_processar_tb
             JOIN #temp_funeral_1 td
               ON td.apolice_id = #temp_SEGA9169_processar_tb.apolice_id
                  AND td.sucursal_seguradora_id = #temp_SEGA9169_processar_tb.sucursal_seguradora_id
                  AND td.seguradora_cod_susep = #temp_SEGA9169_processar_tb.seguradora_cod_susep
                  AND td.ramo_id = #temp_SEGA9169_processar_tb.ramo_id
                  AND td.sub_grupo_id = #temp_SEGA9169_processar_tb.sub_grupo_id
             JOIN #temp_apolice ta
               ON ta.apolice_id = #temp_SEGA9169_processar_tb.apolice_id
                  AND ta.sucursal_seguradora_id = #temp_SEGA9169_processar_tb.sucursal_seguradora_id
                  AND ta.seguradora_cod_susep = #temp_SEGA9169_processar_tb.seguradora_cod_susep
                  AND ta.ramo_id = #temp_SEGA9169_processar_tb.ramo_id
                  AND ta.sub_grupo_id = #temp_SEGA9169_processar_tb.sub_grupo_id

      INSERT INTO interface_dados_db..SEGA9169_10_processar_tb
                  (sega9169_processar_id,
                   processar_id,
                   num_contrato,
                   versao_contrato,
                   apolice_id,
                   proposta_externa,
                   tp_movimento,
                   nome_usuario,
                   dt_ini_vigencia,
                   dt_fim_vigencia,
                   cnpj,
                   cpf,
                   endereco_usuario,
                   uf_usuario,
                   municipio_usuario,
                   bairro_usuario,
                   cep_usuario,
                   telefone_usuario,
                   endereco_risco,
                   uf_risco,
                   municipio_risco,
                   bairro_risco,
                   cep_risco,
                   telefone_risco,
                   placa_veiculo,
                   chassi_veiculo,
                   cor_veiculo,
                   ano_fab_veiculo,
                   modelo_veiculo,
                   marca_veiculo,
                   dias_reserva_pparcial,
                   dias_reserva_ptotal,
                   dias_reserva_roubo,
                   nome_estipulante,
                   parentesco,
                   val_lim_funeral,
                   val_lim_dmho,
                   qtd_cestas,
                   val_cestas,
                   dt_nasc_usuario,
                   dt_inclusao,
                   dt_alteracao,
                   usuario,
                   chave_principal)
      SELECT sega9169_processar_id,
             processar_id,
             num_contrato,
             versao_contrato,
             apolice_id,
             proposta_externa,
             tp_operacao,
             nome_segurado,
             dt_inicio_vigencia,
             dt_fim_vigencia,
             cpf_cnpj_estipulante,
             cpf_segurado,
             endereco,
             estado,
             municipio,
             bairro,
             cep,
             telefone,
             endereco_risco,
             uf_risco,
             municipio_risco,
             bairro_risco,
             cep_risco,
             telefone_risco,
             placa_veiculo,
             chassi_veiculo,
             cor_veiculo,
             ano_fab_veiculo,
             modelo_veiculo,
             marca_veiculo,
             dias_reserva_pparcial,
             dias_reserva_ptotal,
             dias_reserva_roubo,
             nome_estipulante,
             parentesco,
             val_lim_funeral,
             val_lim_dmho,
             qtd_cestas,
             val_cestas,
             dt_nasc_usuario,
             @dt_sistema,
             NULL,
             @usuario,
             chave_principal
      FROM   #temp_SEGA9169_processar_tb

      -- altera��es 18720075 - Altera��es produto 1206
      SELECT SEGA9169_processar_tb.sega9169_processar_id,
             SEGA9169_processar_tb.processar_id,
             '40000053479'                                                                                   AS num_contrato,
             0                                                                                               AS versao_contrato,
             apolice_tb.apolice_id,
             RIGHT('00000000'
                   + Cast( Isnull(CONVERT(VARCHAR(15), proposta_fechadA_tb.proposta_bb), '') AS VARCHAR), 8) AS proposta_externa,
             CASE SEGA9169_processar_tb.tp_movimento_id
               WHEN 1 THEN 'I'
               WHEN 3 THEN 'A'
               ELSE 'E'
             END                                                                                             tp_operacao,
             cliente_tb.nome COLLATE sql_latin1_general_cp1251_ci_as                                         nome_segurado,
             apolice_tb.dt_inicio_vigencia,
             Isnull(Cast(CONVERT(CHAR, apolice_tb.dt_fim_vigencia, 112) AS VARCHAR), '')                     dt_fim_vigencia,
             cliente_tb.cpf_cnpj                                                                             AS cpf_cnpj_estipulante,
             '00000000000'                                                                                   AS cpf_segurado,
             Isnull(endereco_corresp_tb.endereco, '')                                                        endereco,
             Isnull(endereco_corresp_tb.estado, '')                                                          estado,
             Isnull(endereco_corresp_tb.municipio, '')                                                       municipio,
             Isnull(endereco_corresp_tb.bairro, '')                                                          bairro,
             Isnull(endereco_corresp_tb.cep, '')                                                             cep,
             Isnull(Rtrim(Ltrim(endereco_corresp_tb.ddd))
                    + Rtrim(Ltrim(endereco_corresp_tb.telefone)), 0)                                         telefone,
             ''                                                                                              endereco_risco,
             ''                                                                                              uf_risco,
             ''                                                                                              municipio_risco,
             ''                                                                                              bairro_risco,
             ''                                                                                              cep_risco,
             0                                                                                               telefone_risco,
             ''                                                                                              placa_veiculo,
             ''                                                                                              chassi_veiculo,
             ''                                                                                              cor_veiculo,
             ''                                                                                              ano_fab_veiculo,
             ''                                                                                              modelo_veiculo,
             ''                                                                                              marca_veiculo,
             ''                                                                                              dias_reserva_pparcial,
             ''                                                                                              dias_reserva_ptotal,
             ''                                                                                              dias_reserva_roubo,
             cliente_tb.nome                                                                                 AS nome_estipulante,
             ''                                                                                              parentesco,
             0                                                                                               val_lim_funeral,
             Cast (0.00 AS NUMERIC(15, 2))                                                                   val_lim_dmho,
             0                                                                                               qtd_cestas,
             0                                                                                               val_cestas,
             ''                                                                                              AS dt_nasc_usuario,
             Getdate()                                                                                       AS dt_inclusao,
             NULL                                                                                            AS dt_alteracao,
             @usuario                                                                                        AS usuario,
             Isnull(RIGHT('000000000' + Cast( apolice_tb.apolice_id AS VARCHAR), 9)
                    + RIGHT('0000' + Cast( apolice_tb.ramo_id AS VARCHAR), 4)
                    + '000' + '000000000', 0)                                                                AS chave_principal
      INTO   #temp_SEGA9169_10_processar_tb_3479
      FROM   interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK)
             JOIN assistencia_db..proposta_assistencia_atual_tb WITH (nolock)
               ON SEGA9169_processar_tb.proposta_id = proposta_assistencia_atual_tb.proposta_id
             JOIN seguros_db..endereco_corresp_tb WITH (NOLOCK)
               ON seguros_db..endereco_corresp_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..proposta_tb WITH (nolock)
               ON proposta_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..apolice_tb WITH (nolock)
               ON apolice_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..proposta_fechada_tb WITH (nolock)
               ON proposta_fechada_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..cliente_tb WITH (nolock)
               ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id
      WHERE  SEGA9169_processar_tb.produto_id = 1206
             AND SEGA9169_processar_tb.plano_assistencia_id IN ( 3479 )

      SELECT SEGA9169_processar_tb.sega9169_processar_id,
             SEGA9169_processar_tb.processar_id,
             '40000043481'                                                                                   AS num_contrato,
             0                                                                                               AS versao_contrato,
             apolice_tb.apolice_id,
             RIGHT('00000000'
                   + Cast( Isnull(CONVERT(VARCHAR(15), proposta_fechadA_tb.proposta_bb), '') AS VARCHAR), 8) AS proposta_externa,
             CASE SEGA9169_processar_tb.tp_movimento_id
               WHEN 1 THEN 'I'
               WHEN 3 THEN 'A'
               ELSE 'E'
             END                                                                                             tp_operacao,
             cliente_tb.nome COLLATE sql_latin1_general_cp1251_ci_as                                         nome_segurado,
             apolice_tb.dt_inicio_vigencia,
             Isnull(Cast(CONVERT(CHAR, apolice_tb.dt_fim_vigencia, 112) AS VARCHAR), '')                     dt_fim_vigencia,
             cliente_tb.cpf_cnpj                                                                             AS cpf_cnpj_estipulante,
             '00000000000'                                                                                   AS cpf_segurado,
             Isnull(endereco_corresp_tb.endereco, '')                                                        endereco,
             Isnull(endereco_corresp_tb.estado, '')                                                          estado,
             Isnull(endereco_corresp_tb.municipio, '')                                                       municipio,
             Isnull(endereco_corresp_tb.bairro, '')                                                          bairro,
             Isnull(endereco_corresp_tb.cep, '')                                                             cep,
             Isnull(Rtrim(Ltrim(endereco_corresp_tb.ddd))
                    + Rtrim(Ltrim(endereco_corresp_tb.telefone)), 0)                                         telefone,
             ''                                                                                              endereco_risco,
             ''                                                                                              uf_risco,
             ''                                                                                              municipio_risco,
             ''                                                                                              bairro_risco,
             ''                                                                                              cep_risco,
             0                                                                                               telefone_risco,
             ''                                                                                              placa_veiculo,
             ''                                                                                              chassi_veiculo,
             ''                                                                                              cor_veiculo,
             ''                                                                                              ano_fab_veiculo,
             ''                                                                                              modelo_veiculo,
             ''                                                                                              marca_veiculo,
             ''                                                                                              dias_reserva_pparcial,
             ''                                                                                              dias_reserva_ptotal,
             ''                                                                                              dias_reserva_roubo,
             cliente_tb.nome                                                                                 AS nome_estipulante,
             ''                                                                                              parentesco,
             0                                                                                               val_lim_funeral,
             Cast (0.00 AS NUMERIC(15, 2))                                                                   val_lim_dmho,
             0                                                                                               qtd_cestas,
             0                                                                                               val_cestas,
             ''                                                                                              AS dt_nasc_usuario,
             Getdate()                                                                                       AS dt_inclusao,
             NULL                                                                                            AS dt_alteracao,
             @usuario                                                                                        AS usuario,
             Isnull(RIGHT('000000000' + Cast( apolice_tb.apolice_id AS VARCHAR), 9)
                    + RIGHT('0000' + Cast( apolice_tb.ramo_id AS VARCHAR), 4)
                    + '000' + '000000000', 0)                                                                AS chave_principal
      INTO   #temp_SEGA9169_10_processar_tb_3481
      FROM   interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK)
             JOIN assistencia_db..proposta_assistencia_atual_tb WITH (nolock)
               ON SEGA9169_processar_tb.proposta_id = proposta_assistencia_atual_tb.proposta_id
             JOIN seguros_db..endereco_corresp_tb WITH (NOLOCK)
               ON seguros_db..endereco_corresp_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..proposta_tb WITH (nolock)
               ON proposta_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..apolice_tb WITH (nolock)
               ON apolice_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..proposta_fechada_tb WITH (nolock)
               ON proposta_fechada_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             JOIN seguros_db..cliente_tb WITH (nolock)
               ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id
      WHERE  SEGA9169_processar_tb.produto_id = 1206
             AND SEGA9169_processar_tb.plano_assistencia_id IN ( 3481 )

      INSERT INTO interface_dados_db..SEGA9169_10_processar_tb
                  (sega9169_processar_id,
                   processar_id,
                   num_contrato,
                   versao_contrato,
                   apolice_id,
                   proposta_externa,
                   tp_movimento,
                   nome_usuario,
                   dt_ini_vigencia,
                   dt_fim_vigencia,
                   cnpj,
                   cpf,
                   endereco_usuario,
                   uf_usuario,
                   municipio_usuario,
                   bairro_usuario,
                   cep_usuario,
                   telefone_usuario,
                   endereco_risco,
                   uf_risco,
                   municipio_risco,
                   bairro_risco,
                   cep_risco,
                   telefone_risco,
                   placa_veiculo,
                   chassi_veiculo,
                   cor_veiculo,
                   ano_fab_veiculo,
                   modelo_veiculo,
                   marca_veiculo,
                   dias_reserva_pparcial,
                   dias_reserva_ptotal,
                   dias_reserva_roubo,
                   nome_estipulante,
                   parentesco,
                   val_lim_funeral,
                   val_lim_dmho,
                   qtd_cestas,
                   val_cestas,
                   dt_nasc_usuario,
                   dt_inclusao,
                   dt_alteracao,
                   usuario,
                   chave_principal)
      SELECT sega9169_processar_id,
             processar_id,
             num_contrato,
             versao_contrato,
             apolice_id,
             proposta_externa,
             tp_operacao,
             nome_segurado,
             dt_inicio_vigencia,
             dt_fim_vigencia,
             cpf_cnpj_estipulante,
             cpf_segurado,
             endereco,
             estado,
             municipio,
             bairro,
             cep,
             telefone,
             endereco_risco,
             uf_risco,
             municipio_risco,
             bairro_risco,
             cep_risco,
             telefone_risco,
             placa_veiculo,
             chassi_veiculo,
             cor_veiculo,
             ano_fab_veiculo,
             modelo_veiculo,
             marca_veiculo,
             dias_reserva_pparcial,
             dias_reserva_ptotal,
             dias_reserva_roubo,
             nome_estipulante,
             parentesco,
             val_lim_funeral,
             val_lim_dmho,
             qtd_cestas,
             val_cestas,
             dt_nasc_usuario,
             @dt_sistema,
             NULL,
             @usuario,
             chave_principal
      FROM   #temp_SEGA9169_10_processar_tb_3479
      UNION
      SELECT sega9169_processar_id,
             processar_id,
             num_contrato,
             versao_contrato,
             apolice_id,
             proposta_externa,
             tp_operacao,
             nome_segurado,
             dt_inicio_vigencia,
             dt_fim_vigencia,
             cpf_cnpj_estipulante,
             cpf_segurado,
             endereco,
             estado,
             municipio,
             bairro,
             cep,
             telefone,
             endereco_risco,
             uf_risco,
             municipio_risco,
             bairro_risco,
             cep_risco,
             telefone_risco,
             placa_veiculo,
             chassi_veiculo,
             cor_veiculo,
             ano_fab_veiculo,
             modelo_veiculo,
             marca_veiculo,
             dias_reserva_pparcial,
             dias_reserva_ptotal,
             dias_reserva_roubo,
             nome_estipulante,
             parentesco,
             val_lim_funeral,
             val_lim_dmho,
             qtd_cestas,
             val_cestas,
             dt_nasc_usuario,
             @dt_sistema,
             NULL,
             @usuario,
             chave_principal
      FROM   #temp_SEGA9169_10_processar_tb_3481

      --MU-2017-021041 - Mudan�a de escopo Assist�ncia --2017-05-29 -in�cio
      IF Object_id('tempdb..#plano_assistencia2') IS NOT NULL
        BEGIN
            DROP TABLE #plano_assistencia2
        END

      CREATE TABLE #plano_assistencia2
        (
           id                   INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
           tp_assistencia_id    INT NULL,
           plano_assistencia_id INT NULL,
           produto_id           INT NULL,
           plano_id             INT NULL,
           num_contrato         VARCHAR(20) NULL,
           num_versao_contrato  VARCHAR(15) NULL
        )

      INSERT INTO #plano_assistencia2
                  (tp_assistencia_id,
                   plano_assistencia_id,
                   produto_id,
                   plano_id,
                   num_contrato,
                   num_versao_contrato)
      SELECT 74                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1235               produto_id,
             1                  plano_id,
             '99999999999'      num_contrato,
             '0'                num_versao_contrato
      UNION
      SELECT 75                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1235               produto_id,
             2                  plano_id,
             '99999999999'      num_contrato,
             '0'                num_versao_contrato
      UNION
      SELECT 76                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1235               produto_id,
             3                  plano_id,
             '99999999999'      num_contrato,
             '0'                num_versao_contrato
      UNION
      SELECT 77                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1236               produto_id,
             1                  plano_id,
             '99999999999'      num_contrato,
             '0'                num_versao_contrato
      UNION
      SELECT 78                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1237               produto_id,
             1                  plano_id,
             '99999999999'      num_contrato,
             '0'                num_versao_contrato
      UNION
      SELECT 79                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1237               produto_id,
             2                  plano_id,
             '99999999999'      num_contrato,
             '0'                num_versao_contrato
      UNION
      SELECT 80                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1237               produto_id,
             3                  plano_id,
             '99999999999'      num_contrato,
             '0'                num_versao_contrato

      --atualizando plano de assist�ncia
      UPDATE a
      SET    plano_assistencia_id = b.plano_assistencia_id
      FROM   #plano_assistencia2 a
             INNER JOIN assistencia_db.dbo.plano_assistencia_tb b WITH (NOLOCK)
                     ON a.tp_assistencia_id = b.tipo_assistencia_id
      WHERE  Isnull(b.DT_FIM_VIGENCIA, Dateadd(DAY, 1, Getdate())) > Getdate()

      --atualizando contrato e vers�o
      UPDATE a
      SET    num_contrato = Isnull(b.num_contrato, a.num_contrato),
             num_versao_contrato = Isnull(b.num_versao_contrato, a.num_versao_contrato)
      FROM   #plano_assistencia2 a
             INNER JOIN assistencia_db.dbo.plano_assistencia_tb b WITH (NOLOCK)
                     ON a.tp_assistencia_id = b.tipo_assistencia_id
      WHERE  Isnull(b.DT_FIM_VIGENCIA, Dateadd(DAY, 1, Getdate())) > Getdate()

      IF Object_id('tempdb..#temp_SEGA9169_10_processar_tb_novos_produtos') IS NOT NULL
        BEGIN
            DROP TABLE #temp_SEGA9169_10_processar_tb_novos_produtos
        END

      CREATE TABLE #temp_SEGA9169_10_processar_tb_novos_produtos
        (
           [id]                    [INT] IDENTITY(1, 1) PRIMARY KEY NOT NULL,
           [sega9169_processar_id] [INT] NULL,
           [processar_id]          [INT] NULL,
           [num_contrato]          [VARCHAR](20) NULL,
           [num_versao_contrato]   [VARCHAR](15) NULL,
           [apolice_id]            [NUMERIC](9, 0) NULL,
           [proposta_externa]      [VARCHAR](8) NULL,
           [tp_operacao]           [VARCHAR](1) NULL,
           [nome_segurado]         [VARCHAR](60) NULL,
           [dt_inicio_vigencia]    [SMALLDATETIME] NULL,
           [dt_fim_vigencia]       [VARCHAR](30) NULL,
           [cpf_cnpj_estipulante]  [VARCHAR](14) NULL,
           [cpf_segurado]          [VARCHAR](11) NULL,
           [endereco]              [VARCHAR](60) NULL,
           [estado]                [VARCHAR](2) NULL,
           [municipio]             [VARCHAR](30) NULL,
           [bairro]                [VARCHAR](30) NULL,
           [cep]                   [VARCHAR](8) NULL,
           [telefone]              [VARCHAR](13) NULL,
           [endereco_risco]        [VARCHAR](1) NULL,
           [uf_risco]              [VARCHAR](1) NULL,
           [municipio_risco]       [VARCHAR](1) NULL,
           [bairro_risco]          [VARCHAR](1) NULL,
           [cep_risco]             [VARCHAR](1) NULL,
           [telefone_risco]        [INT] NULL,
           [placa_veiculo]         [VARCHAR](1) NULL,
           [chassi_veiculo]        [VARCHAR](1) NULL,
           [cor_veiculo]           [VARCHAR](1) NULL,
           [ano_fab_veiculo]       [VARCHAR](1) NULL,
           [modelo_veiculo]        [VARCHAR](1) NULL,
           [marca_veiculo]         [VARCHAR](1) NULL,
           [dias_reserva_pparcial] [VARCHAR](1) NULL,
           [dias_reserva_ptotal]   [VARCHAR](1) NULL,
           [dias_reserva_roubo]    [VARCHAR](1) NULL,
           [nome_estipulante]      [VARCHAR](60) NULL,
           [parentesco]            [VARCHAR](1) NULL,
           [val_lim_funeral]       [INT] NULL,
           [val_lim_dmho]          [NUMERIC](15, 2) NULL,
           [qtd_cestas]            [INT] NULL,
           [val_cestas]            [INT] NULL,
           [dt_nasc_usuario]       [VARCHAR](1) NULL,
           [dt_inclusao]           [DATETIME] NULL,
           [dt_alteracao]          [INT] NULL,
           [usuario]               [VARCHAR](20) NULL,
           [chave_principal]       [VARCHAR](25) NULL,
           [prop_cliente_id]       [INT] NULL,
           [proposta_id]           [NUMERIC](9, 0) NULL,
           [endosso_id]            [INT] NULL
        )

      INSERT INTO [#temp_SEGA9169_10_processar_tb_novos_produtos]
                  ([sega9169_processar_id],
                   [processar_id],
                   [num_contrato],
                   [num_versao_contrato],
                   [apolice_id],
                   [proposta_externa],
                   [tp_operacao],
                   [nome_segurado],
                   [dt_inicio_vigencia],
                   [dt_fim_vigencia],
                   [cpf_cnpj_estipulante],
                   [cpf_segurado],
                   [endereco],
                   [estado],
                   [municipio],
                   [bairro],
                   [cep],
                   [telefone],
                   [endereco_risco],
                   [uf_risco],
                   [municipio_risco],
                   [bairro_risco],
                   [cep_risco],
                   [telefone_risco],
                   [placa_veiculo],
                   [chassi_veiculo],
                   [cor_veiculo],
                   [ano_fab_veiculo],
                   [modelo_veiculo],
                   [marca_veiculo],
                   [dias_reserva_pparcial],
                   [dias_reserva_ptotal],
                   [dias_reserva_roubo],
                   [nome_estipulante],
                   [parentesco],
                   [val_lim_funeral],
                   [val_lim_dmho],
                   [qtd_cestas],
                   [val_cestas],
                   [dt_nasc_usuario],
                   [dt_inclusao],
                   [dt_alteracao],
                   [usuario],
                   [chave_principal],
                   [prop_cliente_id],
                   [proposta_id],
                   [endosso_id])
      SELECT SEGA9169_processar_tb.sega9169_processar_id,
             SEGA9169_processar_tb.processar_id,
             plano_assistencia2.num_contrato,
             plano_assistencia2.num_versao_contrato,
             apolice_tb.apolice_id,
             RIGHT('00000000'
                   + Cast(Isnull(CONVERT(VARCHAR(15), proposta_fechada_tb.proposta_bb ), '') AS VARCHAR(15)), 8) AS proposta_externa,
             CASE
               WHEN SEGA9169_processar_tb.tp_movimento_id = 1 THEN 'I'
               WHEN SEGA9169_processar_tb.tp_movimento_id = 3 THEN 'A'
               ELSE 'E'
             END                                                                                                 AS tp_operacao,
             cliente_tb.nome COLLATE sql_latin1_general_cp1251_ci_as                                             AS nome_segurado,
             apolice_tb.dt_inicio_vigencia,
             Isnull(Cast(CONVERT(VARCHAR(15), apolice_tb.dt_fim_vigencia, 112) AS VARCHAR(15)), '')              dt_fim_vigencia,
             cliente_tb.cpf_cnpj                                                                                 AS cpf_cnpj_estipulante,
             '00000000000'                                                                                       AS cpf_segurado,
             Isnull(endereco_corresp_tb.endereco, '')                                                            endereco,
             Isnull(endereco_corresp_tb.estado, '')                                                              estado,
             Isnull(endereco_corresp_tb.municipio, '')                                                           municipio,
             Isnull(endereco_corresp_tb.bairro, '')                                                              bairro,
             Isnull(endereco_corresp_tb.cep, '')                                                                 cep,
             Isnull(Rtrim(Ltrim(endereco_corresp_tb.ddd))
                    + Rtrim(Ltrim(endereco_corresp_tb.telefone)), 0)                                             telefone,
             ''                                                                                                  endereco_risco,
             ''                                                                                                  uf_risco,
             ''                                                                                                  municipio_risco,
             ''                                                                                                  bairro_risco,
             ''                                                                                                  cep_risco,
             0                                                                                                   telefone_risco,
             ''                                                                                                  placa_veiculo,
             ''                                                                                                  chassi_veiculo,
             ''                                                                                                  cor_veiculo,
             ''                                                                                                  ano_fab_veiculo,
             ''                                                                                                  modelo_veiculo,
             ''                                                                                                  marca_veiculo,
             ''                                                                                                  dias_reserva_pparcial,
             ''                                                                                                  dias_reserva_ptotal,
             ''                                                                                                  dias_reserva_roubo,
             cliente_tb.nome                                                                                     AS nome_estipulante,
             ''                                                                                                  parentesco,
             0.00                                                                                                val_lim_funeral,
             Cast(0.00 AS NUMERIC(15, 2))                                                                        val_lim_dmho,
             0                                                                                                   qtd_cestas,
             0                                                                                                   val_cestas,
             ''                                                                                                  AS dt_nasc_usuario,
             Getdate()                                                                                           AS dt_inclusao,
             NULL                                                                                                AS dt_alteracao,
             @usuario                                                                                            AS usuario,
             --ISNULL(RIGHT('000000000' + CAST(apolice_tb.apolice_id AS VARCHAR(15)), 9) + RIGHT('0000' + CAST(apolice_tb.ramo_id AS VARCHAR(15)), 4) + '000' + '000000000', 0) AS chave_principal
             Isnull(RIGHT('' + Cast( apolice_tb.apolice_id AS VARCHAR), 9)
                    + RIGHT('0000' + Cast( apolice_tb.ramo_id AS VARCHAR), 4)
                    + '000'
                    + RIGHT('00000000000' + Cast( cliente_tb.cpf_cnpj AS VARCHAR), 11), 0)                       AS chave_principal,
             SEGA9169_processar_tb.prop_cliente_id,
             SEGA9169_processar_tb.proposta_id,
             SEGA9169_processar_tb.endosso_id
      FROM   interface_dados_db.dbo.SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK)
             INNER JOIN assistencia_db.dbo.proposta_assistencia_atual_tb AS proposta_assistencia_atual_tb WITH (NOLOCK)
                     ON SEGA9169_processar_tb.proposta_id = proposta_assistencia_atual_tb.proposta_id
             INNER JOIN seguros_db.dbo.endereco_corresp_tb AS endereco_corresp_tb WITH (NOLOCK)
                     ON endereco_corresp_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             INNER JOIN seguros_db.dbo.proposta_tb AS proposta_tb WITH (NOLOCK)
                     ON proposta_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             INNER JOIN seguros_db.dbo.apolice_tb AS apolice_tb WITH (NOLOCK)
                     ON apolice_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             INNER JOIN seguros_db.dbo.proposta_fechada_tb AS proposta_fechada_tb WITH (NOLOCK)
                     ON proposta_fechada_tb.proposta_id = SEGA9169_processar_tb.proposta_id
             INNER JOIN seguros_db.dbo.cliente_tb AS cliente_tb WITH (NOLOCK)
                     ON cliente_tb.cliente_id = SEGA9169_processar_tb.prop_cliente_id
             INNER JOIN #plano_assistencia2 AS plano_assistencia2
                     ON plano_assistencia2.plano_assistencia_id = SEGA9169_processar_tb.plano_assistencia_id
                        AND plano_assistencia2.produto_id = SEGA9169_processar_tb.produto_id

  /*Sergio - Nova Tendencia - 18234489 - Mudan�a de escopo - Valor do limite funeral - Inicio*/
      --Inclus�o - Titular
      UPDATE SEGA9169_10_processar_tb_novos_produtos
      SET    [val_lim_funeral] = escolha_plano_tp_cob_tb.val_is
      FROM   [#temp_SEGA9169_10_processar_tb_novos_produtos] SEGA9169_10_processar_tb_novos_produtos
             INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[prop_cliente_id] = proposta_tb.prop_cliente_id
             INNER JOIN seguros_db.dbo.escolha_plano_tp_cob_tb escolha_plano_tp_cob_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = escolha_plano_tp_cob_tb.proposta_id
             INNER JOIN seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb WITH (NOLOCK)
                     ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id
      WHERE  tp_cob_comp_tb.tp_cobertura_id = 819
             AND SEGA9169_10_processar_tb_novos_produtos.[tp_operacao] = 'I'
             AND tp_cob_comp_tb.tp_componente_id = 1
             AND escolha_plano_tp_cob_tb.dt_escolha <= proposta_tb.dt_contratacao
             AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= proposta_tb.dt_contratacao

      --Inclus�o - Conjuge
      UPDATE SEGA9169_10_processar_tb_novos_produtos
      SET    [val_lim_funeral] = escolha_plano_tp_cob_tb.val_is
      FROM   [#temp_SEGA9169_10_processar_tb_novos_produtos] SEGA9169_10_processar_tb_novos_produtos
             INNER JOIN seguros_db.dbo.proposta_complementar_tb proposta_complementar_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_complementar_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[prop_cliente_id] = proposta_complementar_tb.prop_cliente_id
             INNER JOIN seguros_db.dbo.escolha_plano_tp_cob_tb escolha_plano_tp_cob_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = escolha_plano_tp_cob_tb.proposta_id
             INNER JOIN seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb WITH (NOLOCK)
                     ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id
      WHERE  tp_cob_comp_tb.tp_cobertura_id = 819
             AND SEGA9169_10_processar_tb_novos_produtos.[tp_operacao] = 'I'
             AND tp_cob_comp_tb.tp_componente_id = 3
             AND escolha_plano_tp_cob_tb.dt_escolha <= proposta_complementar_tb.dt_inicio_vigencia
             AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= proposta_complementar_tb.dt_inicio_vigencia

      --Altera��o - Titular
      UPDATE SEGA9169_10_processar_tb_novos_produtos
      SET    [val_lim_funeral] = escolha_plano_tp_cob_tb.val_is
      FROM   [#temp_SEGA9169_10_processar_tb_novos_produtos] SEGA9169_10_processar_tb_novos_produtos
             INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[prop_cliente_id] = proposta_tb.prop_cliente_id
             INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = endosso_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[endosso_id] = endosso_tb.endosso_id
             INNER JOIN seguros_db.dbo.escolha_plano_tp_cob_tb escolha_plano_tp_cob_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = escolha_plano_tp_cob_tb.proposta_id
             INNER JOIN seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb WITH (NOLOCK)
                     ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id
      WHERE  tp_cob_comp_tb.tp_cobertura_id = 819
             AND SEGA9169_10_processar_tb_novos_produtos.[tp_operacao] = 'A'
             AND tp_cob_comp_tb.tp_componente_id = 1
             AND escolha_plano_tp_cob_tb.dt_escolha <= endosso_tb.dt_pedido_endosso
             AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= endosso_tb.dt_pedido_endosso

      --Altera��o - Conjuge
      UPDATE SEGA9169_10_processar_tb_novos_produtos
      SET    [val_lim_funeral] = escolha_plano_tp_cob_tb.val_is
      FROM   [#temp_SEGA9169_10_processar_tb_novos_produtos] SEGA9169_10_processar_tb_novos_produtos
             INNER JOIN seguros_db.dbo.proposta_complementar_tb proposta_complementar_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_complementar_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[prop_cliente_id] = proposta_complementar_tb.prop_cliente_id
             INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = endosso_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[endosso_id] = endosso_tb.endosso_id
             INNER JOIN seguros_db.dbo.escolha_plano_tp_cob_tb escolha_plano_tp_cob_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = escolha_plano_tp_cob_tb.proposta_id
             INNER JOIN seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb WITH (NOLOCK)
                     ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id
      WHERE  tp_cob_comp_tb.tp_cobertura_id = 819
             AND SEGA9169_10_processar_tb_novos_produtos.[tp_operacao] = 'A'
             AND tp_cob_comp_tb.tp_componente_id = 3
             AND escolha_plano_tp_cob_tb.dt_escolha <= endosso_tb.dt_pedido_endosso
             AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= endosso_tb.dt_pedido_endosso

      --Exclus�o - Titular
      UPDATE SEGA9169_10_processar_tb_novos_produtos
      SET    [val_lim_funeral] = escolha_plano_tp_cob_tb.val_is
      FROM   [#temp_SEGA9169_10_processar_tb_novos_produtos] SEGA9169_10_processar_tb_novos_produtos
             INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[prop_cliente_id] = proposta_tb.prop_cliente_id
             LEFT JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)
                    ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = endosso_tb.proposta_id
                       AND SEGA9169_10_processar_tb_novos_produtos.[endosso_id] = endosso_tb.endosso_id
             LEFT JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK)
                    ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_adesao_tb.proposta_id
             LEFT JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK)
                    ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_fechada_tb.proposta_id
             INNER JOIN seguros_db.dbo.escolha_plano_tp_cob_tb escolha_plano_tp_cob_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = escolha_plano_tp_cob_tb.proposta_id
             INNER JOIN seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb WITH (NOLOCK)
                     ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id
      WHERE  tp_cob_comp_tb.tp_cobertura_id = 819
             AND SEGA9169_10_processar_tb_novos_produtos.[tp_operacao] = 'E'
             AND tp_cob_comp_tb.tp_componente_id = 1
             AND ( ( escolha_plano_tp_cob_tb.dt_escolha <= endosso_tb.dt_pedido_endosso
                     AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= endosso_tb.dt_pedido_endosso )
                    OR ( escolha_plano_tp_cob_tb.dt_escolha <= Isnull(proposta_fechada_tb.dt_fim_vig, proposta_adesao_tb.dt_fim_vigencia)
                         AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= Isnull(proposta_fechada_tb.dt_fim_vig, proposta_adesao_tb.dt_fim_vigencia) ) )

      --Exclus�o - Conjuge
      UPDATE SEGA9169_10_processar_tb_novos_produtos
      SET    [val_lim_funeral] = escolha_plano_tp_cob_tb.val_is
      FROM   [#temp_SEGA9169_10_processar_tb_novos_produtos] SEGA9169_10_processar_tb_novos_produtos
             INNER JOIN seguros_db.dbo.proposta_complementar_tb proposta_complementar_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_complementar_tb.proposta_id
                        AND SEGA9169_10_processar_tb_novos_produtos.[prop_cliente_id] = proposta_complementar_tb.prop_cliente_id
             LEFT JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)
                    ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = endosso_tb.proposta_id
                       AND SEGA9169_10_processar_tb_novos_produtos.[endosso_id] = endosso_tb.endosso_id
             LEFT JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK)
                    ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_adesao_tb.proposta_id
             LEFT JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK)
                    ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = proposta_fechada_tb.proposta_id
             INNER JOIN seguros_db.dbo.escolha_plano_tp_cob_tb escolha_plano_tp_cob_tb WITH (NOLOCK)
                     ON SEGA9169_10_processar_tb_novos_produtos.[proposta_id] = escolha_plano_tp_cob_tb.proposta_id
             INNER JOIN seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb WITH (NOLOCK)
                     ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id
      WHERE  tp_cob_comp_tb.tp_cobertura_id = 819
             AND SEGA9169_10_processar_tb_novos_produtos.[tp_operacao] = 'E'
             AND tp_cob_comp_tb.tp_componente_id = 3
             AND ( ( escolha_plano_tp_cob_tb.dt_escolha <= endosso_tb.dt_pedido_endosso
                     AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= endosso_tb.dt_pedido_endosso )
                    OR ( escolha_plano_tp_cob_tb.dt_escolha <= Isnull(proposta_fechada_tb.dt_fim_vig, proposta_adesao_tb.dt_fim_vigencia)
                         AND Isnull(escolha_plano_tp_cob_tb.dt_fim_vigencia_cob, @dt_sistema) >= Isnull(proposta_fechada_tb.dt_fim_vig, proposta_adesao_tb.dt_fim_vigencia) ) )

      /*Sergio - Nova Tendencia - 18234489 - Mudan�a de escopo - Valor do limite funeral - Fim*/
      INSERT INTO interface_dados_db.dbo.SEGA9169_10_processar_tb
                  (sega9169_processar_id,
                   processar_id,
                   num_contrato,
                   versao_contrato,
                   apolice_id,
                   proposta_externa,
                   tp_movimento,
                   nome_usuario,
                   dt_ini_vigencia,
                   dt_fim_vigencia,
                   cnpj,
                   cpf,
                   endereco_usuario,
                   uf_usuario,
                   municipio_usuario,
                   bairro_usuario,
                   cep_usuario,
                   telefone_usuario,
                   endereco_risco,
                   uf_risco,
                   municipio_risco,
                   bairro_risco,
                   cep_risco,
                   telefone_risco,
                   placa_veiculo,
                   chassi_veiculo,
                   cor_veiculo,
                   ano_fab_veiculo,
                   modelo_veiculo,
                   marca_veiculo,
                   dias_reserva_pparcial,
                   dias_reserva_ptotal,
                   dias_reserva_roubo,
                   nome_estipulante,
                   parentesco,
                   val_lim_funeral,
                   val_lim_dmho,
                   qtd_cestas,
                   val_cestas,
                   dt_nasc_usuario,
                   dt_inclusao,
                   dt_alteracao,
                   usuario,
                   chave_principal)
      SELECT sega9169_processar_id,
             processar_id,
             num_contrato,
             num_versao_contrato,
             apolice_id,
             proposta_externa,
             tp_operacao,
             nome_segurado,
             dt_inicio_vigencia,
             dt_fim_vigencia,
             cpf_cnpj_estipulante,
             cpf_segurado,
             endereco,
             estado,
             municipio,
             bairro,
             cep,
             telefone,
             endereco_risco,
             uf_risco,
             municipio_risco,
             bairro_risco,
             cep_risco,
             telefone_risco,
             placa_veiculo,
             chassi_veiculo,
             cor_veiculo,
             ano_fab_veiculo,
             modelo_veiculo,
             marca_veiculo,
             dias_reserva_pparcial,
             dias_reserva_ptotal,
             dias_reserva_roubo,
             nome_estipulante,
             parentesco,
             val_lim_funeral,
             val_lim_dmho,
             qtd_cestas,
             val_cestas,
             dt_nasc_usuario,
             dt_inclusao,
             dt_alteracao,
             usuario,
             chave_principal
      FROM   #temp_SEGA9169_10_processar_tb_novos_produtos

	  --RAFAEL INACIO - IM00117493
      --UPDATE a
      --SET    tp_movimento_id = 1
      --FROM   assistencia_db.dbo.movimento_assistencia_atual_tb a
      --WHERE  EXISTS (SELECT proposta_id
      --               FROM   interface_dados_db.dbo.sega9169_processar_tb b WITH (NOLOCK)
      --               WHERE  a.proposta_id = b.proposta_id
      --                      AND b.produto_id NOT IN ( 1235, 1236, 1237 ))

      --MU-2017-021041 - Mudan�a de escopo Assist�ncia --2017-05-29 -fim
      SET NOCOUNT OFF

      RETURN
  END TRY

  BEGIN CATCH
      DECLARE @ErrorMessage NVARCHAR(4000)
      DECLARE @ErrorSeverity INT
      DECLARE @ErrorState INT

      SELECT @ErrorMessage = Error_procedure() + ' - Linha '
                             + CONVERT(VARCHAR(15), Error_line()) + ' - '
                             + Error_message(),
             @ErrorSeverity = Error_severity(),
             @ErrorState = Error_state()

      -- Use RAISERROR inside the CATCH block to return error    
      -- information about the original error that caused    
      -- execution to jump to the CATCH block.    
      RAISERROR (@ErrorMessage,-- Message text.    
                 @ErrorSeverity,-- Severity.    
                 @ErrorState -- State.    
      )
  END CATCH 

