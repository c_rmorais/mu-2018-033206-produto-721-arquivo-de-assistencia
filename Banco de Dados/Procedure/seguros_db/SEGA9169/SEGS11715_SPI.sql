CREATE PROCEDURE dbo.Segs11715_spi @layout_id  INT,
                               @produto_id INT,
                               @usuario    VARCHAR(20),
                               @producao   CHAR(1) = 'N'
AS
    --------------------------------------------------------                  
    -- Procedure principal de gera��o do arquivo SEGA9169 --                  
    --------------------------------------------------------                  
BEGIN

	SET NOCOUNT ON

  BEGIN TRY
      DECLARE @dt_sistema SMALLDATETIME

      SELECT @dt_sistema = dt_operacional
      FROM   seguros_db.dbo.parametro_geral_tb WITH (NOLOCK)

      INSERT INTO interface_dados_db..SEGA9169_processar_tb
                  (layout_id,
                   proposta_bb,
                   proposta_id,
                   prop_cliente_id,
                   certificado_id,
                   apolice_id,
                   sub_grupo_id,
                   produto_id,
                   ramo_id,
                   plano_assistencia_id,
                   tp_movimento_id,
                   usuario,
                   dt_inclusao,
                   dt_alteracao,
                   num_solicitacao)
      SELECT @layout_id,--layout_id            
             assistencia_db..movimento_assistencia_vida_atual_tb.proposta_bb,
             assistencia_db..movimento_assistencia_vida_atual_tb.proposta_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.prop_cliente_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.certificado_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.apolice_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.sub_grupo_id,
             seguros_db..proposta_tb.produto_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.ramo_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.plano_assistencia_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.tp_movimentacao_id,
             assistencia_db..movimento_assistencia_vida_atual_tb.usuario,
             assistencia_db..movimento_assistencia_vida_atual_tb.dt_inclusao,
             assistencia_db..movimento_assistencia_vida_atual_tb.dt_alteracao,
             0
      FROM   assistencia_db..movimento_assistencia_vida_atual_tb WITH (nolock)
             INNER JOIN seguros_db..proposta_tb WITH (nolock)
                     ON assistencia_db..movimento_assistencia_vida_atual_tb.proposta_id = seguros_db..proposta_tb.proposta_id
             INNER JOIN seguros_db..produto_tb WITH (nolock)
                     ON seguros_db..proposta_tb.produto_id = seguros_db..produto_tb.produto_id
      WHERE  assistencia_db..movimento_assistencia_vida_atual_tb.plano_assistencia_id = 2904 --Assistencia Escolar          
             --and seguros_db..proposta_tb.produto_id in (115,123,150)       
             --and assistencia_db..movimento_assistencia_vida_atual_tb.layout_id = @layout_id            
             AND assistencia_db..movimento_assistencia_vida_atual_tb.enviar = 'S'
             AND assistencia_db..movimento_assistencia_vida_atual_tb.layout_id IS NULL
             AND assistencia_db..movimento_assistencia_vida_atual_tb.versao IS NULL

      INSERT INTO interface_dados_db..SEGA9169_processar_tb
                  (layout_id,
                   proposta_bb,
                   proposta_id,
                   prop_cliente_id,
                   certificado_id,
                   apolice_id,
                   sub_grupo_id,
                   produto_id,
                   ramo_id,
                   plano_assistencia_id,
                   tp_movimento_id,
                   usuario,
                   dt_inclusao,
                   dt_alteracao,
                   num_solicitacao)
      SELECT @layout_id,
             pf.proposta_bb,
             maa.proposta_id,
             maa.cliente_id,
             NULL AS certificado_id,
             a.apolice_id,
             0    AS sub_grupo_id,
             maa.produto_id,
             a.ramo_id,
             paa.plano_assistencia_id,
             maa.tp_movimento_id,
             maa.usuario,
             maa.dt_inclusao,
             maa.dt_alteracao,
             0    AS num_solicitacao
      FROM   assistencia_db..movimento_Assistencia_atual_tb maa WITH (nolock)
             JOIN assistencia_db..proposta_assistencia_atual_Tb paa WITH (nolock)
               ON paa.proposta_id = maa.proposta_id
             JOIN seguros_db..proposta_tb p WITH (nolock)
               ON maa.proposta_id = p.proposta_id
             JOIN seguros_db..proposta_fechada_tb pf WITH (nolock)
               ON maa.proposta_id = pf.proposta_id
             JOIN seguros_db..apolice_tb a WITH (nolock)
               ON a.proposta_id = maa.proposta_id
      WHERE  maa.produto_id = 1206
             AND paa.plano_assistencia_id IN ( 3479, 3481 )
             AND maa.dt_envio IS NULL
      UNION
      SELECT @layout_id,
             pf.proposta_bb,
             maa.proposta_id,
             maa.cliente_id,
             NULL AS certificado_id,
             a.apolice_id,
             0    AS sub_grupo_id,
             maa.produto_id,
             a.ramo_id,
             paa.plano_assistencia_id,
             maa.tp_movimento_id,
             maa.usuario,
             maa.dt_inclusao,
             maa.dt_alteracao,
             0    AS num_solicitacao
      FROM   assistencia_db..movimento_Assistencia_atual_tb maa WITH (nolock)
             JOIN assistencia_db..proposta_assistencia_atual_Tb paa WITH (nolock)
               ON paa.proposta_id = maa.proposta_id
             JOIN seguros_db..proposta_tb p WITH (nolock)
               ON maa.proposta_id = p.proposta_id
             JOIN seguros_db..proposta_fechada_tb pf WITH (nolock)
               ON maa.proposta_id = pf.proposta_id
             JOIN seguros_db..apolice_tb a WITH (nolock)
               ON a.proposta_id = maa.proposta_id
      WHERE  maa.produto_id = 1206
             AND paa.plano_assistencia_id IN ( 3479, 3481 )
             --Rafael Inacio - IM00117493
			 --AND maa.dt_envio IS NOT NULL
			 AND maa.dt_envio IS NULL
             AND maa.tp_movimento_id IN ( 2, 3 )

      --and maa.dt_alteracao <> maa.dt_inclusao
      --MU-2017-021041 - Mudan�a de escopo Assist�ncia --2017-05-22 -in�cio
      DECLARE @inclusao  INT = 1,
              @exclusao  INT = 2,
              @alteracao INT = 3

      IF Object_id('tempdb..#plano_assistencia') IS NOT NULL
        BEGIN
            DROP TABLE #plano_assistencia
        END

      IF Object_id('tempdb..#gerarsega') IS NOT NULL
        BEGIN
            DROP TABLE #gerarsega
        END

      IF Object_id('tempdb..#componente_enviar') IS NOT NULL
        BEGIN
            DROP TABLE #componente_enviar
        END

      CREATE TABLE #plano_assistencia
        (
           id                   INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
           tp_assistencia_id    INT NULL,
           plano_assistencia_id INT NULL,
           produto_id           INT NULL,
           plano_id             INT NULL
        )

      INSERT INTO #plano_assistencia
                  (tp_assistencia_id,
                   plano_assistencia_id,
                   produto_id,
                   plano_id)
      SELECT 74                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1235               produto_id,
             1                  plano_id
      UNION
      SELECT 75                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1235               produto_id,
             2                  plano_id
      UNION
      SELECT 76                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1235               produto_id,
             3                  plano_id
      UNION
      SELECT 77                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1236               produto_id,
             1                  plano_id
      UNION
      SELECT 78                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1237               produto_id,
             1                  plano_id
      UNION
      SELECT 79                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1237               produto_id,
             2                  plano_id
      UNION
      SELECT 80                 tp_assistencia_id,
             CONVERT(INT, NULL) plano_assistencia_id,
             1237               produto_id,
             3                  plano_id

      --atualizando plano de assist�ncia
      UPDATE a
      SET    plano_assistencia_id = b.plano_assistencia_id
      FROM   #plano_assistencia a
             INNER JOIN assistencia_db.dbo.plano_assistencia_tb b WITH (NOLOCK)
                     ON a.tp_assistencia_id = b.tipo_assistencia_id
      WHERE  Isnull(b.DT_FIM_VIGENCIA, Dateadd(DAY, 1, Getdate())) > Getdate()

      --registros para enviar.
      CREATE TABLE #gerarsega
        (
           id                         INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
           proposta_id                NUMERIC (15, 0) NULL,
           tp_assistencia_id          INT NULL,
           tp_assistencia_id_anterior INT NULL,
           plano_assistencia_id       INT NULL,
           plano_id                   INT NULL,
           produto_id                 INT NULL,
           endosso_id                 INT NULL,
           tp_endosso_id              INT NULL,
           dt_pedido_endosso          SMALLDATETIME NULL,
           tp_movimento_id            INT NULL
        )

      INSERT INTO #gerarsega
                  (proposta_id,
                   tp_assistencia_id,
                   tp_assistencia_id_anterior,
                   plano_assistencia_id,
                   plano_id,
                   produto_id,
                   endosso_id,
                   tp_endosso_id,
                   dt_pedido_endosso,
                   tp_movimento_id)
      SELECT a.proposta_id,
             a.tp_assistencia_id,
             a.tp_assistencia_id tp_assistencia_id_anterior,
             b.plano_assistencia_id,
             b.plano_id,
             b.produto_id,
             a.endosso_id,
             c.tp_endosso_id,
             c.dt_pedido_endosso,
             a.tp_movimento_id
      FROM   assistencia_db.dbo.movimento_assistencia_atual_tb a WITH (NOLOCK)
             INNER JOIN #plano_assistencia b WITH (NOLOCK)
                     ON a.tp_assistencia_id = b.tp_assistencia_id
                        AND a.produto_id = b.produto_id
             LEFT JOIN seguros_db.dbo.endosso_tb c WITH (NOLOCK)
                    ON a.proposta_id = c.proposta_id
                       AND a.endosso_id = c.endosso_id
      WHERE  a.dt_envio IS NULL

      -->identificar componente para enviar
      --componente titular-
      CREATE TABLE #componente_enviar
        (
           id               INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
           prop_cliente_id  INT NULL,
           proposta_id      INT NULL,
           tp_componente_id INT NULL
        )

      INSERT INTO #componente_enviar
                  (prop_cliente_id,
                   proposta_id,
                   tp_componente_id)
      SELECT DISTINCT a.prop_cliente_id,
                      a.proposta_id,
                      1 tp_componente_id
      FROM   seguros_db.dbo.proposta_tb a WITH(NOLOCK)
             INNER JOIN #gerarsega b
                     ON a.proposta_id = b.proposta_id
      WHERE  Isnull(b.tp_endosso_id, 0) NOT IN ( 30, 31, 13 )
      UNION
      --componentes da proposta --tratamento c�njuge - endosso
      SELECT DISTINCT a.prop_cliente_id,
                      a.proposta_id,
                      3 tp_componente_id
      FROM   seguros_db.dbo.proposta_complementar_tb a WITH(NOLOCK)
             INNER JOIN #gerarsega b
                     ON a.proposta_id = b.proposta_id
                        AND b.dt_pedido_endosso BETWEEN a.dt_inicio_vigencia AND Isnull(a.dt_fim_vigencia, Dateadd(DAY, 1, Getdate()))
      WHERE  b.endosso_id IS NOT NULL
      UNION
      --componentes da proposta --tratamento c�njuge - fim de vig�ncia  
      SELECT DISTINCT a.prop_cliente_id,
                      a.proposta_id,
                      3 tp_componente_id
      FROM   seguros_db.dbo.proposta_complementar_tb a WITH(NOLOCK)
             INNER JOIN #gerarsega b
                     ON a.proposta_id = b.proposta_id
                        AND @dt_sistema BETWEEN a.dt_inicio_vigencia AND Isnull(a.dt_fim_vigencia, Dateadd(DAY, 1, Getdate()))
      WHERE  b.endosso_id IS NULL
             AND b.tp_movimento_id = @exclusao
      UNION
      --componentes da proposta --tratamento c�njuge - contrata��o 
      SELECT DISTINCT a.prop_cliente_id,
                      a.proposta_id,
                      3 tp_componente_id
      FROM   seguros_db.dbo.proposta_complementar_tb a WITH(NOLOCK)
             INNER JOIN #gerarsega b
                     ON a.proposta_id = b.proposta_id
             INNER JOIN seguros_db.dbo.proposta_tb c WITH(NOLOCK)
                     ON a.proposta_id = c.proposta_id
      WHERE  c.dt_contratacao BETWEEN a.dt_inicio_vigencia AND Isnull(a.dt_fim_vigencia, Dateadd(DAY, 1, Getdate()))
             AND b.endosso_id IS NULL
             AND b.tp_movimento_id <> @exclusao

      UPDATE a
      SET    tp_movimento_id = @inclusao
      FROM   #gerarsega AS a
      WHERE  a.endosso_id IS NULL
             AND tp_movimento_id <> @exclusao

      UPDATE a
      SET    tp_movimento_id = @inclusao
      FROM   #gerarsega AS a
      WHERE  Isnull(a.tp_endosso_id, 0) IN ( ( 13 ), ( 266 ) )

      UPDATE a
      SET    tp_movimento_id = @alteracao
      FROM   #gerarsega AS a
      WHERE  Isnull(a.tp_endosso_id, 0) IN ( ( 51 ), ( 308 ), ( 314 ) )

      UPDATE a
      SET    tp_movimento_id = @exclusao
      FROM   #gerarsega AS a
      WHERE  Isnull(a.tp_endosso_id, 0) IN ( ( 30 ), ( 31 ), ( 63 ), ( 64 ),
                                             ( 68 ), ( 90 ), ( 92 ), ( 350 ), ( 37 ) )

      --UPDATE #gerarsega
      --SET tp_movimento_id = @exclusao 
      --WHERE endosso_id = 0
      --gravar a movimenta��o
      INSERT INTO interface_dados_db.dbo.SEGA9169_processar_tb
                  (layout_id,
                   proposta_bb,
                   proposta_id,
                   prop_cliente_id,
                   certificado_id,
                   apolice_id,
                   sub_grupo_id,
                   produto_id,
                   ramo_id,
                   plano_assistencia_id,
                   tp_movimento_id,
                   usuario,
                   dt_inclusao,
                   dt_alteracao,
                   num_solicitacao,
                   endosso_id)
      SELECT @layout_id,
             d.proposta_bb,
             a.proposta_id,
             b.prop_cliente_id,
             NULL AS certificado_id,
             e.apolice_id,
             0    AS sub_grupo_id,
             a.produto_id,
             c.ramo_id,
             a.plano_assistencia_id,
             a.tp_movimento_id,
             @usuario,
             Getdate(),
             NULL,
             0    AS num_solicitacao,
             a.endosso_id
      FROM   #gerarsega a
             INNER JOIN #componente_enviar b
                     ON a.proposta_id = b.proposta_id
             INNER JOIN seguros_db.dbo.proposta_tb c WITH (NOLOCK)
                     ON a.proposta_id = c.proposta_id
             INNER JOIN seguros_db.dbo.proposta_fechada_tb d WITH (NOLOCK)
                     ON c.proposta_id = d.proposta_id
             INNER JOIN seguros_db.dbo.apolice_tb e WITH (NOLOCK)
                     ON a.proposta_id = e.proposta_id

      --atualiza a nova assist�ncia para os novos produtos,quando o tipo de endosso for o 37
      UPDATE a
      SET    plano_id = b.plano_id
      FROM   #gerarsega a
             INNER JOIN seguros_db.dbo.escolha_plano_tb b WITH (NOLOCK)
                     ON a.proposta_id = b.proposta_id
                        AND a.endosso_id = b.endosso_id
      WHERE  Isnull(a.tp_endosso_id, 0) = 37

      UPDATE a
      SET    tp_assistencia_id = b.tp_assistencia_id,
             plano_assistencia_id = b.plano_assistencia_id,
             tp_movimento_id = @inclusao --inclus�o na nova assist�ncia
      FROM   #gerarsega a
             INNER JOIN #plano_assistencia b
                     ON a.produto_id = b.produto_id
                        AND a.plano_id = b.plano_id
      WHERE  Isnull(a.tp_endosso_id, 0) = 37

      --gerar um outro movimento, de inclus�o, quando o tipo de endosso for o 37.
      INSERT INTO interface_dados_db.dbo.SEGA9169_processar_tb
                  (layout_id,
                   proposta_bb,
                   proposta_id,
                   prop_cliente_id,
                   certificado_id,
                   apolice_id,
                   sub_grupo_id,
                   produto_id,
                   ramo_id,
                   plano_assistencia_id,
                   tp_movimento_id,
                   usuario,
                   dt_inclusao,
                   dt_alteracao,
                   num_solicitacao,
                   endosso_id)
      SELECT @layout_id,
             d.proposta_bb,
             a.proposta_id,
             b.prop_cliente_id,
             NULL AS certificado_id,
             e.apolice_id,
             0    AS sub_grupo_id,
             a.produto_id,
             c.ramo_id,
             a.plano_assistencia_id,
             a.tp_movimento_id,
             @usuario,
             Getdate(),
             NULL,
             0    AS num_solicitacao,
             a.endosso_id
      FROM   #gerarsega a
             INNER JOIN #componente_enviar b
                     ON a.proposta_id = b.proposta_id
             INNER JOIN seguros_db.dbo.proposta_tb c WITH (NOLOCK)
                     ON a.proposta_id = c.proposta_id
             INNER JOIN seguros_db.dbo.proposta_fechada_tb d WITH (NOLOCK)
                     ON c.proposta_id = d.proposta_id
             INNER JOIN seguros_db.dbo.apolice_tb e WITH (NOLOCK)
                     ON a.proposta_id = e.proposta_id
      WHERE  Isnull(a.tp_endosso_id, 0) = 37

      -->atualizar os envios
      --quando n�o � final de vig�ncia nem endosso 37.
      UPDATE a
      SET    dt_envio = Getdate(),
             tp_movimento_id = b.tp_movimento_id
      FROM   assistencia_db.dbo.movimento_assistencia_atual_tb a
             INNER JOIN #gerarsega b
                     ON a.proposta_id = b.proposta_id
                        AND a.tp_assistencia_id = b.tp_assistencia_id_anterior
      WHERE  Isnull(b.tp_endosso_id, 0) <> 37
             AND NOT ( a.endosso_id IS NULL
                       AND a.tp_movimento_id = @exclusao )

      --quando � final de vig�ncia, o movimento j� � atualizado na extra��o
      UPDATE a
      SET    dt_envio = Getdate()
      FROM   assistencia_db.dbo.movimento_assistencia_atual_tb a
             INNER JOIN #gerarsega b
                     ON a.proposta_id = b.proposta_id
                        AND a.tp_assistencia_id = b.tp_assistencia_id_anterior
      WHERE  b.endosso_id IS NULL
             AND b.tp_movimento_id = @exclusao

      --para o endosso 37, � preciso atualizar a nova assist�ncia.
      UPDATE a
      SET    dt_envio = Getdate(),
             tp_movimento_id = b.tp_movimento_id --inclus�o
             ,
             tp_assistencia_id = b.tp_assistencia_id
      FROM   assistencia_db.dbo.movimento_assistencia_atual_tb a
             INNER JOIN #gerarsega b
                     ON a.proposta_id = b.proposta_id
                        AND a.tp_assistencia_id = b.tp_assistencia_id_anterior
      WHERE  Isnull(b.tp_endosso_id, 0) = 37

      --MU-2017-021041 - Mudan�a de escopo Assist�ncia --2017-05-22 -fim
	  
-------------------------------------------------------------------------------------------- 
-- In�cio  - MU-2018-033206 - Produto 721 - Arquivo de Assist�ncia
-------------------------------------------------------------------------------------------- 

		 INSERT INTO interface_dados_db.dbo.SEGA9169_processar_tb 
				(layout_id  ,            
				 proposta_bb ,             
				 proposta_id ,            
				 prop_cliente_id ,             
				 certificado_id ,             
				 apolice_id ,            
				 sub_grupo_id ,             
				 produto_id ,             
				 ramo_id ,            
				 plano_assistencia_id ,             
				 tp_movimento_id ,             
				 usuario ,             
				 dt_inclusao ,             
				 dt_alteracao,
				 num_solicitacao)
		 SELECT 
				@layout_id,
				proposta_adesao_tb.proposta_bb,
				movimento_Assistencia_atual_tb.proposta_id,
				movimento_Assistencia_atual_tb.cliente_id,
				NULL as certificado_id,
				proposta_adesao_tb.apolice_id,
				0 as sub_grupo_id,
				movimento_Assistencia_atual_tb.produto_id,
				proposta_adesao_tb.ramo_id,
				plano_assistencia_tb.plano_assistencia_id,
				movimento_Assistencia_atual_tb.tp_movimento_id,
				movimento_Assistencia_atual_tb.usuario,
				movimento_Assistencia_atual_tb.dt_inclusao,
				movimento_Assistencia_atual_tb.dt_alteracao,
				0 as num_solicitacao
		   FROM assistencia_uss_db.dbo.movimento_Assistencia_atual_tb movimento_Assistencia_atual_tb with (nolock)
	 INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with (nolock)
			 ON movimento_Assistencia_atual_tb.proposta_id = proposta_adesao_tb.proposta_id
	 INNER JOIN assistencia_db.dbo.plano_assistencia_tb plano_assistencia_tb with (nolock)
			 ON plano_assistencia_tb.tipo_assistencia_id = movimento_Assistencia_atual_tb.tp_assistencia_id
		  WHERE movimento_Assistencia_atual_tb.produto_id = 721 
			AND plano_assistencia_tb.plano_assistencia_id in (4073,4074) 
			AND movimento_Assistencia_atual_tb.dt_envio is null
			AND movimento_Assistencia_atual_tb.tp_movimento_id in (1,2,3,7) 
			
-------------------------------------------------------------------------------------------- 
-- Fim  - MU-2018-033206 - Produto 721 - Arquivo de Assist�ncia
-------------------------------------------------------------------------------------------- 
	  
	  
      SET NOCOUNT OFF

      RETURN
  END TRY

  BEGIN CATCH
      DECLARE @ErrorMessage NVARCHAR(4000)
      DECLARE @ErrorSeverity INT
      DECLARE @ErrorState INT

      SELECT @ErrorMessage = Error_procedure() + ' - Linha '
                             + CONVERT(VARCHAR(15), Error_line()) + ' - '
                             + Error_message(),
             @ErrorSeverity = Error_severity(),
             @ErrorState = Error_state()

      -- Use RAISERROR inside the CATCH block to return error
      -- information about the original error that caused
      -- execution to jump to the CATCH block.
      RAISERROR (@ErrorMessage,-- Message text.
                 @ErrorSeverity,-- Severity.
                 @ErrorState -- State.
      )
  END CATCH 
END
GO

