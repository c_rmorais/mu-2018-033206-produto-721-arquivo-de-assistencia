CREATE PROCEDURE dbo.SEGS11742_SPI  
              
    @layout_id INT,                    
    @produto_id INT,                    
    @usuario VARCHAR(20),                    
    @producao CHAR(1) = 'N'                    
                    
AS 

--------------------------------------------------------                    
-- PARAMETROS PARA TESTE
-- DECLARE  
--  @layout_id INT,                    
--  @produto_id INT,                    
--  @usuario VARCHAR(20),                    
--  @producao CHAR(1)
--
-- SET @layout_id = 1692                    
-- SET @produto_id = NULL                   
-- SET @usuario = 'teste'
-- SET @producao = 'N'                
--------------------------------------------------------                    



--------------------------------------------------------                    
-- Procedure principal de geraÃ§Ã£o do arquivo sega9196 --                    
--------------------------------------------------------                    
BEGIN                    
SET NOCOUNT ON                    
                    
DECLARE @dt_sistema SMALLDATETIME                    
DECLARE @msg VARCHAR(100)                     
                    
 SELECT @dt_sistema = dt_operacional                    
   FROM seguros_db.dbo.parametro_geral_tb WITH(NOLOCK)                    
    
    
BEGIN TRY      
	    
		 INSERT INTO interface_dados_db..sega9196_processar_tb                    
	   (layout_id  ,              
	   proposta_bb ,               
	   proposta_id ,              
	   prop_cliente_id ,               
	   certificado_id ,               
	   apolice_id ,              
	   sub_grupo_id ,               
	   produto_id ,             
	   ramo_id ,              
	   plano_assistencia_id ,--               
	   tp_movimento_id ,  --             
	   usuario ,               
	   dt_inclusao ,               
	   dt_alteracao)              
SELECT
	   @layout_id --layout_id    
	   ,pf.proposta_bb       
	   ,sv.proposta_id 	
	   ,sv.prop_cliente_id 
	   ,sv.certificado_id       
	   ,sv.apolice_id    
	   ,sv.sub_grupo_id 
	   ,p.produto_id  
	   ,sv.ramo_id 
	   ,2904 --plano_assistencia_id 
	   ,1 AS tp_movimentacao_id  
	   ,@usuario
	   ,GETDATE()             
	   ,NULL
  FROM proposta_tb p WITH (NOLOCK)      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = p.proposta_id      
  JOIN cliente_tb cprop WITH (NOLOCK)      
    ON cprop.cliente_id = p.prop_cliente_id      
  JOIN apolice_tb a WITH (NOLOCK)      
    ON a.proposta_id = p.proposta_id      
   AND (a.dt_fim_vigencia IS NULL    
    OR (a.dt_fim_vigencia IS NOT NULL      
   AND a.dt_fim_vigencia >= @dt_sistema)) -- Apolice Vigente      
  JOIN sub_grupo_apolice_tb sa WITH (NOLOCK)      
    ON sa.seguradora_cod_susep = a.seguradora_cod_susep      
   AND sa.sucursal_seguradora_id = a.sucursal_seguradora_id      
   AND sa.ramo_id = a.ramo_id      
   AND sa.apolice_id = a.apolice_id      
   AND sa.dt_fim_vigencia_sbg IS NULL -- Subgrupos Ativos      
  JOIN assistencia_db..sub_grupo_assistencia_atual_tb sg WITH (NOLOCK)      
    ON sg.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sg.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sg.ramo_id = sa.ramo_id      
   AND sg.apolice_id = sa.apolice_id      
   AND sg.sub_grupo_id = sa.sub_grupo_id      
   AND sg.plano_assistencia_id = 2904 -- Assistencia Escolar      
   AND sg.dt_ini_assist_sbg IS NOT NULL      
   AND sg.dt_ini_assist_sbg <= @dt_sistema -- Vigente      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.seguradora_cod_susep = sa.seguradora_cod_susep        
   AND sv.sucursal_seguradora_id = sa.sucursal_seguradora_id        
   AND sv.ramo_id = sa.ramo_id      
   AND sv.apolice_id = sa.apolice_id      
   AND sv.sub_grupo_id = sa.sub_grupo_id      
   AND sv.dt_fim_vigencia_sbg IS NULL -- Vida Ativa      
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                    FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                     WHERE sv2.apolice_id = sv.apolice_id      
                                       AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                       AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                       AND sv2.ramo_id = sv.ramo_id      
                                       AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                       AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                       AND sv2.proposta_id = sv.proposta_id      
                                       AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                       AND sv2.tp_componente_id = sv.tp_componente_id      
                                       AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                     ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)      
    ON pe.pf_cliente_id = cvida.cliente_id   
  JOIN escolha_sub_grp_tp_cob_comp_tb es  
 ON es.apolice_id = sa.apolice_id    
   AND es.sucursal_seguradora_id = sa.sucursal_seguradora_id    
   AND es.seguradora_cod_susep = sa.seguradora_cod_susep    
   AND es.ramo_id = sa.ramo_id    
   AND es.sub_grupo_id = sa.sub_grupo_id   
  JOIN tp_cob_comp_tb cc WITH (NOLOCK)      
    ON cc.tp_cob_comp_id = es.tp_cob_comp_id    
   AND cc.tp_cobertura_id = 830 -- Cobertura DMHO       
 WHERE p.produto_id in (115,123,150)      
   AND p.ramo_id = 82      
   AND p.situacao = 'i'     
 
 
 
 -- alterações 18720075
INSERT INTO interface_dados_db..SEGA9196_processar_tb  
	(layout_id  ,            
	 proposta_bb ,             
	 proposta_id ,            
	 prop_cliente_id ,             
	 certificado_id ,             
	 apolice_id ,            
	 sub_grupo_id ,             
	 produto_id ,             
	 ramo_id ,            
	 plano_assistencia_id ,             
	 tp_movimento_id ,             
	 usuario ,             
	 dt_inclusao ,             
	 dt_alteracao,
	 num_solicitacao) 
	select 
			@layout_id,
			pf.proposta_bb,
			paa.proposta_id,
			p.prop_cliente_id as cliente_id,
			NULL as certificado_id,
			a.apolice_id,
			0 as sub_grupo_id,
			p.produto_id,
			a.ramo_id,
			paa.plano_assistencia_id,
			1 as tp_movimento_id,
			paa.usuario,
			getdate() as dt_inclusao,
			null  as dt_alteracao,
			0 as num_solicitacao
		from assistencia_db..proposta_assistencia_atual_tb paa with (nolock) --> ASSISTENCIA_DB
		join seguros_db..proposta_tb p  with (nolock)
			on paa.proposta_id = p.proposta_id
	join seguros_db..proposta_processo_susep_tb pps  with (nolock)
			on pps.proposta_id = paa.proposta_id
    join seguros_db..cliente_tb c  with (nolock)
			on c.cliente_id = p.prop_cliente_id
	join seguros_db..pessoa_juridica_tb pe with (nolock)
			on pe.pj_cliente_id = c.cliente_id
	join seguros_db..apolice_tb a with (nolock)
			on a.proposta_id = p.proposta_id
		   AND (a.dt_fim_vigencia IS NULL    
			OR (a.dt_fim_vigencia IS NOT NULL 
		   AND a.dt_fim_vigencia >= @dt_sistema)) -- Apolice Vigente        
	join seguros_db..proposta_fechada_tb pf with (nolock)
			on pf.proposta_id = p.proposta_id
	join assistencia_db..plano_assistencia_tb pa with (nolock)
		on pa.plano_assistencia_id = paa.plano_assistencia_id
	JOIN seguros_db..PROPOSTA_BASICA_TB WITH (NOLOCK)
		ON PROPOSTA_BASICA_TB.PROPOSTA_ID = PAA.PROPOSTA_ID
	where  p.produto_id =  1206
	 and paa.plano_assistencia_id in (3479, 3481)
	 and a.dt_fim_vigencia >= @dt_sistema
	 and p.situacao = 'i'	


--------------------------------------------------------------------------------------------     
-- inicio alterações assistencias para os produtos 1235, 1236 e 1237 - Demanda 18234489   
--------------------------------------------------------------------------------------------  
    
    --Planos de assitencia da demanda
    IF OBJECT_ID('tempdb..#plano_assistencia') IS NOT NULL
    BEGIN
        DROP TABLE #plano_assistencia
    END
      

    CREATE TABLE #plano_assistencia
      (id INT IDENTITY(1,1) PRIMARY KEY NOT NULL
      ,tp_assistencia_id INT NULL
      ,plano_assistencia_id	INT NULL
      ,produto_id INT NULL
      ,plano_id	INT NULL)
    
    INSERT INTO #plano_assistencia
     (tp_assistencia_id
     ,plano_assistencia_id
     ,produto_id
     ,plano_id)
				
    SELECT 74 tp_assistencia_id, CONVERT(INT,NULL) plano_assistencia_id, 1235 produto_id, 1 plano_id
    UNION
    SELECT 75 tp_assistencia_id, CONVERT(INT,NULL) plano_assistencia_id, 1235 produto_id, 2 plano_id
    UNION
    SELECT 76 tp_assistencia_id, CONVERT(INT,NULL) plano_assistencia_id, 1235 produto_id, 3 plano_id
    UNION
    SELECT 77 tp_assistencia_id, CONVERT(INT,NULL) plano_assistencia_id, 1236 produto_id, 1 plano_id
    UNION
    SELECT 78 tp_assistencia_id, CONVERT(INT,NULL) plano_assistencia_id, 1237 produto_id, 1 plano_id
    UNION
    SELECT 79 tp_assistencia_id, CONVERT(INT,NULL) plano_assistencia_id, 1237 produto_id, 2 plano_id
    UNION
    SELECT 80 tp_assistencia_id, CONVERT(INT,NULL) plano_assistencia_id, 1237 produto_id, 3 plano_id
    
    --Atualiza plano de assistencia 
    UPDATE a
       SET plano_assistencia_id = plano_assistencia_tb.plano_assistencia_id
      FROM #plano_assistencia  AS a
INNER JOIN assistencia_db.dbo.plano_assistencia_tb plano_assistencia_tb WITH (NOLOCK)
        ON a.tp_assistencia_id = plano_assistencia_tb.tipo_assistencia_id
     WHERE ISNULL(plano_assistencia_tb.DT_FIM_VIGENCIA,DATEADD(DAY,1,GETDATE())) > GETDATE()
     

    IF OBJECT_ID('tempdb..#SEGA9196_processar_tb') IS NOT NULL
    BEGIN
        DROP TABLE #SEGA9196_processar_tb
    END
    
    CREATE TABLE #SEGA9196_processar_tb(
        id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
        layout_id int NULL, 
	    proposta_bb [numeric](9, 0) NULL,
	    proposta_id [numeric](9, 0) NULL,
	    cliente_id [int] NULL,
	    certificado_id [int] NULL,
	    apolice_id [numeric](9, 0) NULL,
	    sub_grupo_id [int] NULL,
	    produto_id [int] NULL,
	    ramo_id [int] NULL,
	    plano_assistencia_id [int] NULL,
	    tp_movimento_id [int] NULL,
	    usuario [varchar](20) NULL,
	    dt_inclusao [datetime] NULL,
	    dt_alteracao [int] NULL,
	    num_solicitacao [int] NULL,
	    plano_id int NULL
    ) 


    INSERT INTO #SEGA9196_processar_tb
    (
        layout_id , 
	    proposta_bb ,
	    proposta_id ,
	    cliente_id ,
	    certificado_id ,
	    apolice_id ,
	    sub_grupo_id ,
	    produto_id ,
	    ramo_id ,
	    plano_assistencia_id ,
	    tp_movimento_id ,
	    usuario ,
	    dt_inclusao ,
	    dt_alteracao ,
	    num_solicitacao     
    )
    SELECT @layout_id,
           pf.proposta_bb,
           paa.proposta_id,
           p.prop_cliente_id AS cliente_id,
           NULL AS certificado_id,
           a.apolice_id,
           0 AS sub_grupo_id,
           p.produto_id,
           a.ramo_id,
           paa.plano_assistencia_id,
           1 AS tp_movimento_id,
           paa.usuario,
           GETDATE() AS dt_inclusao,
           NULL AS dt_alteracao,
           0 AS num_solicitacao
      FROM assistencia_db.dbo.proposta_assistencia_atual_tb paa WITH (NOLOCK) --> ASSISTENCIA_DB
INNER JOIN seguros_db.dbo.proposta_tb p  WITH (NOLOCK)
        ON paa.proposta_id = p.proposta_id
INNER JOIN seguros_db.dbo.apolice_tb a WITH (NOLOCK)
        ON a.proposta_id = p.proposta_id
INNER JOIN seguros_db.dbo.proposta_fechada_tb pf WITH (NOLOCK)
        ON pf.proposta_id = p.proposta_id
INNER JOIN #plano_assistencia AS plano_assistencia
        ON plano_assistencia.plano_assistencia_id = paa.plano_assistencia_id           
     WHERE p.produto_id IN (1235, 1236, 1237)
       AND (a.dt_fim_vigencia IS NULL OR(a.dt_fim_vigencia IS NOT NULL AND DATEADD(DAY,5,dt_fim_vigencia) > @dt_sistema)) -- Apolice Vigente (considerando como "vigente" as vencidas em menos de 5 dias)
       AND p.situacao = 'i'	
UNION
    SELECT @layout_id,
           pf.proposta_bb,
           paa.proposta_id,
           p.prop_cliente_id AS cliente_id,
           NULL AS certificado_id,
           a.apolice_id,
           0 AS sub_grupo_id,
           proposta_tb.produto_id,
           a.ramo_id,
           paa.plano_assistencia_id,
           1 AS tp_movimento_id,
           paa.usuario,
           GETDATE() AS dt_inclusao,
           NULL AS dt_alteracao,
           0 AS num_solicitacao
      FROM assistencia_db.dbo.proposta_assistencia_atual_tb paa WITH (NOLOCK) --> ASSISTENCIA_DB
INNER JOIN seguros_db.dbo.proposta_complementar_tb p  WITH (NOLOCK)
        ON paa.proposta_id = p.proposta_id
INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
        ON proposta_tb.proposta_id = p.proposta_id         
INNER JOIN seguros_db.dbo.apolice_tb a WITH (NOLOCK)
        ON a.proposta_id = p.proposta_id
INNER JOIN seguros_db.dbo.proposta_fechada_tb pf WITH (NOLOCK)
        ON pf.proposta_id = p.proposta_id
INNER JOIN #plano_assistencia AS plano_assistencia
        ON plano_assistencia.plano_assistencia_id = paa.plano_assistencia_id        
     WHERE proposta_tb.produto_id IN (1235, 1236, 1237)
       AND (a.dt_fim_vigencia IS NULL OR(a.dt_fim_vigencia IS NOT NULL AND DATEADD(DAY,5,a.dt_fim_vigencia) > @dt_sistema)) -- Apolice Vigente (considerando como "vigente" as vencidas em menos de 5 dias)                  
       AND proposta_tb.situacao = 'i'
       AND p.dt_fim_vigencia IS NULL	       
       
 
    UPDATE a
       SET plano_id = b.plano_id
      FROM #SEGA9196_processar_tb a
INNER JOIN seguros_db.dbo.escolha_plano_tb b WITH (NOLOCK)
        ON a.proposta_id = b.proposta_id
     WHERE b.dt_fim_vigencia IS NULL

    UPDATE a
       SET plano_assistencia_id = plano_assistencia_temp.plano_assistencia_id
      FROM #SEGA9196_processar_tb a 
INNER JOIN #plano_assistencia  AS plano_assistencia_temp
        ON plano_assistencia_temp.plano_id = a.plano_id
	   AND plano_assistencia_temp.produto_id = a.produto_id


    INSERT INTO interface_dados_db.dbo.SEGA9196_processar_tb
          (layout_id,
           proposta_bb,
           proposta_id,
           prop_cliente_id,
           certificado_id,
           apolice_id,
           sub_grupo_id,
           produto_id,
           ramo_id,
           plano_assistencia_id,
           tp_movimento_id,
           usuario,
           dt_inclusao,
           dt_alteracao,
           num_solicitacao)  
    SELECT  
            layout_id , 
            proposta_bb ,
            proposta_id ,
            cliente_id ,
            certificado_id ,
            apolice_id ,
            sub_grupo_id ,
            produto_id ,
            ramo_id ,
            plano_assistencia_id ,
            tp_movimento_id ,
            usuario ,
            dt_inclusao ,
            dt_alteracao ,
            num_solicitacao  
       FROM #SEGA9196_processar_tb

--------------------------------------------------------------------------------------------    
-- fim alteração assistencias para os produtos 1235, 1236 e 1237 - Demanda 18234489   
--------------------------------------------------------------------------------------------	

--------------------------------------------------------------------------------------------    
-- Início Alteração MU-2018-033206 - Produto 721 - Arquivo de Assistência   
--------------------------------------------------------------------------------------------
		
IF OBJECT_ID('TEMPDB..#PropostasEmitidas') IS NOT NULL DROP TABLE #PropostasEmitidas
	CREATE TABLE #PropostasEmitidas  
                (proposta_id NUMERIC(9) NOT NULL,
				 --cliente_id INT NOT NULL, 
				 --dt_fim_vigencia SMALLDATETIME NULL,  
                 )
     --inserindo propostas emitidas do produto 721
	 INSERT INTO #PropostasEmitidas (proposta_id)	
		  SELECT proposta_tb.proposta_id     
            FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)
		   WHERE proposta_tb.produto_id = 721 
		     AND proposta_tb.situacao = 'i'
			 
	--verificando ultimo endosso 250 das propostas  - INICIO
	IF OBJECT_ID('TEMPDB..#PropostasEmitidasComRenovacaoEndossos') IS NOT NULL DROP TABLE #PropostasEmitidasComRenovacaoEndossos
	CREATE TABLE #PropostasEmitidasComRenovacaoEndossos  
                (proposta_id NUMERIC(9) NOT NULL,
				 endosso_id INT NOT NULL,
				 seq_endosso INT NOT NULL )
	 INSERT INTO #PropostasEmitidasComRenovacaoEndossos (proposta_id, endosso_id, seq_endosso)	
		  SELECT p.proposta_id,
		         endosso_tb.endosso_id,
				 row_number() over(partition by p.proposta_id order by endosso_tb.ENDOSSO_ID desc) as seq_endosso
		    FROM #PropostasEmitidas p  
      INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)
			  ON p.proposta_id = endosso_tb.proposta_id
		   WHERE endosso_tb.tp_endosso_id = 250


		 IF OBJECT_ID('TEMPDB..#PropostasEmitidasComRenovacao') IS NOT NULL DROP TABLE #PropostasEmitidasComRenovacao
	CREATE TABLE #PropostasEmitidasComRenovacao  
                (proposta_id NUMERIC(9) NOT NULL,
				 endosso_id INT NOT NULL)
	 INSERT INTO #PropostasEmitidasComRenovacao (proposta_id, endosso_id)
		   SELECT PROPOSTA_ID, 
		          ENDOSSO_ID
		    FROM  #PropostasEmitidasComRenovacaoEndossos
			WHERE SEQ_ENDOSSO = 1
	--verificando ultimo endosso 250 das propostas  - FIM	   

		   
IF OBJECT_ID('TEMPDB..#PropostasAtivas') IS NOT NULL DROP TABLE #PropostasAtivas
	CREATE TABLE #PropostasAtivas  
                (proposta_id NUMERIC(9) NOT NULL,
				 dt_fim_vigencia SMALLDATETIME NULL  
                 )
     --Pegando vigencia de todas propostas do produto 721 emitidas		
	 INSERT INTO #PropostasAtivas (proposta_id,dt_fim_vigencia)						
		  SELECT p.proposta_id,
				 proposta_adesao.dt_fim_vigencia    
            FROM #PropostasEmitidas p WITH(NOLOCK)
	  INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao WITH(NOLOCK)
	          ON p.proposta_id = proposta_adesao.proposta_id

     --Atualizando vigência das propostas que sofreram endosso de renovação com a data de fim de vigencia do ultimo endosso 250.
          UPDATE PropostasAtivas
		     SET PropostasAtivas.dt_fim_vigencia = endosso_tb.dt_fim_vigencia_end   
            FROM #PropostasAtivas PropostasAtivas WITH(NOLOCK)
	  INNER JOIN #PropostasEmitidasComRenovacao PropostasEmitidasComRenovacao 
	          ON PropostasEmitidasComRenovacao.proposta_id = PropostasAtivas.proposta_id
	  INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)
	          ON PropostasEmitidasComRenovacao.proposta_id = endosso_tb.proposta_id
			 AND PropostasEmitidasComRenovacao.endosso_id = endosso_tb.endosso_id


		
		 
	INSERT INTO INTERFACE_DADOS_DB.DBO.SEGA9196_PROCESSAR_TB 
			  (
			   layout_id  ,            
			   proposta_bb ,             
			   proposta_id ,            
			   prop_cliente_id ,             
			   certificado_id ,             
			   apolice_id ,            
			   sub_grupo_id ,             
			   produto_id ,             
			   ramo_id ,            
			   plano_assistencia_id ,             
			   tp_movimento_id ,             
			   usuario ,             
			   dt_inclusao ,             
			   dt_alteracao,
			   num_solicitacao
			   ) 
		SELECT 
			   @layout_id,
			   proposta_adesao_tb.proposta_bb,
			   proposta_tb.proposta_id,
			   proposta_tb.prop_cliente_id as cliente_id,
			   NULL as certificado_id,
			   apolice_tb.apolice_id,
			   0 as sub_grupo_id,
			   proposta_tb.produto_id,
			   apolice_tb.ramo_id,
			   CASE plano_tb.tp_plano_id 
			   WHEN 53 THEN 4074
			   WHEN 54 THEN 4073
			   ELSE NULL END AS plano_assistencia_id,
			   1 AS tp_movimento_id,
			   @usuario,
			   getdate() as dt_inclusao,
			   null  as dt_alteracao,
			   0 as num_solicitacao
	      FROM #PropostasAtivas PropostasAtivas
	INNER JOIN seguros_db.dbo.proposta_tb proposta_tb  with (nolock)
	        ON PropostasAtivas.proposta_id = proposta_tb.proposta_id
	INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with (nolock)
			ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id	
	INNER JOIN seguros_db.dbo.apolice_tb apolice_tb WITH(NOLOCK)            
			ON apolice_tb.apolice_id             = proposta_adesao_tb.apolice_id            
		   AND apolice_tb.ramo_id                = proposta_adesao_tb.ramo_id            
		   AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id               
		   AND apolice_tb.seguradora_cod_susep   = proposta_adesao_tb.seguradora_cod_susep
	INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH(NOLOCK)    
			ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id    
	INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH(NOLOCK)    
			ON plano_tb.produto_id = escolha_plano_tb.produto_id    
		   AND plano_tb.plano_id = escolha_plano_tb.plano_id    
		   AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia   
		 WHERE plano_tb.tp_plano_id in (53,54)
		   AND escolha_plano_tb.dt_fim_vigencia IS NULL  
		   AND PropostasAtivas.dt_fim_vigencia >= @dt_sistema	
		   
--------------------------------------------------------------------------------------------    
-- Fim Alteração MU-2018-033206 - Produto 721 - Arquivo de Assistência   
--------------------------------------------------------------------------------------------	


	SET NOCOUNT OFF                      
	RETURN                      
                      
END TRY                                             
BEGIN CATCH

  DECLARE @ErrorMessage NVARCHAR(4000)
  DECLARE @ErrorSeverity INT
  DECLARE @ErrorState INT

  SELECT 
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE()

      -- Use RAISERROR inside the CATCH block to return error
      -- information about the original error that caused
      -- execution to jump to the CATCH block.
      RAISERROR (@ErrorMessage, -- Message text.
                 @ErrorSeverity, -- Severity.
                 @ErrorState -- State.
                 )

END CATCH  
END
GO




