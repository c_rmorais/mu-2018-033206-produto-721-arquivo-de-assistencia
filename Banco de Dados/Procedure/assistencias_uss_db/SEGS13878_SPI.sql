CREATE PROCEDURE dbo.SEGS13878_SPI  
 @usuario VARCHAR(20),    
 @dt_sistema SMALLDATETIME = NULL   
AS  
  
 /**************************************************************************************************/   
 /*Desenvolvedor : Ricardo Morais                                                                  */
 /*Empresa: Nova Tendencia    																	   */
 /*Projeto: MU-2018-033206 - Produto 721 - Arquivo de Assistência  								   */
 /*Alteração: Modificação da forma de extração da assistência obrigatória						   */
 /*Teste: assistencia_uss_db.dbo.SEGS13878_SPI usuario = 'TESTE', dt_sistema = '2018-07-18 00:00:00'    */
 /**************************************************************************************************/   

 
    /**********************************************/  
    /*                                            */  
    /*  BB Seguro Vida - Assistência Facultativa  */  
    /*                                            */  
    /**********************************************/  
BEGIN
SET NOCOUNT ON

    DECLARE @ramo_id INT,        
            @subramo_id NUMERIC(5),        
            @dt_inicio_vigencia_sbr SMALLDATETIME,        
            @produto_id INT,        
            @tp_assistencia_id TINYINT,        
            @dt_inicio_vigencia_ass SMALLDATETIME,        
            @dt_ultimo_processamento SMALLDATETIME,        
            @qtd_registros INT,        
            @dt_limite SMALLDATETIME   
    
	/* Define a data do sistema */    
    
      IF @dt_sistema IS NULL     
  SELECT @dt_sistema = DATEADD(DAY, -1, dt_operacional) FROM seguros_db.dbo.parametro_geral_tb WITH(NOLOCK)

    /* Define a data de limite */    

    SET @dt_limite = DATEADD(DAY, 1, @dt_sistema)  
   
	/* Seleciona os dados do subramo */    
   
    SELECT @ramo_id = ramo_id,  
           @subramo_id = subramo_id,  
           @dt_inicio_vigencia_sbr = dt_inicio_vigencia_sbr,  
           @produto_id = produto_id,  
           @tp_assistencia_id = tp_assistencia_id,  
           @dt_inicio_vigencia_ass = dt_inicio_vigencia_ass,  
           @dt_ultimo_processamento = DATEADD(DAY, 1, dt_ult_processamento)  
      FROM assistencia_uss_db.dbo.subramo_tp_assistencia_tb WITH(NOLOCK)
     WHERE ramo_id = 93  
       AND subramo_id = 9320  
       AND plano_assistencia_id = 4073   
  
    /* Cria tabela temporária de assistência */   
    IF OBJECT_ID('TEMPDB..#movimento_assistencia') IS NOT NULL DROP TABLE #movimento_assistencia
	CREATE TABLE #movimento_assistencia  
                (movimento_id INT IDENTITY(1,1),  
                 dt_registro_segbr SMALLDATETIME NULL,  
                 proposta_id NUMERIC(9) NOT NULL,  
                 endosso_id INT NULL,  
                 sinistro_id NUMERIC(11) NULL,  
                 nome VARCHAR(60) NULL,  
                 cpf_cnpj NUMERIC(14) NULL,  
                 dt_nascimento DATETIME NULL,  
                 sexo CHAR(1) NULL,  
                 endereco VARCHAR(100) NULL,  
                 cidade VARCHAR(60) NULL,  
                 bairro VARCHAR(60) NULL,  
                 uf CHAR(2) NULL,  
                 cep NUMERIC(8) NULL,  
                 cliente_id INT NULL,  
                 tp_movimento_id INT NULL,  
                 tp_movimento_saida_id INT NULL,  
                 dt_vigencia_assistencia SMALLDATETIME NULL,  
                 proposta_bb NUMERIC(9) NULL, 
                 apolice_id NUMERIC(9) NULL, 
                 certificado_id NUMERIC(9) NULL,  
                 produto_id INT NULL,  
                 ddd VARCHAR(4) NULL,  
                 telefone VARCHAR(9) NULL, 
                 endereco_risco VARCHAR(100) NULL,  
                 bairro_risco VARCHAR(60) NULL,  
                 cidade_risco VARCHAR(60) NULL,  
                 uf_risco CHAR(2) NULL,  
                 cep_risco NUMERIC(8) NULL)  
    
	/* Cria e popula tabela temporária com propostas emitidas para o produto */   
    IF OBJECT_ID('TEMPDB..#PropostasEmitidas') IS NOT NULL DROP TABLE #PropostasEmitidas
	CREATE TABLE #PropostasEmitidas 
                (proposta_id NUMERIC(9) NOT NULL)
	 INSERT INTO #PropostasEmitidas (proposta_id)			
		  SELECT proposta_tb.proposta_id     
            FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)
		   WHERE proposta_tb.produto_id = @produto_id 
		     AND proposta_tb.situacao = 'i'			
				 
	/* Cria e popula tabela temporária com propostas canceladas para o produto */   
    IF OBJECT_ID('TEMPDB..#PropostasCanceladas') IS NOT NULL DROP TABLE #PropostasCanceladas
	CREATE TABLE #PropostasCanceladas 
                (proposta_id NUMERIC(9) NOT NULL)
	 INSERT INTO #PropostasCanceladas (proposta_id)			
		  SELECT proposta_tb.proposta_id     
            FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)
		   WHERE proposta_tb.produto_id = @produto_id 
		     AND proposta_tb.situacao = 'c'						 
				 

	
    /* Seleciona as contratações de proposta */    
    IF OBJECT_ID('TEMPDB..#contratacao_proposta') IS NOT NULL DROP TABLE #contratacao_proposta
	CREATE TABLE #contratacao_proposta
				( proposta_id NUMERIC(9)NOT NULL,
				  dt_inicio_vigencia SMALLDATETIME NULL,
				  dt_contratacao SMALLDATETIME NOT NULL
				)
	INSERT INTO #contratacao_proposta (proposta_id,dt_inicio_vigencia,dt_contratacao)			
    SELECT proposta_adesao_tb.proposta_id,    
           proposta_adesao_tb.dt_inicio_vigencia,    
           proposta_tb.dt_contratacao       
      FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)
INNER JOIN #PropostasEmitidas PropostasEmitidas
        ON proposta_tb.proposta_id = PropostasEmitidas.proposta_id	 
INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH(NOLOCK)    
        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id    
INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH(NOLOCK)    
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id    
INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH(NOLOCK)    
        ON plano_tb.produto_id = escolha_plano_tb.produto_id    
       AND plano_tb.plano_id = escolha_plano_tb.plano_id    
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia   
     WHERE plano_tb.tp_plano_id = 54
	   AND escolha_plano_tb.dt_fim_vigencia IS NULL    
	   AND proposta_tb.dt_contratacao >= @dt_ultimo_processamento
       AND proposta_tb.dt_contratacao < @dt_limite    
    
    INSERT INTO #movimento_assistencia    
               (dt_registro_segbr,    
                proposta_id,    
                endosso_id,    
                sinistro_id,    
                nome,    
                cpf_cnpj,    
                dt_nascimento,    
                sexo,    
                endereco,    
                cidade,    
                bairro,    
                uf,    
                cep,    
                cliente_id,    
                tp_movimento_id,    
                tp_movimento_saida_id,    
                dt_vigencia_assistencia,    
                proposta_bb,    
                apolice_id,    
                certificado_id,    
                produto_id,    
                ddd,    
                telefone,    
                endereco_risco,    
                bairro_risco,    
                cidade_risco,    
                uf_risco,    
                cep_risco)    
         SELECT contratacao_proposta.dt_contratacao,    
                contratacao_proposta.proposta_id,    
                NULL,    
                NULL,    
                cliente_tb.nome,    
                pessoa_fisica_tb.cpf,    
                pessoa_fisica_tb.dt_nascimento,    
                pessoa_fisica_tb.sexo,    
                endereco_corresp_tb.endereco,    
                endereco_corresp_tb.municipio,    
                endereco_corresp_tb.bairro,    
                endereco_corresp_tb.estado,    
                endereco_corresp_tb.cep,    
                cliente_tb.cliente_id,    
                1,    
                1,    
                contratacao_proposta.dt_inicio_vigencia,    
                proposta_adesao_tb.proposta_bb,    
                proposta_adesao_tb.apolice_id,    
                certificado_tb.certificado_id,    
                proposta_tb.produto_id,    
                cliente_tb.ddd_1,    
                cliente_tb.telefone_1,    
                NULL,    
                NULL,    
                NULL,    
                NULL,    
                NULL    
           FROM #contratacao_proposta contratacao_proposta   
          INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)  
             ON proposta_tb.proposta_id = contratacao_proposta.proposta_id    
          INNER JOIN seguros_db.dbo.cliente_tb cliente_tb WITH(NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id    
          INNER JOIN seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb WITH(NOLOCK)   
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id    
          INNER JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH(NOLOCK)   
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id    
          INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH(NOLOCK)    
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id    
           LEFT JOIN seguros_db.dbo.certificado_tb certificado_tb WITH(NOLOCK) 
             ON certificado_tb.proposta_id = proposta_tb.proposta_id    
    
    /* Seleciona as inclusões de endosso de renovação */       
    IF OBJECT_ID('TEMPDB..#renovacao_proposta') IS NOT NULL DROP TABLE #renovacao_proposta
	CREATE TABLE #renovacao_proposta
				( proposta_id NUMERIC(9)NOT NULL,
				  endosso_id INT NOT NULL,
				  dt_pedido_endosso SMALLDATETIME NULL,
				  dt_inclusao SMALLDATETIME NOT NULL
				)
	INSERT INTO #renovacao_proposta (proposta_id,endosso_id,dt_pedido_endosso,dt_inclusao)			
    SELECT endosso_tb.proposta_id,    
           endosso_tb.endosso_id,    
           endosso_tb.dt_inicio_vigencia_end as dt_pedido_endosso,    
           endosso_tb.dt_inclusao      
      FROM #PropostasEmitidas PropostasEmitidas    
     INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb (NOLOCK)    
        ON escolha_plano_tb.proposta_id = PropostasEmitidas.proposta_id    
     INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH(NOLOCK)   
        ON plano_tb.produto_id = escolha_plano_tb.produto_id    
       AND plano_tb.plano_id = escolha_plano_tb.plano_id    
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia    
     INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)   
        ON endosso_tb.proposta_id = PropostasEmitidas.proposta_id    
     WHERE plano_tb.tp_plano_id = 54  
       AND endosso_tb.tp_endosso_id = 266   
       AND escolha_plano_tb.dt_fim_vigencia IS NULL    
       AND endosso_tb.dt_inclusao >= @dt_ultimo_processamento    
       AND endosso_tb.dt_inclusao < @dt_limite    
    
    INSERT INTO #movimento_assistencia    
               (dt_registro_segbr,    
                proposta_id,    
                endosso_id,    
                sinistro_id,    
                nome,    
                cpf_cnpj,    
                dt_nascimento,    
                sexo,    
                endereco,    
                cidade,    
                bairro,    
                uf,    
                cep,    
                cliente_id,    
                tp_movimento_id,    
                tp_movimento_saida_id,    
                dt_vigencia_assistencia,    
                proposta_bb,    
                apolice_id,    
                certificado_id,    
                produto_id,    
                ddd,    
                telefone,    
                endereco_risco,    
                bairro_risco,    
                cidade_risco,    
                uf_risco,    
                cep_risco)    
         SELECT renovacao_proposta.dt_inclusao,    
                renovacao_proposta.proposta_id,    
                renovacao_proposta.endosso_id,    
                NULL,    
                cliente_tb.nome,    
                pessoa_fisica_tb.cpf,    
                pessoa_fisica_tb.dt_nascimento,    
                pessoa_fisica_tb.sexo,    
                endereco_corresp_tb.endereco,    
                endereco_corresp_tb.municipio,    
                endereco_corresp_tb.bairro,    
                endereco_corresp_tb.estado,    
                endereco_corresp_tb.cep,    
                cliente_tb.cliente_id,    
                3,    
                4,
                renovacao_proposta.dt_pedido_endosso,    
                proposta_adesao_tb.proposta_bb,    
                proposta_adesao_tb.apolice_id,    
                certificado_tb.certificado_id,    
                proposta_tb.produto_id,    
                cliente_tb.ddd_1,    
                cliente_tb.telefone_1,    
                NULL,    
                NULL,    
                NULL,    
                NULL,    
                NULL    
           FROM #renovacao_proposta renovacao_proposta    
          INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)    
             ON proposta_tb.proposta_id = renovacao_proposta.proposta_id    
          INNER JOIN seguros_db.dbo.cliente_tb cliente_tb WITH(NOLOCK)    
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id    
          INNER JOIN seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb WITH(NOLOCK)    
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id    
          INNER JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH(NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id    
          INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH(NOLOCK)    
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id    
           LEFT JOIN seguros_db.dbo.certificado_tb certificado_tb WITH(NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id    
  
  
    /* Seleciona os cancelamentos de proposta */       
    IF OBJECT_ID('TEMPDB..#cancelamento_proposta') IS NOT NULL DROP TABLE #cancelamento_proposta
			CREATE TABLE #cancelamento_proposta
				( proposta_id NUMERIC(9)NOT NULL,
				  endosso_id INT NOT NULL,
				  dt_pedido_endosso SMALLDATETIME NULL,
				  dt_inclusao SMALLDATETIME NOT NULL
				)
 INSERT INTO #cancelamento_proposta (proposta_id,endosso_id,dt_pedido_endosso,dt_inclusao)				
 SELECT    endosso_tb.proposta_id,    
           endosso_tb.endosso_id,    
           endosso_tb.dt_inicio_vigencia_end as dt_pedido_endosso,    
           endosso_tb.dt_inclusao      
      FROM #PropostasCanceladas PropostasCanceladas     
     INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH(NOLOCK)   
        ON escolha_plano_tb.proposta_id = PropostasCanceladas.proposta_id    
     INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH(NOLOCK)    
        ON plano_tb.produto_id = escolha_plano_tb.produto_id    
       AND plano_tb.plano_id = escolha_plano_tb.plano_id    
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia    
     INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)    
        ON endosso_tb.proposta_id = PropostasCanceladas.proposta_id    
     WHERE plano_tb.tp_plano_id = 54  
       AND endosso_tb.tp_endosso_id in (63,64,66,68,90,92) 
       AND escolha_plano_tb.dt_fim_vigencia IS NULL        
       AND endosso_tb.dt_inclusao >= @dt_ultimo_processamento    
       AND endosso_tb.dt_inclusao < @dt_limite  
  

    INSERT INTO #movimento_assistencia    
               (dt_registro_segbr,    
                proposta_id,    
                endosso_id,    
                sinistro_id,    
                nome,    
                cpf_cnpj,    
                dt_nascimento,    
                sexo,    
                endereco,    
                cidade,    
                bairro,    
                uf,    
                cep,    
                cliente_id,    
                tp_movimento_id,    
                tp_movimento_saida_id,    
                dt_vigencia_assistencia,    
                proposta_bb,    
                apolice_id,    
                certificado_id,    
                produto_id,    
                ddd,    
                telefone,    
                endereco_risco,    
                bairro_risco,    
                cidade_risco,    
                uf_risco,    
                cep_risco)    
         SELECT cancelamento_proposta.dt_inclusao,    
                proposta_tb.proposta_id,    
                cancelamento_proposta.endosso_id,    
                NULL,    
                cliente_tb.nome,    
                pessoa_fisica_tb.cpf,    
                pessoa_fisica_tb.dt_nascimento,    
                pessoa_fisica_tb.sexo,    
                endereco_corresp_tb.endereco,    
                endereco_corresp_tb.municipio,    
                endereco_corresp_tb.bairro,    
                endereco_corresp_tb.estado,    
                endereco_corresp_tb.cep,    
                cliente_tb.cliente_id,    
                2,    
                3,    
                cancelamento_proposta.dt_pedido_endosso,    
                proposta_adesao_tb.proposta_bb,    
                proposta_adesao_tb.apolice_id,    
                certificado_tb.certificado_id,    
                proposta_tb.produto_id,    
                cliente_tb.ddd_1,    
                cliente_tb.telefone_1,    
                NULL,    
                NULL,    
                NULL,    
                NULL,    
                NULL    
           FROM #cancelamento_proposta cancelamento_proposta   
          INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)   
             ON proposta_tb.proposta_id = cancelamento_proposta.proposta_id    
          INNER JOIN seguros_db.dbo.cliente_tb cliente_tb WITH(NOLOCK)    
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id    
          INNER JOIN seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb WITH(NOLOCK)    
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id    
          INNER JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH(NOLOCK) 
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id    
          INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH(NOLOCK)    
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id    
           LEFT JOIN seguros_db.dbo.certificado_tb certificado_tb WITH(NOLOCK)    
             ON certificado_tb.proposta_id = proposta_tb.proposta_id    
    
    /* Seleciona as alterações de endereço de proposta */    
    IF OBJECT_ID('TEMPDB..#alteracao_proposta') IS NOT NULL DROP TABLE #alteracao_proposta
	CREATE TABLE #alteracao_proposta
		( proposta_id NUMERIC(9)NOT NULL,
		  endosso_id INT NOT NULL,
		  dt_pedido_endosso SMALLDATETIME NULL,
		  dt_inclusao SMALLDATETIME NOT NULL
		)
    INSERT INTO #alteracao_proposta (proposta_id,endosso_id,dt_pedido_endosso,dt_inclusao)
	SELECT    
		   endosso_tb.proposta_id,    
           endosso_tb.endosso_id,    
           endosso_tb.dt_inicio_vigencia_end as dt_pedido_endosso,    
           endosso_tb.dt_inclusao      
      FROM #PropostasEmitidas PropostasEmitidas
     INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH(NOLOCK)   
        ON escolha_plano_tb.proposta_id = PropostasEmitidas.proposta_id    
     INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH(NOLOCK)    
        ON plano_tb.produto_id = escolha_plano_tb.produto_id    
       AND plano_tb.plano_id = escolha_plano_tb.plano_id    
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia    
     INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)    
        ON endosso_tb.proposta_id = PropostasEmitidas.proposta_id    
     WHERE plano_tb.tp_plano_id = 54  
       AND endosso_tb.tp_endosso_id in (1,250)    
       AND escolha_plano_tb.dt_fim_vigencia IS NULL
       AND endosso_tb.dt_inclusao >= @dt_ultimo_processamento    
       AND endosso_tb.dt_inclusao < @dt_limite   
  
  
    INSERT INTO #movimento_assistencia    
               (dt_registro_segbr,    
                proposta_id,    
                endosso_id,    
                sinistro_id,    
                nome,    
                cpf_cnpj,    
                dt_nascimento,    
                sexo,    
                endereco,    
                cidade,    
                bairro,    
                uf,    
                cep,    
                cliente_id,    
                tp_movimento_id,    
                tp_movimento_saida_id,    
                dt_vigencia_assistencia,    
                proposta_bb,    
                apolice_id,    
                certificado_id,    
                produto_id,    
                ddd,    
                telefone,    
                endereco_risco,    
                bairro_risco,    
                cidade_risco,    
                uf_risco,    
                cep_risco)    
         SELECT alteracao_proposta.dt_inclusao,    
                proposta_tb.proposta_id,    
                alteracao_proposta.endosso_id,    
                NULL,    
                cliente_tb.nome,    
                pessoa_fisica_tb.cpf,    
                pessoa_fisica_tb.dt_nascimento,    
                pessoa_fisica_tb.sexo,    
                endereco_corresp_tb.endereco,    
                endereco_corresp_tb.municipio,    
                endereco_corresp_tb.bairro,    
                endereco_corresp_tb.estado,    
                endereco_corresp_tb.cep,    
                cliente_tb.cliente_id,    
                7,    
                2,    
                alteracao_proposta.dt_pedido_endosso,    
                proposta_adesao_tb.proposta_bb,    
                proposta_adesao_tb.apolice_id,    
                certificado_tb.certificado_id,    
                proposta_tb.produto_id,    
                cliente_tb.ddd_1,    
                cliente_tb.telefone_1,    
                NULL,    
                NULL,    
                NULL,    
                NULL,    
                NULL    
           FROM #alteracao_proposta alteracao_proposta   
          INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)    
             ON proposta_tb.proposta_id = alteracao_proposta.proposta_id    
          INNER JOIN seguros_db.dbo.cliente_tb cliente_tb WITH(NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id    
          INNER JOIN seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb WITH(NOLOCK)   
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id    
          INNER JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH(NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id    
          INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH(NOLOCK)    
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id    
           LEFT JOIN seguros_db.dbo.certificado_tb certificado_tb WITH(NOLOCK)   
             ON certificado_tb.proposta_id = proposta_tb.proposta_id    
    
    /* Exclui todas as movimentações que já tenham sido gravadas na base de assistência */    
  
    DELETE  
      FROM #movimento_assistencia  
     WHERE EXISTS (SELECT 1  
                     FROM assistencia_uss_db.dbo.movimento_assistencia_tb movimento_assistencia_tb WITH(NOLOCK)  
                    WHERE movimento_assistencia_tb.proposta_id = #movimento_assistencia.proposta_id  
                      AND movimento_assistencia_tb.endosso_id IS NULL  
                      AND movimento_assistencia_tb.sinistro_id IS NULL  
                      AND movimento_assistencia_tb.tp_assistencia_id = @tp_assistencia_id  
                      AND movimento_assistencia_tb.tp_movimento_id = #movimento_assistencia.tp_movimento_id  
                      AND movimento_assistencia_tb.dt_registro_segbr = #movimento_assistencia.dt_registro_segbr)  
  
    DELETE  
      FROM #movimento_assistencia  
     WHERE EXISTS (SELECT 1  
                     FROM assistencia_uss_db.dbo.movimento_assistencia_tb  movimento_assistencia_tb WITH(NOLOCK)  
                    WHERE movimento_assistencia_tb.proposta_id = #movimento_assistencia.proposta_id  
                      AND movimento_assistencia_tb.endosso_id = #movimento_assistencia.endosso_id  
                      AND movimento_assistencia_tb.tp_assistencia_id = @tp_assistencia_id  
                      AND movimento_assistencia_tb.tp_movimento_id = #movimento_assistencia.tp_movimento_id  
                      AND movimento_assistencia_tb.dt_registro_segbr = #movimento_assistencia.dt_registro_segbr)  
  
  

	/* Insere as movimentações de assistência */  

    INSERT INTO assistencia_uss_db.dbo.movimento_assistencia_tb  
               (tp_assistencia_id,  
                dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                proposta_bb, 
				apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                ramo_id,  
                subramo_id,  
                dt_inicio_vigencia_sbr,  
                dt_inicio_vigencia_ass,  
                dt_vigencia_assistencia,  
                dt_inclusao,  
                usuario)  
         SELECT @tp_assistencia_id,  
                dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
				endereco_risco,  
                bairro_risco,  
				cidade_risco,  
                uf_risco,  
                cep_risco,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                @ramo_id,  
                @subramo_id,  
                @dt_inicio_vigencia_sbr,  
                @dt_inicio_vigencia_ass,  
                dt_vigencia_assistencia,  
                GETDATE(),  
                @usuario  
           FROM #movimento_assistencia
		   
			 IF @@ERROR <> 0                
			 BEGIN                
			  SELECT 0            
			  RAISERROR ('Erro ao gravar dados na tabela movimento_assistencia_tb.', 16, 1)                   
			  RETURN                
			 END 		   
    
    SET @qtd_registros = (SELECT COUNT(1)    
                            FROM #movimento_assistencia)    
    
    /* Insere as movimentações atuais de assistência */    
    
    DELETE    
      FROM #movimento_assistencia    
     WHERE EXISTS (SELECT 1    
                     FROM assistencia_uss_db.dbo.movimento_assistencia_atual_tb movimento_assistencia_atual_tb WITH(NOLOCK)      
                    WHERE movimento_assistencia_atual_tb.proposta_id = #movimento_assistencia.proposta_id    
                      AND movimento_assistencia_atual_tb.cliente_id = #movimento_assistencia.cliente_id    
                      AND movimento_assistencia_atual_tb.tp_assistencia_id = @tp_assistencia_id)    
    
    INSERT INTO assistencia_uss_db.dbo.movimento_assistencia_atual_tb    
               (proposta_id,    
                cliente_id,    
                tp_assistencia_id,
				tp_movimento_id,
                produto_id,    
                dt_inclusao,    
                usuario)    
         SELECT proposta_id,    
                cliente_id,    
                @tp_assistencia_id,
				tp_movimento_id,
                produto_id,    
                GETDATE(),    
                @usuario    
           FROM #movimento_assistencia    
          GROUP BY proposta_id,    
                   cliente_id,    
                   produto_id,
				   tp_movimento_id				   
    
			 IF @@ERROR <> 0                
			   BEGIN                
		         SELECT 0            
				 RAISERROR ('Erro ao gravar dados na tabela movimento_assistencia_atual_tb.', 16, 1)                   
		         RETURN                
		       END     
       
	
    /* Atualiza a última data de processamento do tipo de assistência */    
    
    UPDATE assistencia_uss_db.dbo.subramo_tp_assistencia_tb    
       SET dt_ult_processamento = @dt_sistema,    
           dt_alteracao = GETDATE(),    
           usuario = @usuario    
     WHERE tp_assistencia_id = @tp_assistencia_id    
       AND ramo_id = @ramo_id    
       AND subramo_id = @subramo_id    
       AND dt_inicio_vigencia_sbr = @dt_inicio_vigencia_sbr    
       AND dt_inicio_vigencia_ass = @dt_inicio_vigencia_ass    
		
	SET NOCOUNT OFF
		
		IF @@ERROR <> 0                
		BEGIN                
		 SELECT 0            
		 RAISERROR ('Erro ao atualizar dados na tabela subramo_tp_assistencia_tb.', 16, 1)                   
		 RETURN                
		END    

		
    /* Retorna a quantidade de movimentos extraídos */    
    
    SELECT @qtd_registros    
    
RETURN  
END
GO

  