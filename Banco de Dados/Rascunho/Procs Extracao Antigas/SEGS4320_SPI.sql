CREATE PROCEDURE SEGS4320_SPI  
@dt_sistema SMALLDATETIME,  
@usuario VARCHAR(20)  
  
AS  
  
    /**********************************************/  
    /*                                            */  
    /*  BB Seguro Vida - Assist�ncia Facultativa  */  
    /*                                            */  
    /**********************************************/  
  
    DECLARE @ramo_id INT,  
            @subramo_id NUMERIC(5),  
            @dt_inicio_vigencia_sbr SMALLDATETIME,  
            @produto_id INT,  
            @tp_assistencia_id TINYINT,  
            @dt_inicio_vigencia_ass SMALLDATETIME,  
            @dt_ultimo_processamento SMALLDATETIME,  
            @lock_ultimo_processamento SMALLDATETIME,  
            @qtd_registros INT,  
            @dt_limite SMALLDATETIME,  
            @lock_limite SMALLDATETIME  
  
    /* Define a data de limite */  
  
    SET @dt_limite = DATEADD(DAY, 1, @dt_sistema)  
  
    /* Seleciona os dados do subramo */  
  
    SELECT @ramo_id = ramo_id,  
           @subramo_id = subramo_id,  
           @dt_inicio_vigencia_sbr = dt_inicio_vigencia_sbr,  
           @produto_id = produto_id,  
           @tp_assistencia_id = tp_assistencia_id,  
           @dt_inicio_vigencia_ass = dt_inicio_vigencia_ass,  
           @dt_ultimo_processamento = DATEADD(DAY, 1, dt_ult_processamento)  
      FROM subramo_tp_assistencia_tb  
     WHERE ramo_id = 93  
       AND subramo_id = 9320  
       AND tp_assistencia_id = 2  
  
     /* Define os timestamps correspondentes �s datas de �ltimo processamento e limite */  
  
     SELECT @lock_ultimo_processamento = MIN(dbts)   
     FROM util_db..timestamp_seguros_db_tb a  
     WHERE data >= @dt_ultimo_processamento  
  
     SELECT @lock_limite = MAX(dbts)   
     FROM util_db..timestamp_seguros_db_tb a  
     WHERE data < @dt_limite  
  
    /* Cria tabela tempor�ria de assist�ncia */  
  
    CREATE TABLE #movimento_assistencia  
                (movimento_id INT IDENTITY(1,1),  
                 dt_registro_segbr SMALLDATETIME,  
                 proposta_id NUMERIC(9),  
                 endosso_id INT,  
                 sinistro_id NUMERIC(11),  
                 nome VARCHAR(60),  
                 cpf_cnpj NUMERIC(14),  
                 dt_nascimento DATETIME,  
                 sexo CHAR(1),  
                 endereco VARCHAR(100),  
                 cidade VARCHAR(60),  
                 bairro VARCHAR(60),  
                 uf CHAR(2),  
                 cep NUMERIC(8),  
                 cliente_id INT,  
                 tp_movimento_id INT,  
                 tp_movimento_saida_id INT,  
                 dt_vigencia_assistencia SMALLDATETIME,  
                 proposta_bb NUMERIC(9),  
                 apolice_id NUMERIC(9),  
                 certificado_id NUMERIC(9),  
                 produto_id INT,  
                 ddd VARCHAR(4),  
  
-- Autor: anderson.ribeiro (Nova Consultoria) - Data da Altera��o: 11/07/2012  
-- Demanda: 15179437 - Item: Altera��o do campo telefone.  
                 telefone VARCHAR(9),  
-- Autor: anderson.ribeiro (Nova Consultoria) - Data da Altera��o: 11/07/2012       
       
                 endereco_risco VARCHAR(100),  
                 bairro_risco VARCHAR(60),  
                 cidade_risco VARCHAR(60),  
                 uf_risco CHAR(2),  
                 cep_risco NUMERIC(8))  
  
    /* Seleciona as contrata��es de proposta */  
  
    SELECT proposta_adesao_tb.proposta_id,  
           proposta_adesao_tb.dt_inicio_vigencia,  
           avaliacao_proposta_tb.dt_inclusao  
      INTO #contratacao_proposta  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
     INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     INNER JOIN seguros_db..avaliacao_proposta_tb avaliacao_proposta_tb (NOLOCK)  
        ON avaliacao_proposta_tb.proposta_id = proposta_tb.proposta_id  
     WHERE proposta_tb.produto_id = @produto_id  
       AND proposta_tb.situacao IN ('a', 'i', 'c')  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND avaliacao_proposta_tb.tp_avaliacao_id IN (0, 6, 8, 11, 1392, 1451, 1453,1383)  
       AND avaliacao_proposta_tb.lock >= @lock_ultimo_processamento  
       AND avaliacao_proposta_tb.lock <= @lock_limite  
--        AND avaliacao_proposta_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND avaliacao_proposta_tb.dt_inclusao < @dt_limite  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #contratacao_proposta.dt_inclusao,  
                #contratacao_proposta.proposta_id,  
                NULL,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                1,  
                1,  
                #contratacao_proposta.dt_inicio_vigencia,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #contratacao_proposta  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #contratacao_proposta.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona as inclus�es de endosso de renova��o */  
  
    SELECT endosso_tb.proposta_id,  
           endosso_tb.endosso_id,  
           endosso_tb.dt_inicio_vigencia_end as dt_pedido_endosso,  
           endosso_tb.dt_inclusao  
      INTO #renovacao_proposta  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     INNER JOIN seguros_db..endosso_tb endosso_tb (NOLOCK)  
        ON endosso_tb.proposta_id = proposta_tb.proposta_id  
     WHERE proposta_tb.produto_id = @produto_id  
       AND proposta_tb.situacao IN ('a', 'i', 'c')  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND endosso_tb.tp_endosso_id = 250  
       AND endosso_tb.lock >= @lock_ultimo_processamento    
       AND endosso_tb.lock <= @lock_limite  
--        AND endosso_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND endosso_tb.dt_inclusao < @dt_limite  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #renovacao_proposta.dt_inclusao,  
                #renovacao_proposta.proposta_id,  
                #renovacao_proposta.endosso_id,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                3,  
                6,  
                #renovacao_proposta.dt_pedido_endosso,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #renovacao_proposta  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #renovacao_proposta.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
            and isnumeric(pessoa_fisica_tb.cpf) > 0  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona as substitui��es de cobertura de aviso de sinistro para outra cobertura que n�o seja a b�sica */  
  
    SELECT sinistro_tb.proposta_id,             sinistro_cobertura_tb.sinistro_id,  
           sinistro_cobertura_tb.tp_cobertura_id,  
           sinistro_cobertura_tb.dt_inicio_vigencia,  
           sinistro_cobertura_tb.dt_inclusao  
      INTO #substitui_sinistro_aviso_basica  
      FROM seguros_db..sinistro_cobertura_tb sinistro_cobertura_tb (NOLOCK)  
     INNER JOIN seguros_db..sinistro_tb sinistro_tb (NOLOCK)  
        ON sinistro_tb.sinistro_id = sinistro_cobertura_tb.sinistro_id  
     INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
        ON proposta_tb.proposta_id = sinistro_tb.proposta_id  
     INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
        ON pessoa_fisica_tb.pf_cliente_id = proposta_tb.prop_cliente_id  
     INNER JOIN seguros_db..sinistro_vida_tb sinistro_vida_tb (NOLOCK)  
        ON sinistro_vida_tb.sinistro_id = sinistro_tb.sinistro_id  
       AND sinistro_vida_tb.cpf = pessoa_fisica_tb.cpf  
     INNER JOIN seguros_db..tp_cob_item_prod_tb tp_cob_item_prod_tb (NOLOCK)  
        ON tp_cob_item_prod_tb.produto_id = proposta_tb.produto_id  
       AND tp_cob_item_prod_tb.ramo_id = proposta_tb.ramo_id  
       AND tp_cob_item_prod_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     WHERE proposta_tb.produto_id = @produto_id  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND sinistro_cobertura_tb.val_estimativa <> 0  
       AND sinistro_cobertura_tb.dt_fim_vigencia IS NULL  
       AND tp_cob_item_prod_tb.class_tp_cobertura <> 'b'  
       AND sinistro_cobertura_tb.lock >= @lock_ultimo_processamento  
       AND sinistro_cobertura_tb.lock <= @lock_limite  
--        AND sinistro_cobertura_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND sinistro_cobertura_tb.dt_inclusao < @dt_limite  
  
    DELETE  
      FROM #substitui_sinistro_aviso_basica  
     WHERE NOT EXISTS (SELECT 1  
                         FROM seguros_db..sinistro_cobertura_tb sinistro_cobertura_tb (NOLOCK)  
                        INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
                           ON proposta_tb.proposta_id = #substitui_sinistro_aviso_basica.proposta_id  
                        INNER JOIN seguros_db..tp_cob_item_prod_tb tp_cob_item_prod_tb (NOLOCK)  
                           ON tp_cob_item_prod_tb.produto_id = proposta_tb.produto_id  
                          AND tp_cob_item_prod_tb.ramo_id = proposta_tb.ramo_id  
                          AND tp_cob_item_prod_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id  
                        WHERE sinistro_cobertura_tb.sinistro_id = #substitui_sinistro_aviso_basica.sinistro_id  
                          AND tp_cob_item_prod_tb.class_tp_cobertura = 'b'  
                          AND (sinistro_cobertura_tb.dt_fim_vigencia = #substitui_sinistro_aviso_basica.dt_inicio_vigencia  
                               OR sinistro_cobertura_tb.dt_fim_vigencia = DATEADD(DAY, -1, #substitui_sinistro_aviso_basica.dt_inicio_vigencia)))  
  
    DELETE  
      FROM #substitui_sinistro_aviso_basica  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..sinistro_cobertura_tb sinistro_cobertura_tb (NOLOCK)  
                    INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
                       ON proposta_tb.proposta_id = #substitui_sinistro_aviso_basica.proposta_id  
                    INNER JOIN seguros_db..tp_cob_item_prod_tb tp_cob_item_prod_tb (NOLOCK)  
                       ON tp_cob_item_prod_tb.produto_id = proposta_tb.produto_id  
                      AND tp_cob_item_prod_tb.ramo_id = proposta_tb.ramo_id  
             AND tp_cob_item_prod_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id  
                    WHERE sinistro_cobertura_tb.sinistro_id = #substitui_sinistro_aviso_basica.sinistro_id  
                      AND tp_cob_item_prod_tb.class_tp_cobertura = 'b'  
                      AND sinistro_cobertura_tb.dt_fim_vigencia IS NULL)  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #substitui_sinistro_aviso_basica.dt_inclusao,  
                proposta_tb.proposta_id,  
                NULL,  
                #substitui_sinistro_aviso_basica.sinistro_id,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                6,  
                6,  
                #substitui_sinistro_aviso_basica.dt_inicio_vigencia,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #substitui_sinistro_aviso_basica  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #substitui_sinistro_aviso_basica.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona os cancelamentos de proposta */  
  
    SELECT cancelamento_proposta_tb.proposta_id,  
           ISNULL(cancelamento_proposta_tb.endosso_id, 0) AS endosso_id,  
           CONVERT(CHAR(8), cancelamento_proposta_tb.dt_inclusao, 112) AS dt_pedido_endosso,  
           cancelamento_proposta_tb.dt_inclusao  
      INTO #cancelamento_proposta  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     INNER JOIN seguros_db..cancelamento_proposta_tb cancelamento_proposta_tb (NOLOCK)  
        ON cancelamento_proposta_tb.proposta_id = proposta_tb.proposta_id  
     WHERE proposta_tb.produto_id = @produto_id  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND cancelamento_proposta_tb.lock >= @lock_ultimo_processamento  
       AND cancelamento_proposta_tb.lock <= @lock_limite  
--        AND cancelamento_proposta_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND cancelamento_proposta_tb.dt_inclusao < @dt_limite  
  
    DELETE  
      FROM #cancelamento_proposta  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
                    WHERE proposta_adesao_tb.proposta_id = #cancelamento_proposta.proposta_id  
                      AND proposta_adesao_tb.dt_fim_vigencia <= #cancelamento_proposta.dt_pedido_endosso  
                      AND NOT EXISTS (SELECT 1  
                                        FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                                       WHERE endosso_tb.proposta_id = proposta_adesao_tb.proposta_id  
            AND endosso_tb.tp_endosso_id = 250))  
  
    DELETE  
      FROM #cancelamento_proposta  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                    WHERE endosso_tb.proposta_id = #cancelamento_proposta.proposta_id  
                      AND endosso_tb.dt_fim_vigencia_end <= #cancelamento_proposta.dt_pedido_endosso  
                      AND endosso_tb.endosso_id = (SELECT MAX(e2.endosso_id)  
                                                     FROM seguros_db..endosso_tb e2 (NOLOCK)  
                                                    WHERE e2.proposta_id = endosso_tb.proposta_id  
                                                      AND e2.tp_endosso_id = 250))  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #cancelamento_proposta.dt_inclusao,  
                proposta_tb.proposta_id,  
                #cancelamento_proposta.endosso_id,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                2,  
                3,  
                #cancelamento_proposta.dt_pedido_endosso,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #cancelamento_proposta  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #cancelamento_proposta.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona os avisos de sinistro na cobertura b�sica, as substitui��es de cobertura de aviso de sinistro  
       para a cobertura b�sica e as reaberturas de aviso de sinistro com cobertura de morte */  
  
    SELECT sinistro_tb.proposta_id,  
           sinistro_cobertura_tb.sinistro_id,  
           sinistro_cobertura_tb.tp_cobertura_id,  
           sinistro_cobertura_tb.dt_inicio_vigencia,  
           sinistro_cobertura_tb.dt_inclusao,  
           sinistro_cobertura_tb.lock  
      INTO #sinistro_aviso_basica  
      FROM seguros_db..sinistro_cobertura_tb sinistro_cobertura_tb (NOLOCK)  
     INNER JOIN seguros_db..sinistro_tb sinistro_tb (NOLOCK)  
        ON sinistro_tb.sinistro_id = sinistro_cobertura_tb.sinistro_id  
     INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
        ON proposta_tb.proposta_id = sinistro_tb.proposta_id  
     INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
        ON pessoa_fisica_tb.pf_cliente_id = proposta_tb.prop_cliente_id  
     INNER JOIN seguros_db..sinistro_vida_tb sinistro_vida_tb (NOLOCK)  
        ON sinistro_vida_tb.sinistro_id = sinistro_tb.sinistro_id  
       AND sinistro_vida_tb.cpf = pessoa_fisica_tb.cpf  
     INNER JOIN seguros_db..tp_cob_item_prod_tb tp_cob_item_prod_tb (NOLOCK)  
        ON tp_cob_item_prod_tb.produto_id = proposta_tb.produto_id  
       AND tp_cob_item_prod_tb.ramo_id = proposta_tb.ramo_id  
       AND tp_cob_item_prod_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     WHERE proposta_tb.produto_id = @produto_id  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND sinistro_cobertura_tb.val_estimativa <> 0  
       AND sinistro_cobertura_tb.dt_fim_vigencia IS NULL  
       AND tp_cob_item_prod_tb.class_tp_cobertura = 'b'  
       AND sinistro_cobertura_tb.lock >= @lock_ultimo_processamento  
       AND sinistro_cobertura_tb.lock <= @lock_limite  
--        AND sinistro_cobertura_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND sinistro_cobertura_tb.dt_inclusao < @dt_limite  
  
    DELETE  
      FROM #sinistro_aviso_basica  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..sinistro_cobertura_tb sinistro_cobertura_tb (NOLOCK)  
                    WHERE sinistro_cobertura_tb.sinistro_id = #sinistro_aviso_basica.sinistro_id  
                      AND sinistro_cobertura_tb.tp_cobertura_id = #sinistro_aviso_basica.tp_cobertura_id  
                      AND sinistro_cobertura_tb.dt_fim_vigencia = #sinistro_aviso_basica.dt_inicio_vigencia  
                      AND sinistro_cobertura_tb.lock <> #sinistro_aviso_basica.lock)  
      
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #sinistro_aviso_basica.dt_inclusao,  
                proposta_tb.proposta_id,  
                NULL,  
                #sinistro_aviso_basica.sinistro_id,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                4,  
                3,  
                #sinistro_aviso_basica.dt_inicio_vigencia,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #sinistro_aviso_basica  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #sinistro_aviso_basica.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona as altera��es de endere�o de proposta */  
  
    SELECT endosso_tb.proposta_id,  
           endosso_tb.endosso_id,  
           CONVERT(CHAR(8), endosso_tb.dt_inclusao, 112) AS dt_pedido_endosso,  
           endosso_tb.dt_inclusao  
      INTO #alteracao_endereco_proposta  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
     INNER JOIN seguros_db..endosso_proposta_tb endosso_proposta_tb (NOLOCK)  
        ON endosso_proposta_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..endosso_tb endosso_tb (NOLOCK)  
        ON endosso_tb.proposta_id = proposta_tb.proposta_id  
       AND endosso_tb.endosso_id = endosso_proposta_tb.endosso_id  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     WHERE proposta_tb.produto_id = @produto_id  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND (endosso_proposta_tb.ult_nome IS NOT NULL  
       OR endosso_proposta_tb.ult_endereco IS NOT NULL  
            OR endosso_proposta_tb.ult_bairro IS NOT NULL  
            OR endosso_proposta_tb.ult_municipio IS NOT NULL  
            OR endosso_proposta_tb.ult_estado IS NOT NULL  
            OR endosso_proposta_tb.ult_cep IS NOT NULL  
            OR endosso_proposta_tb.ult_telefone IS NOT NULL  
            OR endosso_proposta_tb.ult_ddd IS NOT NULL  
            OR endosso_proposta_tb.ult_sexo IS NOT NULL  
            OR endosso_proposta_tb.ult_dt_nascimento IS NOT NULL  
            OR endosso_proposta_tb.ult_tp_pessoa IS NOT NULL  
            OR endosso_proposta_tb.ult_cpf IS NOT NULL  
            OR endosso_proposta_tb.ult_cgc IS NOT NULL)  
       AND endosso_tb.lock >= @lock_ultimo_processamento    
       AND endosso_tb.lock <= @lock_limite  
--        AND endosso_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND endosso_tb.dt_inclusao < @dt_limite  
  
    DELETE  
      FROM #alteracao_endereco_proposta  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
                    WHERE proposta_adesao_tb.proposta_id = #alteracao_endereco_proposta.proposta_id  
                      AND proposta_adesao_tb.dt_fim_vigencia <= #alteracao_endereco_proposta.dt_pedido_endosso  
                      AND NOT EXISTS (SELECT 1  
                                        FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
      WHERE endosso_tb.proposta_id = proposta_adesao_tb.proposta_id  
                                         AND endosso_tb.tp_endosso_id = 250))  
  
    DELETE  
      FROM #alteracao_endereco_proposta  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                    WHERE endosso_tb.proposta_id = #alteracao_endereco_proposta.proposta_id  
                      AND endosso_tb.dt_fim_vigencia_end <= #alteracao_endereco_proposta.dt_pedido_endosso  
                      AND endosso_tb.endosso_id = (SELECT MAX(e2.endosso_id)  
                                                     FROM seguros_db..endosso_tb e2 (NOLOCK)  
                                                    WHERE e2.proposta_id = endosso_tb.proposta_id  
                                                      AND e2.tp_endosso_id = 250))  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #alteracao_endereco_proposta.dt_inclusao,  
                proposta_tb.proposta_id,  
                #alteracao_endereco_proposta.endosso_id,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                7,  
                2,  
                #alteracao_endereco_proposta.dt_pedido_endosso,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #alteracao_endereco_proposta  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #alteracao_endereco_proposta.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona os cancelamentos e reativa��es de proposta */  
  
    SELECT cancelamento_endosso_tb.proposta_id,  
           cancelamento_endosso_tb.endosso_id,  
           cancelamento_endosso_tb.canc_endosso_id,  
           CONVERT(CHAR(8), cancelamento_endosso_tb.dt_inclusao, 112) AS dt_pedido_endosso,  
           cancelamento_endosso_tb.dt_inclusao,  
           1 AS qtd  
      INTO #canceladas  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     INNER JOIN seguros_db..cancelamento_endosso_tb cancelamento_endosso_tb (NOLOCK)  
        ON cancelamento_endosso_tb.proposta_id = proposta_tb.proposta_id  
     WHERE proposta_tb.produto_id = @produto_id  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND cancelamento_endosso_tb.lock >= @lock_ultimo_processamento  
       AND cancelamento_endosso_tb.lock <= @lock_limite  
--        AND cancelamento_endosso_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND cancelamento_endosso_tb.dt_inclusao < @dt_limite  
  
    DECLARE @tem_endosso TINYINT  
    SET @tem_endosso = 1  
  
    WHILE @tem_endosso = 1  
    BEGIN  
  
        UPDATE #canceladas  
           SET canc_endosso_id = cancelamento_endosso_tb.canc_endosso_id,  
               qtd = #canceladas.qtd + 1  
          FROM seguros_db..cancelamento_endosso_tb cancelamento_endosso_tb (NOLOCK)  
         INNER JOIN #canceladas  
            ON #canceladas.proposta_id = cancelamento_endosso_tb.proposta_id  
           AND #canceladas.canc_endosso_id = cancelamento_endosso_tb.endosso_id  
  
        IF @@ROWCOUNT = 0  
            SET @tem_endosso = 0  
  
    END  
  
    DELETE  
      FROM #canceladas  
     WHERE NOT EXISTS (SELECT 1  
                         FROM seguros_db..cancelamento_proposta_tb cancelamento_proposta_tb (NOLOCK)  
                        WHERE cancelamento_proposta_tb.proposta_id = #canceladas.proposta_id  
                          AND cancelamento_proposta_tb.endosso_id = #canceladas.canc_endosso_id)  
  
    SELECT DISTINCT  
           #canceladas.proposta_id,  
           #canceladas.endosso_id,  
           #canceladas.dt_pedido_endosso,  
           #canceladas.dt_inclusao,  
           CASE  
               WHEN #canceladas.qtd % 2 <> 0  
               THEN 3  
               ELSE 2  
           END AS tp_movimento_id,  
           CASE  
               WHEN #canceladas.qtd % 2 <> 0  
               THEN 6  
    ELSE 3  
           END AS tp_movimento_saida_id  
      INTO #cancelamento_reativacao_proposta  
      FROM #canceladas  
  
    DELETE  
      FROM #cancelamento_reativacao_proposta  
     WHERE EXISTS (SELECT 1  
                     FROM #movimento_assistencia  
                    WHERE #movimento_assistencia.proposta_id = #cancelamento_reativacao_proposta.proposta_id  
                      AND #movimento_assistencia.endosso_id = #cancelamento_reativacao_proposta.endosso_id  
                      AND #movimento_assistencia.tp_movimento_id = #cancelamento_reativacao_proposta.tp_movimento_id)  
  
    DELETE  
      FROM #cancelamento_reativacao_proposta  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
                    WHERE proposta_adesao_tb.proposta_id = #cancelamento_reativacao_proposta.proposta_id  
                      AND proposta_adesao_tb.dt_fim_vigencia <= #cancelamento_reativacao_proposta.dt_pedido_endosso  
                      AND NOT EXISTS (SELECT 1  
                                        FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                                       WHERE endosso_tb.proposta_id = proposta_adesao_tb.proposta_id  
                                         AND endosso_tb.tp_endosso_id = 250))  
  
    DELETE  
      FROM #cancelamento_reativacao_proposta  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                    WHERE endosso_tb.proposta_id = #cancelamento_reativacao_proposta.proposta_id  
            AND endosso_tb.dt_fim_vigencia_end <= #cancelamento_reativacao_proposta.dt_pedido_endosso  
                      AND endosso_tb.endosso_id = (SELECT MAX(e2.endosso_id)  
                                                     FROM seguros_db..endosso_tb e2 (NOLOCK)  
                                                    WHERE e2.proposta_id = endosso_tb.proposta_id  
                                                      AND e2.tp_endosso_id = 250))  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #cancelamento_reativacao_proposta.dt_inclusao,  
                proposta_tb.proposta_id,  
                #cancelamento_reativacao_proposta.endosso_id,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                #cancelamento_reativacao_proposta.tp_movimento_id,  
                #cancelamento_reativacao_proposta.tp_movimento_saida_id,  
                #cancelamento_reativacao_proposta.dt_pedido_endosso,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #cancelamento_reativacao_proposta  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #cancelamento_reativacao_proposta.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona os cancelamentos e reativa��es de endosso de renova��o */  
  
    SELECT cancelamento_endosso_tb.proposta_id,  
           cancelamento_endosso_tb.endosso_id,  
           cancelamento_endosso_tb.canc_endosso_id,  
           CONVERT(CHAR(8), cancelamento_endosso_tb.dt_inclusao, 112) AS dt_pedido_endosso,  
           cancelamento_endosso_tb.dt_inclusao,  
           1 AS qtd  
      INTO #canceladas_endosso  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     INNER JOIN seguros_db..cancelamento_endosso_tb cancelamento_endosso_tb (NOLOCK)  
        ON cancelamento_endosso_tb.proposta_id = proposta_tb.proposta_id  
     WHERE proposta_tb.produto_id = @produto_id  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND cancelamento_endosso_tb.lock >= @lock_ultimo_processamento  
       AND cancelamento_endosso_tb.lock <= @lock_limite  
--        AND cancelamento_endosso_tb.dt_inclusao >= @dt_ultimo_processamento  
--        AND cancelamento_endosso_tb.dt_inclusao < @dt_limite  
  
    SET @tem_endosso = 1  
  
    WHILE @tem_endosso = 1  
    BEGIN  
  
        UPDATE #canceladas_endosso  
           SET canc_endosso_id = cancelamento_endosso_tb.canc_endosso_id,  
               qtd = #canceladas_endosso.qtd + 1  
          FROM seguros_db..cancelamento_endosso_tb cancelamento_endosso_tb (NOLOCK)  
         INNER JOIN #canceladas_endosso  
            ON #canceladas_endosso.proposta_id = cancelamento_endosso_tb.proposta_id  
           AND #canceladas_endosso.canc_endosso_id = cancelamento_endosso_tb.endosso_id  
  
        IF @@ROWCOUNT = 0  
            SET @tem_endosso = 0  
  
    END  
  
    DELETE  
      FROM #canceladas_endosso  
     WHERE NOT EXISTS (SELECT 1  
                         FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                        WHERE endosso_tb.proposta_id = #canceladas_endosso.proposta_id  
                          AND endosso_tb.endosso_id = #canceladas_endosso.canc_endosso_id  
                          AND endosso_tb.tp_endosso_id = 250)  
  
    SELECT DISTINCT  
           #canceladas_endosso.proposta_id,  
           #canceladas_endosso.endosso_id,  
           #canceladas_endosso.dt_pedido_endosso,  
           #canceladas_endosso.dt_inclusao,  
           CASE  
               WHEN #canceladas_endosso.qtd % 2 <> 0  
               THEN 2  
               ELSE 3  
           END AS tp_movimento_id,  
           CASE  
               WHEN #canceladas_endosso.qtd % 2 <> 0  
               THEN 3  
               ELSE 6  
           END AS tp_movimento_saida_id  
      INTO #cancelamento_reativacao_endosso  
      FROM #canceladas_endosso  
  
    DELETE  
      FROM #cancelamento_reativacao_endosso  
     WHERE EXISTS (SELECT 1  
                     FROM #movimento_assistencia  
                    WHERE #movimento_assistencia.proposta_id = #cancelamento_reativacao_endosso.proposta_id  
                      AND #movimento_assistencia.endosso_id = #cancelamento_reativacao_endosso.endosso_id  
                      AND #movimento_assistencia.tp_movimento_id = #cancelamento_reativacao_endosso.tp_movimento_id)  
  
    DELETE  
      FROM #cancelamento_reativacao_endosso  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
                    WHERE proposta_adesao_tb.proposta_id = #cancelamento_reativacao_endosso.proposta_id  
                      AND proposta_adesao_tb.dt_fim_vigencia <= #cancelamento_reativacao_endosso.dt_pedido_endosso  
                      AND NOT EXISTS (SELECT 1  
                                        FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                                       WHERE endosso_tb.proposta_id = proposta_adesao_tb.proposta_id  
                                         AND endosso_tb.tp_endosso_id = 250))  
  
    DELETE  
      FROM #cancelamento_reativacao_endosso  
     WHERE EXISTS (SELECT 1  
                     FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                    WHERE endosso_tb.proposta_id = #cancelamento_reativacao_endosso.proposta_id  
                      AND endosso_tb.dt_fim_vigencia_end <= #cancelamento_reativacao_endosso.dt_pedido_endosso  
                      AND endosso_tb.endosso_id = (SELECT MAX(e2.endosso_id)  
                                                     FROM seguros_db..endosso_tb e2 (NOLOCK)  
                                                    WHERE e2.proposta_id = endosso_tb.proposta_id  
                                                      AND e2.tp_endosso_id = 250))  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #cancelamento_reativacao_endosso.dt_inclusao,  
                proposta_tb.proposta_id,  
                #cancelamento_reativacao_endosso.endosso_id,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                #cancelamento_reativacao_endosso.tp_movimento_id,  
                #cancelamento_reativacao_endosso.tp_movimento_saida_id,  
                #cancelamento_reativacao_endosso.dt_pedido_endosso,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
NULL,  
                NULL,  
                NULL  
           FROM #cancelamento_reativacao_endosso  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #cancelamento_reativacao_endosso.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona as finaliza��es de vig�ncia de propostas */  
  
    SELECT proposta_adesao_tb.proposta_id,  
           proposta_adesao_tb.dt_fim_vigencia as dt_inicio_vigencia,  
           proposta_adesao_tb.dt_fim_vigencia as dt_inclusao  
      INTO #fim_vigencia_proposta  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
     INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     WHERE proposta_tb.produto_id = @produto_id  
       AND proposta_tb.situacao IN ('a', 'i', 'c')  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND proposta_adesao_tb.dt_fim_vigencia >= @dt_ultimo_processamento  
       AND proposta_adesao_tb.dt_fim_vigencia < @dt_limite --DATEADD(DAY, 1, @dt_sistema)  
       AND NOT EXISTS (SELECT 1  
                         FROM seguros_db..endosso_tb endosso_tb (NOLOCK)  
                        WHERE endosso_tb.proposta_id = proposta_tb.proposta_id  
                          AND endosso_tb.tp_endosso_id = 250)  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #fim_vigencia_proposta.dt_inclusao,  
                #fim_vigencia_proposta.proposta_id,  
                NULL,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                10,  
                7,  
                #fim_vigencia_proposta.dt_inicio_vigencia,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #fim_vigencia_proposta  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #fim_vigencia_proposta.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
            and isnumeric(pessoa_fisica_tb.cpf) > 0  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Seleciona as finaliza��es de vig�ncia de endosso de renova��o */  
  
    SELECT endosso_tb.proposta_id,  
           endosso_tb.endosso_id,  
           endosso_tb.dt_fim_vigencia_end as dt_pedido_endosso,  
           endosso_tb.dt_fim_vigencia_end as dt_inclusao  
      INTO #fim_vigencia_endosso  
      FROM seguros_db..proposta_tb proposta_tb (NOLOCK)  
INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..endosso_tb endosso_tb (NOLOCK)  
        ON endosso_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..escolha_plano_tb escolha_plano_tb (NOLOCK)  
        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
     INNER JOIN seguros_db..plano_tb plano_tb (NOLOCK)  
        ON plano_tb.produto_id = escolha_plano_tb.produto_id  
       AND plano_tb.plano_id = escolha_plano_tb.plano_id  
       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
     WHERE proposta_tb.produto_id = @produto_id  
       AND proposta_tb.situacao IN ('a', 'i', 'c')  
       AND escolha_plano_tb.dt_fim_vigencia IS NULL  
       AND plano_tb.tp_plano_id = 53  
       AND endosso_tb.dt_fim_vigencia_end >= @dt_ultimo_processamento  
       AND endosso_tb.dt_fim_vigencia_end < @dt_limite --DATEADD(DAY, 1, @dt_sistema)  
       AND endosso_tb.endosso_id = (SELECT MAX(e2.endosso_id)  
                                      FROM seguros_db..endosso_tb e2 (NOLOCK)  
                                     WHERE e2.proposta_id = proposta_tb.proposta_id  
                                       AND e2.tp_endosso_id = 250)  
  
    INSERT INTO #movimento_assistencia  
               (dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
            cpf_cnpj,  
                dt_nascimento,  
                sexo,  
     endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                dt_vigencia_assistencia,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco)  
         SELECT #fim_vigencia_endosso.dt_inclusao,  
                #fim_vigencia_endosso.proposta_id,  
                #fim_vigencia_endosso.endosso_id,  
                NULL,  
                cliente_tb.nome,  
                pessoa_fisica_tb.cpf,  
                pessoa_fisica_tb.dt_nascimento,  
                pessoa_fisica_tb.sexo,  
                endereco_corresp_tb.endereco,  
                endereco_corresp_tb.municipio,  
                endereco_corresp_tb.bairro,  
                endereco_corresp_tb.estado,  
                endereco_corresp_tb.cep,  
                cliente_tb.cliente_id,  
                10,  
                7,  
                #fim_vigencia_endosso.dt_pedido_endosso,  
                proposta_adesao_tb.proposta_bb,  
                proposta_adesao_tb.apolice_id,  
                certificado_tb.certificado_id,  
                proposta_tb.produto_id,  
                cliente_tb.ddd_1,  
                cliente_tb.telefone_1,  
                NULL,  
                NULL,  
                NULL,  
                NULL,  
                NULL  
           FROM #fim_vigencia_endosso  
          INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
             ON proposta_tb.proposta_id = #fim_vigencia_endosso.proposta_id  
          INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
             ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
          INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb (NOLOCK)  
             ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  
          INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)  
             ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
          INNER JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)  
             ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id  
           LEFT JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)  
             ON certificado_tb.proposta_id = proposta_tb.proposta_id  
  
    /* Atualiza as movimenta��es de reativa��o que j� existiam na base de assist�ncia */  
  
    UPDATE #movimento_assistencia  
       SET tp_movimento_saida_id = 4  
     WHERE tp_movimento_saida_id = 6  
       AND EXISTS (SELECT 1  
                     FROM movimento_assistencia_tb  
                    WHERE movimento_assistencia_tb.proposta_id = #movimento_assistencia.proposta_id  
                      AND movimento_assistencia_tb.cliente_id = #movimento_assistencia.cliente_id  
                      AND movimento_assistencia_tb.tp_assistencia_id = @tp_assistencia_id  
                      AND movimento_assistencia_tb.tp_movimento_id = 1)  
  
    UPDATE #movimento_assistencia  
       SET tp_movimento_saida_id = 4  
     WHERE tp_movimento_saida_id = 6  
       AND EXISTS (SELECT 1  
                     FROM #movimento_assistencia m2  
                    WHERE m2.proposta_id = #movimento_assistencia.proposta_id  
                      AND m2.cliente_id = #movimento_assistencia.cliente_id  
                      AND m2.movimento_id <> #movimento_assistencia.movimento_id  
                      AND m2.tp_movimento_id = 1)  
  
    /* Exclui todas as movimenta��es que j� tenham sido gravadas na base de assist�ncia */  
  
    DELETE  
      FROM #movimento_assistencia  
     WHERE EXISTS (SELECT 1  
                     FROM movimento_assistencia_tb  
                    WHERE movimento_assistencia_tb.proposta_id = #movimento_assistencia.proposta_id  
                      AND movimento_assistencia_tb.endosso_id IS NULL  
                      AND movimento_assistencia_tb.sinistro_id IS NULL  
                      AND movimento_assistencia_tb.tp_assistencia_id = @tp_assistencia_id  
                      AND movimento_assistencia_tb.tp_movimento_id = #movimento_assistencia.tp_movimento_id  
                      AND movimento_assistencia_tb.dt_registro_segbr = #movimento_assistencia.dt_registro_segbr)  
  
    DELETE  
      FROM #movimento_assistencia  
     WHERE EXISTS (SELECT 1  
                     FROM movimento_assistencia_tb  
                    WHERE movimento_assistencia_tb.proposta_id = #movimento_assistencia.proposta_id  
                      AND movimento_assistencia_tb.endosso_id = #movimento_assistencia.endosso_id  
                      AND movimento_assistencia_tb.tp_assistencia_id = @tp_assistencia_id  
                      AND movimento_assistencia_tb.tp_movimento_id = #movimento_assistencia.tp_movimento_id  
                      AND movimento_assistencia_tb.dt_registro_segbr = #movimento_assistencia.dt_registro_segbr)  
  
    DELETE  
      FROM #movimento_assistencia  
     WHERE EXISTS (SELECT 1  
                     FROM movimento_assistencia_tb  
                    WHERE movimento_assistencia_tb.proposta_id = #movimento_assistencia.proposta_id  
                      AND movimento_assistencia_tb.sinistro_id = #movimento_assistencia.sinistro_id  
                      AND movimento_assistencia_tb.tp_assistencia_id = @tp_assistencia_id  
                      AND movimento_assistencia_tb.tp_movimento_id = #movimento_assistencia.tp_movimento_id  
                      AND movimento_assistencia_tb.dt_registro_segbr = #movimento_assistencia.dt_registro_segbr)  
  
    /* Atualiza a data de vig�ncia das altera��es cadastrais */  
  
    UPDATE #movimento_assistencia  
       SET dt_vigencia_assistencia = movimento_assistencia_tb.dt_vigencia_assistencia  
      FROM #movimento_assistencia  
     INNER JOIN movimento_assistencia_tb  
        ON movimento_assistencia_tb.proposta_id = #movimento_assistencia.proposta_id  
       AND movimento_assistencia_tb.cliente_id = #movimento_assistencia.cliente_id  
       AND movimento_assistencia_tb.tp_assistencia_id = @tp_assistencia_id  
     WHERE #movimento_assistencia.tp_movimento_id = 7  
       AND movimento_assistencia_tb.tp_movimento_saida_id IN (1, 4, 6)  
       AND ISNULL(movimento_assistencia_tb.enviar, 'N') = 'S'  
       AND movimento_assistencia_tb.dt_registro_segbr = (SELECT MAX(dt_registro_segbr)  
                                                           FROM movimento_assistencia_tb m2  
                                                          WHERE m2.proposta_id = movimento_assistencia_tb.proposta_id  
                                                            AND m2.cliente_id = movimento_assistencia_tb.cliente_id  
                                                            AND m2.tp_assistencia_id = movimento_assistencia_tb.tp_assistencia_id  
                                                            AND m2.tp_movimento_saida_id = movimento_assistencia_tb.tp_movimento_saida_id  
                                                            AND m2.enviar = movimento_assistencia_tb.enviar)  
  
    /* Insere as movimenta��es de assist�ncia */  
  
    INSERT INTO movimento_assistencia_tb  
               (tp_assistencia_id,  
                dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
          ddd,  
                telefone,  
                endereco_risco,  
              bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                ramo_id,  
                subramo_id,  
                dt_inicio_vigencia_sbr,  
                dt_inicio_vigencia_ass,  
                dt_vigencia_assistencia,  
                dt_inclusao,  
                usuario)  
         SELECT @tp_assistencia_id,  
                dt_registro_segbr,  
                proposta_id,  
                endosso_id,  
                sinistro_id,  
                nome,  
                cpf_cnpj,  
                dt_nascimento,  
                sexo,  
                endereco,  
                cidade,  
                bairro,  
                uf,  
                cep,  
                cliente_id,  
                proposta_bb,  
                apolice_id,  
                certificado_id,  
                produto_id,  
                ddd,  
                telefone,  
                endereco_risco,  
                bairro_risco,  
                cidade_risco,  
                uf_risco,  
                cep_risco,  
                tp_movimento_id,  
                tp_movimento_saida_id,  
                @ramo_id,  
                @subramo_id,  
                @dt_inicio_vigencia_sbr,  
                @dt_inicio_vigencia_ass,  
                dt_vigencia_assistencia,  
                GETDATE(),  
                @usuario  
           FROM #movimento_assistencia  
  
    SET @qtd_registros = (SELECT COUNT(1)  
                            FROM #movimento_assistencia)  
  
    /* Insere as movimenta��es atuais de assist�ncia */  
  
    DELETE  
      FROM #movimento_assistencia  
     WHERE EXISTS (SELECT 1  
                     FROM movimento_assistencia_atual_tb  
                    WHERE movimento_assistencia_atual_tb.proposta_id = #movimento_assistencia.proposta_id  
                      AND movimento_assistencia_atual_tb.cliente_id = #movimento_assistencia.cliente_id  
                      AND movimento_assistencia_atual_tb.tp_assistencia_id = @tp_assistencia_id)  
  
    INSERT INTO movimento_assistencia_atual_tb  
               (proposta_id,  
                cliente_id,  
                tp_assistencia_id,  
                produto_id,  
                dt_inclusao,  
                usuario)  
         SELECT proposta_id,  
                cliente_id,  
                @tp_assistencia_id,  
                produto_id,  
                GETDATE(),  
                @usuario  
           FROM #movimento_assistencia  
          GROUP BY proposta_id,  
                   cliente_id,  
                   produto_id  
  
    /* Atualiza a �ltima data de processamento do tipo de assist�ncia */  
  
    UPDATE subramo_tp_assistencia_tb  
       SET dt_ult_processamento = @dt_sistema,  
           dt_alteracao = GETDATE(),  
           usuario = @usuario  
     WHERE tp_assistencia_id = @tp_assistencia_id  
       AND ramo_id = @ramo_id  
       AND subramo_id = @subramo_id  
       AND dt_inicio_vigencia_sbr = @dt_inicio_vigencia_sbr  
       AND dt_inicio_vigencia_ass = @dt_inicio_vigencia_ass  
  
    /* Atualiza pontua��o e �ltimo movimento gerado */  
  
    EXEC SEGS4775_SPU @produto_id,  
                      @tp_assistencia_id  
  
    /* Retorna a quantidade de movimentos extra�dos */  
  
    SELECT @qtd_registros  
  
RETURN  
  
  