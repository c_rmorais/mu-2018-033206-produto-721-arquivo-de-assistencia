
IF OBJECT_ID('TEMPDB..#PropostasEmitidas') IS NOT NULL DROP TABLE #PropostasEmitidas
	CREATE TABLE #PropostasEmitidas  
                (proposta_id NUMERIC(9) NOT NULL,
				 --cliente_id INT NOT NULL, 
				 --dt_fim_vigencia SMALLDATETIME NULL,  
                 )
     
	 INSERT INTO #PropostasEmitidas (proposta_id)	
		  SELECT proposta_tb.proposta_id     
            FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)
		   WHERE proposta_tb.produto_id = 721 
		     AND proposta_tb.situacao = 'i'
			 
			  IF OBJECT_ID('TEMPDB..#PropostasEmitidasComRenovacaoEndossos') IS NOT NULL DROP TABLE #PropostasEmitidasComRenovacaoEndossos
	CREATE TABLE #PropostasEmitidasComRenovacaoEndossos  
                (proposta_id NUMERIC(9) NOT NULL,
				 endosso_id INT NOT NULL,
				 seq_endosso INT NOT NULL )
	 INSERT INTO #PropostasEmitidasComRenovacaoEndossos (proposta_id, endosso_id, seq_endosso)	
		  SELECT p.proposta_id,
		         endosso_tb.endosso_id,
				 row_number() over(partition by p.proposta_id order by endosso_tb.ENDOSSO_ID desc) as seq_endosso
		    FROM #PropostasEmitidas p  
      INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)
			  ON p.proposta_id = endosso_tb.proposta_id
		   WHERE endosso_tb.tp_endosso_id = 250


		 IF OBJECT_ID('TEMPDB..#PropostasEmitidasComRenovacao') IS NOT NULL DROP TABLE #PropostasEmitidasComRenovacao
	CREATE TABLE #PropostasEmitidasComRenovacao  
                (proposta_id NUMERIC(9) NOT NULL,
				 endosso_id INT NOT NULL)
	 INSERT INTO #PropostasEmitidasComRenovacao (proposta_id, endosso_id)
		   SELECT PROPOSTA_ID, 
		          ENDOSSO_ID
		    FROM  #PropostasEmitidasComRenovacaoEndossos
			WHERE SEQ_ENDOSSO = 1
		   

		   
IF OBJECT_ID('TEMPDB..#PropostasAtivas') IS NOT NULL DROP TABLE #PropostasAtivas
	CREATE TABLE #PropostasAtivas  
                (proposta_id NUMERIC(9) NOT NULL,
				 dt_fim_vigencia SMALLDATETIME NULL  
                 )
     --NAO RENOVADAS		
	 INSERT INTO #PropostasAtivas (proposta_id,dt_fim_vigencia)						
		  SELECT p.proposta_id,
				 proposta_adesao.dt_fim_vigencia    
            FROM #PropostasEmitidas p WITH(NOLOCK)
	  INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao WITH(NOLOCK)
	          ON p.proposta_id = proposta_adesao.proposta_id


          UPDATE PropostasAtivas
		     SET PropostasAtivas.dt_fim_vigencia = endosso_tb.dt_fim_vigencia_end   
            FROM #PropostasAtivas PropostasAtivas WITH(NOLOCK)
	  INNER JOIN #PropostasEmitidasComRenovacao PropostasEmitidasComRenovacao 
	          ON PropostasEmitidasComRenovacao.proposta_id = PropostasAtivas.proposta_id
	  INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)
	          ON PropostasEmitidasComRenovacao.proposta_id = endosso_tb.proposta_id
			 AND PropostasEmitidasComRenovacao.endosso_id = endosso_tb.endosso_id


		
		 
--INSERT INTO interface_dados_db.dbo.SEGA9196_processar_tb -- delete from desenv_db..sega9169_processar_tb
--		  (
--		   layout_id  ,            
--		   proposta_bb ,             
--		   proposta_id ,            
--		   prop_cliente_id ,             
--		   certificado_id ,             
--		   apolice_id ,            
--		   sub_grupo_id ,             
--		   produto_id ,             
--		   ramo_id ,            
--		   plano_assistencia_id ,             
--		   tp_movimento_id ,             
--		   usuario ,             
--		   dt_inclusao ,             
--		   dt_alteracao,
--		   num_solicitacao
--		   ) 
	SELECT 
		  -- @layout_id,
		   proposta_adesao_tb.proposta_bb,
		   proposta_tb.proposta_id,
		   proposta_tb.prop_cliente_id as cliente_id,
		   NULL as certificado_id,
		   apolice_tb.apolice_id,
		   0 as sub_grupo_id,
		   proposta_tb.produto_id,
		   apolice_tb.ramo_id,
		   CASE plano_tb.tp_plano_id 
		   WHEN 53 THEN 4074
		   WHEN 54 THEN 4073
		   ELSE NULL END AS plano_assistencia_id,
		   1 AS tp_movimento_id,
		   --@usuario,
		   getdate() as dt_inclusao,
		   null  as dt_alteracao,
		   0 as num_solicitacao
	      FROM #PropostasAtivas PropostasAtivas
	INNER JOIN seguros_db.dbo.proposta_tb proposta_tb  with (nolock)
	        ON PropostasAtivas.proposta_id = proposta_tb.proposta_id
	INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with (nolock)
			ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id	
	INNER JOIN seguros_db.dbo.apolice_tb apolice_tb WITH(NOLOCK)            
			ON apolice_tb.apolice_id             = proposta_adesao_tb.apolice_id            
		   AND apolice_tb.ramo_id                = proposta_adesao_tb.ramo_id            
		   AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id               
		   AND apolice_tb.seguradora_cod_susep   = proposta_adesao_tb.seguradora_cod_susep
	INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH(NOLOCK)    
			ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id    
	INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH(NOLOCK)    
			ON plano_tb.produto_id = escolha_plano_tb.produto_id    
		   AND plano_tb.plano_id = escolha_plano_tb.plano_id    
		   AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia   
		 WHERE plano_tb.tp_plano_id in (53,54)
		   AND escolha_plano_tb.dt_fim_vigencia IS NULL  
		   AND PropostasAtivas.dt_fim_vigencia >= '20181015'			  	     				  