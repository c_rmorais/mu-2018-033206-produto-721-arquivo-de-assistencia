use seguros_db
begin tran
--PROCESSAR
SELECT @@TRANCOUNT
exec seguros_db.dbo.SEGS11714_SPI 1690, NULL, 'C_RMORAIS', 'S'
select * from interface_dados_db.dbo.sega9169_processar_tb with (nolock) where produto_id = 721
select a.* from interface_dados_db.dbo.sega9169_10_processar_tb  a with (nolock) 
join interface_dados_db.dbo.sega9169_processar_tb b with (nolock) 
on a.sega9169_processar_id = b.sega9169_processar_id
 where proposta_id in 
(233754685, 228477390, 218646454, 33384217, 9250482, 233753833, 23381718, 15035174, 215438939, 9468618)



--GERAR ARQUIVO
--delete from  ##tmp_pre_sega9169_tb  
CREATE TABLE ##tmp_pre_sega9169_tb   (       
            tmp_pre_id             INT IDENTITY(1,1), 
            processar_id           INT,               
            registro_id            SMALLINT NULL,     
            registro_dependente_id SMALLINT NULL,     
            remessa                SMALLINT NULL,     
            ordenar_por            INT NULL,          
            arquivo                VARCHAR(8000) )    


SELECT @@TRANCOUNT
EXEC interface_db..exportar_arquivo_generico_spi 1690, 'svccontrolm', 'S' 
SELECT  arquivo FROM ##tmp_pre_sega9169_tb ORDER BY tmp_pre_id

--commit


--P�S PROCESSAMENTO
--atualizar remessar para n�o dar erro 0260 + 1

begin tran
SELECT @@TRANCOUNT
EXEC seguros_db.dbo.SEGS11717_SPI 1690, 'SEGA9169.0263', '2018-08-20', 263, 'S', 'C_RMORAIS'
select * from interface_dados_db.dbo.sega9169_processado_tb with (nolock) where produto_id = 721 and dt_inclusao > '20180101'
rollback
commit

select a.num_contrato,a.proposta_externa,a.tp_movimento,a.nome_usuario,a.cnpj,a.cpf,b.tp_movimento_id 
from interface_dados_db.dbo.sega9169_10_processar_tb  a with (nolock) 
join interface_dados_db.dbo.sega9169_processar_tb b with (nolock) 
on a.sega9169_processar_id = b.sega9169_processar_id
 where proposta_id = 218646454

 
select proposta_id, b.layout_id,b.nome, proposta_bb, produto_id, a.plano_assistencia_id,e.txt_tipo_assistencia,c.descricao 
from interface_dados_db.dbo.sega9169_processado_tb a with (nolock)
INNER JOIN interface_db.dbo.layout_tb b with(nolock)
ON a.layout_id = b.layout_id
INNER JOIN assistencia_uss_db.dbo.tp_movimento_tb c with(nolock)
ON a.tp_movimento_id = c.tp_movimento_id
INNER JOIN assistencia_db.dbo.plano_assistencia_tb d with(nolock)
on a.plano_assistencia_id = d.plano_assistencia_id
INNER JOIN assistencia_db.dbo.tipo_assistencia_tb e with(nolock)
on d.tipo_assistencia_id = e.tipo_assistencia_id
where proposta_id = 218646454