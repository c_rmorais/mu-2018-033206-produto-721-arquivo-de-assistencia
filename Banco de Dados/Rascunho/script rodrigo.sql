/* --s� precisa rodar, se for rodar a extra��o duas vezes seguidas sem rodar o SEGA
--marreta para n�o dar erro no 1206 (desenv legado)
--desenv_Db
alter procedure dbo.rdgsm
as 
UPDATE assistencia_db.dbo.movimento_assistencia_atual_tb
SET tp_movimento_id = 1
where produto_id = 1206 --and usuario = 'rdgs'
and tp_movimento_id <> 1
--and proposta_id in 
--(235369916
--,235383907
--,236161128
--,236183330
--,233739036
--,234868183
--,234074088)
*/


/*
---marreta para finalizar a vig�ncia para fins de teste
select @@TRANCOUNT
alter procedure dbo.rdgsm
as 
UPDATE SEGUROS_DB.DBO.APOLICE_TB
SET dt_fim_vigencia = '2017-04-10' 
WHERE proposta_id = 236418245
AND dt_fim_vigencia = '2021-10-21 00:00:00'


-->este exemplo n�o deve gerar nova movimenta��o, pois o �ltimo movimento foi de cancelamento
select @@TRANCOUNT
alter procedure dbo.rdgsm
as 
UPDATE SEGUROS_DB.DBO.APOLICE_TB --where proposta_id = 236028254
SET dt_fim_vigencia = '2017-04-10' 
WHERE proposta_id = 236028254
AND dt_fim_vigencia = '2021-08-26 00:00:00'
SELECT * FROM assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 236028254

-->este exemplo  deve gerar nova movimenta��o, pois o �ltimo movimento foi de cancelamento DE CONJUGE
select @@TRANCOUNT
alter procedure dbo.rdgsm
as 
UPDATE SEGUROS_DB.DBO.APOLICE_TB --where proposta_id = 236418250
SET dt_fim_vigencia = '2017-04-10' 
WHERE proposta_id = 236418250
AND dt_fim_vigencia = '2021-10-21 00:00:00'

*/

--SELECT * FROM INTERFACE_DB.DBO.layout_tb where nome = 'sega9169'
--SELECT * FROM INTERFACE_DB.DBO.layout_vigencia_tb where layout_id = 1690
--

/* Identificado poss�vel diverg�ncia entre a qtd di�ria e mensal, mas identificado que est� correto, pois o momento que se verifica os componentes � diferente. An�lise:

select COUNT(1) from interface_dados_db.dbo.sega9196_processar_tb where produto_id in (1235,1236,1237) --113594


select * from interface_dados_db.dbo.sega9169_processado_tb a
  left join interface_dados_db.dbo.sega9196_processar_tb b
    on a.proposta_id = b.proposta_id
    and a.prop_cliente_id = b.prop_cliente_id
where a.produto_id in (1235,1236,1237) 
 and b.proposta_id is null

select * from interface_dados_db.dbo.sega9196_processar_tb a
  left join  interface_dados_db.dbo.sega9169_processado_tb   b
    on a.proposta_id = b.proposta_id
    and a.prop_cliente_id = b.prop_cliente_id
where a.produto_id in (1235,1236,1237) 
 and b.proposta_id is null

 
select * from seguros_db.dbo.proposta_tb where proposta_id = 236418239
select * from seguros_db.dbo.apolice_tb where proposta_id = 236418239

select  from seguros_db.dbo.proposta_Tb


select * from interface_dados_db.dbo.sega9196_processar_tb where proposta_id = 236418239
select * from interface_dados_db.dbo.sega9169_processado_tb where proposta_id = 236418239

select * from seguros_db.dbo.proposta_complementar_tb WHERE proposta_id = 236418239
select * from seguros_db.dbo.PROPOSTA_TB WHERE proposta_id = 236418239

select * from seguros_db.dbo.proposta_complementar_tb WHERE proposta_id = 236418212
select * from seguros_db.dbo.PROPOSTA_TB WHERE proposta_id = 236418212

select * from seguros_db.dbo.ENDOSSO_tB WHERE proposta_id IN (236418239, 236418212)


select * from seguros_db.dbo.proposta_tb c 
        inner join seguros_db.dbo.proposta_complementar_tb b
          on c.proposta_id = b.proposta_id
          where c.produto_id in (1235,1236,1237)       
           and not (c.dt_contratacao BETWEEN b.dt_inicio_vigencia 
         AND ISNULL(b.dt_fim_vigencia, DATEADD(DAY, 1, GETDATE()))  ) 


select * from seguros_db.dbo.endosso_Tb where proposta_id in
( 236418240
,236418250
,236418229
,236418241
,236418254
,236418249
)

*/



--rollback
-->Rodar a extra��o
begin tran
select @@TRANCOUNT
exec seguros_Db.dbo.segs11740_spi 'rdgs'

--(solu��o data de corte)
--use desenv_db
--alter procedure dbo.rdgsm
--as
--update assistencia_db.dbo.movimento_Assistencia_atual_tb 
--set dt_envio = getdate()
--where produto_id in (1235,1236,1237) 



select * from seguros_Db.dbo.endosso_tb 
where proposta_id = 235787047

--(solu��o data de corte)
use desenv_db
select @@trancount
alter procedure dbo.rdgsm
as
update seguros_Db.dbo.endosso_tb
set dt_pedido_endosso = '2017-07-18'
where proposta_id = 235787047
and endosso_id = 1 
and dt_pedido_endosso = '2016-08-23 00:00:00'







--rollback

--verificar extra��o
SELECT count(1) FROM assistencia_db.dbo.movimento_assistencia_atual_tb where  produto_id  in (1235,1236,1237) 
--SELECT * FROM assistencia_db.dbo.movimento_assistencia_atual_tb where produto_id  in (1235,1236,1237) and endosso_id is not null and dt_inclusao
--SELECT * FROM assistencia_db.dbo.movimento_assistencia_atual_tb where produto_id  in (1235,1236,1237) and endosso_id is  null
SELECT  top 10 * FROM assistencia_db.dbo.movimento_assistencia_atual_tb where produto_id  in (1235,1236,1237) and dt_envio is null and usuario = 'rdgs'

select * from assistencia_db.dbo.movimento_assistencia_atual_tb where dt_inclusao > getdate()-2 and produto_id in (1235,1236,1237)
and endosso_id is not  null

----controlando contagem para verificar poss�vel loop infinito
--select COUNT(1) from interface_dados_db.dbo.sega9169_processado_tb where produto_id in (1235,1236,1237) --113593
----113366 -- 2164   --2164   --2164
----111019 -- 111019 --111019 --111019


--> Rodar SEGA
select @@TRANCOUNT
   
--delete from  ##tmp_pre_sega9169_tb  
CREATE TABLE ##tmp_pre_sega9169_tb   (       
            tmp_pre_id             INT IDENTITY(1,1), 
            processar_id           INT,               
            registro_id            SMALLINT NULL,     
            registro_dependente_id SMALLINT NULL,     
            remessa                SMALLINT NULL,     
            ordenar_por            INT NULL,          
            arquivo                VARCHAR(8000) )    

--PROCESSAR
SELECT @@TRANCOUNT
exec seguros_db.dbo.SEGS11714_SPI 1690, NULL, 'RDGSM', 'S'
select * from interface_dados_db.dbo.sega9169_processar_tb where produto_id in (1235,1236,1237) --113594



--DROP TABLE #p
select  top 10 a.* 
into #pp
from interface_dados_db.dbo.sega9169_processar_tb a
inner join interface_dados_db.dbo.sega9169_processar_tb  b
   on a.proposta_id = b.proposta_id
   and a.prop_cliente_id <> b.prop_cliente_id
--order by a.sega9196_processar_id  
 and a.produto_id in (1235,1236,1237)

select * from #pp

select   c.* from #pp a
inner join interface_dados_db.dbo.sega9169_10_processar_tb c
on a.sega9169_processar_id = c.sega9169_processar_id
order by a.sega9169_processar_id  
--95733009100000225535173
--116146009100026092417049

--GERAR ARQUIVO
SELECT @@TRANCOUNT
EXEC interface_db..exportar_arquivo_generico_spi 1690, 'svccontrolm', 'S' 

--SELECT  arquivo FROM ##tmp_pre_sega9169_tb ORDER BY tmp_pre_id

--P�S PROCESSAMENTO
--atualizar remessar para n�o dar erro 0260 + 1
SELECT @@TRANCOUNT
EXEC seguros_db.dbo.SEGS11717_SPI 1690, 'SEGA9169.0263', '2017-05-30', 263, 'S', 'RDGSM'
select * from interface_dados_db.dbo.sega9169_processado_tb where produto_id in (1235,1236,1237) and dt_inclusao > '2017-07-15' --113594 


-->compara��o de aquivos
--SELECT * FROM INTERFACE_DB.DBO.layout_campo_tb WHERE LAYOUT_ID = 1690
----40000043481                     00002649060093000000000000     32010790       IHUMANA PRESTADORA DE SERVICOS E COMERCIO LTDA ME                                20160921202109220455892600015800000000000Q SIA QUADRA 5C AREA ESPECIAL 33/34 PARTE C-SETOR DE INDUSTR                    DFBRASILIA                           ZONA INDUSTRIAL (GUARA)            71200055  00000000061033287678                                                                                                                                                                  00000000000000000000                                                                                                                                                             HUMANA PRESTADORA DE SERVICOS E COMERCIO LTDA ME                                                                                                                         0.00         0.00   0         0.0019000101
----40000052424                     10000501330091000000000000     31340503       IPATRICIA HERINGER                                                               201607292021072973214329049   00000000000R ENGENHEIRO TEIXEIRA SOARES-315 AP 201                                         RSPORTO ALEGRE                       BELA VISTA                         90440140  00000000051037116545                                                                                                                                                                  00000000000000000000                                                                                                                                                             PATRICIA HERINGER                                                                                                                                                        0.00         0.00   0         0.0019000101
----

-->SEGA MENSAL


--PROCESSAR
SELECT @@TRANCOUNT
exec seguros_db.dbo.SEGS11741_SPI 1692, NULL, 'RDGSM', 'S'
--select COUNT(1) from interface_dados_db.dbo.sega9196_processar_tb where produto_id in (1235,1236,1237) --113594

DROP TABLE #p
select  top 10 a.* 
into #p
from interface_dados_db.dbo.sega9196_processar_tb a
inner join interface_dados_db.dbo.sega9196_processar_tb  b
   on a.proposta_id = b.proposta_id
   and a.prop_cliente_id <> b.prop_cliente_id
--order by a.sega9196_processar_id  
 and a.produto_id in (1235,1236,1237)

select * from #p

select   c.* from #p a
inner join interface_dados_db.dbo.sega9196_10_processar_tb c
on a.sega9196_processar_id = c.sega9196_processar_id
order by a.sega9196_processar_id  
--95733009100000225535173
--116146009100026092417049


--GERAR ARQUIVO

--delete from  ##tmp_pre_sega9196_tb  
CREATE TABLE ##tmp_pre_sega9196_tb   (       
            tmp_pre_id             INT IDENTITY(1,1), 
            processar_id           INT,               
            registro_id            SMALLINT NULL,     
            registro_dependente_id SMALLINT NULL,     
            remessa                SMALLINT NULL,     
            ordenar_por            INT NULL,          
            arquivo                VARCHAR(8000) )    
            
SELECT @@TRANCOUNT
EXEC interface_db..exportar_arquivo_generico_spi 1692, 'svccontrolm', 'S' 
--select  top 1000 arquivo from ##tmp_pre_sega9196_tb order by tmp_pre_id asc

--P�S PROCESSAMENTO
--atualizar remessar para n�o dar erro 0260 + 1
SELECT @@TRANCOUNT
EXEC seguros_db..SEGS09333_SPI 1692, 'SEGA9196.0260', '2017-05-30', 260, 'S', 'RDGSM'
--select COUNT(1) from interface_dados_db.dbo.sega9196_processado_tb where produto_id in (1235,1236,1237) --113594

-->SEGA MENSAL<--


-->Pegar massa para verificar fluxo.
select a.* from seguros_db.dbo.endosso_tb a with (nolock)
inner join seguros_Db.dbo.proposta_tb b  with (nolock)
  on a.proposta_id = b.proposta_id
  and b.produto_id in (1235,1236,1237)
inner join seguros_Db.dbo.proposta_complementar_tb c with (nolock)
   on a.proposta_id = c.proposta_id
   where b.situacao = 'i' 
   and tp_endosso_id not in (99,100)


--resultado 1 --37 altera��o de plano
select * from seguros_db.dbo.endosso_Tb where proposta_id = 236418245
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 236418245
select * FROM interface_dados_db.dbo.sega9169_processaDO_tb where proposta_id =  236418245   order by sega9169_processar_id
select b.* FROM interface_dados_db.dbo.sega9169_processaDO_tb a
  inner join interface_dados_db.dbo.sega9169_10_processaDO_tb b
    on a.sega9169_processar_id = b. sega9169_processar_id
where proposta_id =  236418245   order by a.sega9169_processar_id

--resultado 2 --51 --altera��o apenas um componente
select * from seguros_db.dbo.endosso_Tb where proposta_id = 235528522
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 235528522
select * FROM interface_dados_db.dbo.sega9169_processaDO_tb where proposta_id =  235528522   order by sega9169_processar_id

--resultado 3 --51 - altera��o com dois componentes
select * from seguros_db.dbo.endosso_Tb where proposta_id = 235115435
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 235115435
select * FROM interface_dados_db.dbo.sega9169_processaDO_tb where proposta_id =  235115435   order by sega9169_processar_id
select b.* FROM interface_dados_db.dbo.sega9169_processaDO_tb a
  inner join interface_dados_db.dbo.sega9169_10_processaDO_tb b
    on a.sega9169_processar_id = b. sega9169_processar_id
where proposta_id =  235115435   order by a.sega9169_processar_id

  --mensal
select * from seguros_db.dbo.endosso_Tb where proposta_id = 235115435
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 235115435
select * FROM interface_dados_db.dbo.sega9196_processaDO_tb where proposta_id =  235115435   order by sega9196_processar_id
select b.* FROM interface_dados_db.dbo.sega9196_processaDO_tb a
  inner join interface_dados_db.dbo.sega9196_10_processaDO_tb b
    on a.sega9196_processar_id = b.sega9196_processar_id
where proposta_id =  235115435   order by a.sega9196_processar_id

 
 --resultado 4 -- sinistro de c�njuge - 31
select * from seguros_db.dbo.endosso_Tb where proposta_id = 236418250
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 236418250
select * FROM interface_dados_db.dbo.sega9169_processaDO_tb where proposta_id =  236418250   order by sega9169_processar_id
select b.* FROM interface_dados_db.dbo.sega9169_processaDO_tb a
  inner join interface_dados_db.dbo.sega9169_10_processaDO_tb b
    on a.sega9169_processar_id = b. sega9169_processar_id
where proposta_id =  236418250   order by a.sega9169_processar_id

  --mensal
select * from seguros_db.dbo.endosso_Tb where proposta_id = 236418250
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 236418250
select * FROM interface_dados_db.dbo.sega9196_processaDO_tb where proposta_id =  236418250   order by sega9196_processar_id
select b.* FROM interface_dados_db.dbo.sega9196_processaDO_tb a
  inner join interface_dados_db.dbo.sega9196_10_processaDO_tb b
    on a.sega9196_processar_id = b.sega9196_processar_id
where proposta_id =  236418250   order by a.sega9196_processar_id

 
  --resultado 5 -- 63 -- cancelamento com c�njuge
select * from seguros_db.dbo.endosso_Tb where proposta_id = 236028254
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 236028254
select * FROM interface_dados_db.dbo.sega9169_processaDO_tb where proposta_id =  236028254   order by sega9169_processar_id
select b.* FROM interface_dados_db.dbo.sega9169_processaDO_tb a
  inner join interface_dados_db.dbo.sega9169_10_processaDO_tb b
    on a.sega9169_processar_id = b. sega9169_processar_id
where proposta_id =  236028254   order by a.sega9169_processar_id

 


 --mensal
select * from seguros_db.dbo.endosso_Tb where proposta_id = 235115435
select * from assistencia_db.dbo.movimento_assistencia_atual_tb where proposta_id = 235115435
select * FROM interface_dados_db.dbo.sega9196_processaDO_tb where proposta_id =  235115435   order by sega9196_processar_id
select b.* FROM interface_dados_db.dbo.sega9196_processaDO_tb a
  inner join interface_dados_db.dbo.sega9196_10_processaDO_tb b
    on a.sega9196_processar_id = b.sega9196_processar_id
where proposta_id =  235115435   order by a.sega9196_processar_id

 
 --massa para testar data de corte
select top 1 a.* from assistencia_db.dbo.movimento_assistencia_atual_Tb a 
   inner join seguros_Db.dbo.endosso_Tb b
     on a.proposta_id = b.proposta_id
     and tp_endosso_id = 51
     and b.endosso_id = 1
     and produto_id = 1235
     and a.dt_inclusao > GETDATE()-2
