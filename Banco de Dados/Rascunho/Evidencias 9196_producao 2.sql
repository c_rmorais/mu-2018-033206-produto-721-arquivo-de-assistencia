
IF OBJECT_ID('TEMPDB..##PropostasEmitidas') IS NOT NULL DROP TABLE ##PropostasEmitidas
	CREATE TABLE ##PropostasEmitidas  
                (proposta_id NUMERIC(9) NOT NULL,
				 --cliente_id INT NOT NULL, 
				 --dt_fim_vigencia SMALLDATETIME NULL,  
                 )
     --inserindo propostas emitidas do produto 721
	 INSERT INTO ##PropostasEmitidas (proposta_id)	
		  SELECT proposta_tb.proposta_id     
            FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)
		   WHERE proposta_tb.produto_id = 721 
		     AND proposta_tb.situacao = 'i'
			 
	--verificando ultimo endosso 250 das propostas  - INICIO
	IF OBJECT_ID('TEMPDB..##PropostasEmitidasComRenovacaoEndossos') IS NOT NULL DROP TABLE ##PropostasEmitidasComRenovacaoEndossos
	CREATE TABLE ##PropostasEmitidasComRenovacaoEndossos  
                (proposta_id NUMERIC(9) NOT NULL,
				 endosso_id INT NOT NULL,
				 seq_endosso INT NOT NULL )
	 INSERT INTO ##PropostasEmitidasComRenovacaoEndossos (proposta_id, endosso_id, seq_endosso)	
		  SELECT p.proposta_id,
		         endosso_tb.endosso_id,
				 row_number() over(partition by p.proposta_id order by endosso_tb.ENDOSSO_ID desc) as seq_endosso
		    FROM ##PropostasEmitidas p  
      INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)
			  ON p.proposta_id = endosso_tb.proposta_id
		   WHERE endosso_tb.tp_endosso_id = 250


		 IF OBJECT_ID('TEMPDB..##PropostasEmitidasComRenovacao') IS NOT NULL DROP TABLE ##PropostasEmitidasComRenovacao
	CREATE TABLE ##PropostasEmitidasComRenovacao  
                (proposta_id NUMERIC(9) NOT NULL,
				 endosso_id INT NOT NULL)
	 INSERT INTO ##PropostasEmitidasComRenovacao (proposta_id, endosso_id)
		   SELECT PROPOSTA_ID, 
		          ENDOSSO_ID
		    FROM  ##PropostasEmitidasComRenovacaoEndossos
			WHERE SEQ_ENDOSSO = 1
	--verificando ultimo endosso 250 das propostas  - FIM	   

		   
IF OBJECT_ID('TEMPDB..##PropostasAtivas') IS NOT NULL DROP TABLE ##PropostasAtivas
	CREATE TABLE ##PropostasAtivas  
                (proposta_id NUMERIC(9) NOT NULL,
				 dt_fim_vigencia SMALLDATETIME NULL  
                 )
     --Pegando vigencia de todas propostas do produto 721 emitidas		
	 INSERT INTO ##PropostasAtivas (proposta_id,dt_fim_vigencia)						
		  SELECT p.proposta_id,
				 proposta_adesao.dt_fim_vigencia    
            FROM ##PropostasEmitidas p WITH(NOLOCK)
	  INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao WITH(NOLOCK)
	          ON p.proposta_id = proposta_adesao.proposta_id

     --Atualizando vig�ncia das propostas que sofreram endosso de renova��o com a data de fim de vigencia do ultimo endosso 250.
          UPDATE PropostasAtivas
		     SET PropostasAtivas.dt_fim_vigencia = endosso_tb.dt_fim_vigencia_end   
            FROM ##PropostasAtivas PropostasAtivas WITH(NOLOCK)
	  INNER JOIN ##PropostasEmitidasComRenovacao PropostasEmitidasComRenovacao 
	          ON PropostasEmitidasComRenovacao.proposta_id = PropostasAtivas.proposta_id
	  INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)
	          ON PropostasEmitidasComRenovacao.proposta_id = endosso_tb.proposta_id
			 AND PropostasEmitidasComRenovacao.endosso_id = endosso_tb.endosso_id

			
		
		 
		SELECT 
			   1692 as layout_id ,
			   proposta_adesao_tb.proposta_bb,
			   proposta_tb.proposta_id,
			   proposta_tb.prop_cliente_id as cliente_id,
			   NULL as certificado_id,
			   apolice_tb.apolice_id,
			   0 as sub_grupo_id,
			   proposta_tb.produto_id,
			   apolice_tb.ramo_id,
			   CASE plano_tb.tp_plano_id 
			   WHEN 53 THEN 4074
			   WHEN 54 THEN 4073
			   ELSE NULL END AS plano_assistencia_id,
			   1 AS tp_movimento_id,
			   'C_RMORAIS' as usuario,
			   getdate() as dt_inclusao,
			   null  as dt_alteracao,
			   0 as num_solicitacao
			INTO ##SEGA9196_processar_tb
	      FROM ##PropostasAtivas PropostasAtivas
	INNER JOIN seguros_db.dbo.proposta_tb proposta_tb  with (nolock)
	        ON PropostasAtivas.proposta_id = proposta_tb.proposta_id
	INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with (nolock)
			ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id	
	INNER JOIN seguros_db.dbo.apolice_tb apolice_tb WITH(NOLOCK)            
			ON apolice_tb.apolice_id             = proposta_adesao_tb.apolice_id            
		   AND apolice_tb.ramo_id                = proposta_adesao_tb.ramo_id            
		   AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id               
		   AND apolice_tb.seguradora_cod_susep   = proposta_adesao_tb.seguradora_cod_susep
	INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH(NOLOCK)    
			ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id    
	INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH(NOLOCK)    
			ON plano_tb.produto_id = escolha_plano_tb.produto_id    
		   AND plano_tb.plano_id = escolha_plano_tb.plano_id    
		   AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia   
		 WHERE plano_tb.tp_plano_id in (53,54)
		   AND escolha_plano_tb.dt_fim_vigencia IS NULL  
		   AND PropostasAtivas.dt_fim_vigencia >= '2018-10-25'	
		   
		  -- select count(*) from ##PropostasAtivas

delete pa from ##PropostasAtivas pa 
left join  ##SEGA9196_processar_tb sg
on pa.proposta_id = sg.proposta_id
where sg.produto_id is null


  IF OBJECT_ID('temp_db..##SEGA9196_10_PROD721') IS NOT NULL   DROP TABLE ##SEGA9196_10_PROD721  
           
       
            CREATE TABLE ##SEGA9196_10_PROD721 (
			proposta_id NUMERIC(9) NOT NULL,      
                  SEGA9196_processar_id INT NULL    
      , processar_id INT NULL    
                  , num_contrato VARCHAR(20) NULL    
      , versao_contrato VARCHAR(15) NULL    
      , apolice_id NUMERIC(9, 0) NULL    
      , proposta_externa VARCHAR(15) NULL    
      , tp_operacao CHAR(1) NULL    
      , nome_segurado VARCHAR(80) NULL    
      , dt_inicio_vigencia SMALLDATETIME NULL    
      , dt_fim_vigencia VARCHAR(10) NULL    
      , cpf_cnpj_estipulante VARCHAR(14) NULL    
      , cpf_segurado VARCHAR(11) NULL    
      , endereco VARCHAR(80) NULL    
      , estado CHAR(2) NULL    
                  , municipio VARCHAR(35) NULL    
                  , bairro VARCHAR(35) NULL    
                  , cep VARCHAR(10) NULL    
      , telefone NUMERIC(20, 0) NULL     
      , endereco_risco VARCHAR(80) NULL                   
                  , uf_risco CHAR(2) NULL    
                  , municipio_risco VARCHAR(35) NULL    
                  , bairro_risco VARCHAR(35) NULL    
                  , cep_risco VARCHAR(10) NULL    
                  , telefone_risco NUMERIC(20, 0) NULL    
                  , placa_veiculo VARCHAR(10) NULL    
                  , chassi_veiculo VARCHAR(20) NULL    
                  , cor_veiculo VARCHAR(10) NULL    
                  , ano_fab_veiculo CHAR(4) NULL    
                  , modelo_veiculo VARCHAR(40) NULL    
                  , marca_veiculo VARCHAR(40) NULL    
                  , dias_reserva_pparcial VARCHAR(11) NULL    
                  , dias_reserva_ptotal VARCHAR(11) NULL    
                  , dias_reserva_roubo VARCHAR(11) NULL    
                  , nome_estipulante VARCHAR(80) NULL    
                  , parentesco VARCHAR(80) NULL    
                  , val_lim_funeral NUMERIC(13, 2) NULL    
                  , val_lim_dmho NUMERIC(13, 2) NULL    
                  , qtd_cestas INT NULL    
                  , val_cestas NUMERIC(13, 2) NULL    
                  , dt_nasc_usuario SMALLDATETIME NULL    
                  , dt_inclusao DATETIME NULL    
                  , dt_alteracao DATETIME NULL    
                  , usuario VARCHAR(20) NULL    
                  , chave_principal  VARCHAR(30) NULL    
                  )    
    
    

      INSERT INTO ##SEGA9196_10_PROD721     
      ( proposta_id,
        SEGA9196_processar_id     
      , processar_id    
                  , num_contrato     
      , versao_contrato     
      , apolice_id     
      , proposta_externa     
      , tp_operacao     
      , nome_segurado    
      , dt_inicio_vigencia     
      , dt_fim_vigencia     
      , cpf_cnpj_estipulante     
      , cpf_segurado     
      , endereco     
      , estado    
                  , municipio     
  , bairro     
                  , cep     
      , telefone     
      , endereco_risco                    
                  , uf_risco     
                  , municipio_risco     
                  , bairro_risco     
                  , cep_risco     
                  , telefone_risco     
                  , placa_veiculo     
                  , chassi_veiculo     
                  , cor_veiculo     
                  , ano_fab_veiculo     
                  , modelo_veiculo     
                  , marca_veiculo     
                  , dias_reserva_pparcial     
                  , dias_reserva_ptotal     
                  , dias_reserva_roubo     
                  , nome_estipulante     
                  , parentesco     
                  , val_lim_funeral     
                  , val_lim_dmho     
                  , qtd_cestas     
                  , val_cestas     
                  , dt_nasc_usuario     
                  , dt_inclusao     
                  , dt_alteracao     
                  , usuario     
                  , chave_principal    
      )    
    
      SELECT
	  PropostasAtivasSEGA9196.proposta_id  , 
     null, --SEGA9196_processar_tb.sega9196_processar_id,  
     null, --SEGA9196_processar_tb.processar_id,  
     null, --plano_assistencia_tb.num_contrato,  
     null, --plano_assistencia_tb.num_versao_contrato,  
     proposta_adesao_tb.apolice_id,  
      RIGHT('00000000'  
         + Cast( Isnull(CONVERT(VARCHAR(15), proposta_adesao_tb.proposta_bb), '') AS VARCHAR(15)), 8) AS proposta_externa,  
      --CASE SEGA9196_processar_tb.tp_movimento_id  
      --  WHEN 2 THEN 'C'  
      --  WHEN 7 THEN 'A'  
      --  ELSE 'I'  
      --END                                                                                             AS tp_operacao, 
	  null, 
      Isnull(cliente_tb.nome, '') COLLATE sql_latin1_general_cp1251_ci_as                                           AS nome_segurado,  
      proposta_adesao_tb.dt_inicio_vigencia,  
      Isnull(Cast(CONVERT(CHAR(15), PropostasAtivasSEGA9196.dt_fim_vigencia, 112) AS VARCHAR(15)), '')             AS dt_fim_vigencia,  
      Isnull(cliente_tb.cpf_cnpj, '')                                                                 AS cpf_cnpj_estipulante,  
      '00000000000'                                                                                   AS cpf_segurado,  
      Isnull(endereco_corresp_tb.endereco, '')                                                        AS endereco,  
      Isnull(endereco_corresp_tb.estado, '')                                                          AS estado,  
      Isnull(endereco_corresp_tb.municipio, '')                                                       AS municipio,  
      Isnull(endereco_corresp_tb.bairro, '')                                                          AS bairro,  
      Isnull(endereco_corresp_tb.cep, '')                                                             AS cep,  
      Isnull(replace(Rtrim(Ltrim(endereco_corresp_tb.ddd)),' ',''),0)      
      + Isnull(replace(Rtrim(Ltrim(endereco_corresp_tb.telefone)),' ',''),0)                          AS telefone,   
      ''                                                                                              AS endereco_risco,  
      ''                                                                                              AS uf_risco,  
      ''                                                                                              AS municipio_risco,  
      ''                                                                                              AS bairro_risco,  
      ''                                                                                              AS cep_risco,  
      0                                                                                               AS telefone_risco,  
      ''                                                                                              AS placa_veiculo,  
      ''                                                                                              AS chassi_veiculo,  
      ''                                                                                        AS cor_veiculo,  
      ''                                                                                              AS ano_fab_veiculo,  
      ''                                                                                              AS modelo_veiculo,  
      ''                                                                                              AS marca_veiculo,  
      ''                                                                                              AS dias_reserva_pparcial,  
      ''                                                                                              AS dias_reserva_ptotal,  
      ''                                                                                              AS dias_reserva_roubo,  
      Isnull(cliente_tb.nome, '')                                                                     AS nome_estipulante,  
      ''                                                                                              AS parentesco,  
      0                                                                                               AS val_lim_funeral,  
      Cast (0.00 AS NUMERIC(15, 2))                                                                   AS val_lim_dmho,  
      0                                                                                               AS qtd_cestas,  
      0                                                                                               AS val_cestas,  
      ''                                                                                              AS dt_nasc_usuario,  
      Getdate()                                                                                       AS dt_inclusao,  
      NULL                                                                                            AS dt_alteracao,  
      'C_RMORAIS'                                                                                        AS usuario,  
      Isnull(RIGHT('000000000' + Cast( proposta_tb.proposta_id AS VARCHAR(20)), 9)  
       + RIGHT('0000' + Cast( proposta_adesao_tb.ramo_id AS VARCHAR(15)), 4)  
       + '000' + '000000000', 0)                                                                  AS chave_principal    
    FROM --interface_dados_db.dbo.SEGA9196_processar_tb SEGA9196_processar_tb WITH (NOLOCK)    
     ##PropostasAtivas PropostasAtivasSEGA9196    
            --ON PropostasAtivasSEGA9196.proposta_id = SEGA9196_processar_tb.proposta_id      
       INNER JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)    --sp_linkedservers
               ON endereco_corresp_tb.proposta_id = PropostasAtivasSEGA9196.proposta_id    
       INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (nolock)    
               ON proposta_tb.proposta_id = PropostasAtivasSEGA9196.proposta_id    
       INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH (nolock)    
               ON proposta_adesao_tb.proposta_id = PropostasAtivasSEGA9196.proposta_id    
       INNER JOIN seguros_db.dbo.cliente_tb cliente_tb WITH (nolock)    
               ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id    
       --INNER JOIN assistencia_db.dbo.plano_assistencia_tb plano_assistencia_tb WITH (NOLOCK)    
       --     ON plano_assistencia_tb.plano_assistencia_id = PropostasAtivasSEGA9196.plano_assistencia_id          
       --     WHERE PropostasAtivasSEGA9196.plano_assistencia_id IN ( 4073, 4074 ) 