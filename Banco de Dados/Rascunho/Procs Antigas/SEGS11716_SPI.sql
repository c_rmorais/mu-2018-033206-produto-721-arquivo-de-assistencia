CREATE PROCEDURE dbo.SEGS11716_SPI  
                                              
@usuario VARCHAR(20)                                              
                                              
AS                                              
-----------------------------------------------------------                                              
-- Procedure Detalhe 10 - Segurado SEGA9169              --                                              
-----------------------------------------------------------                                              
  
BEGIN TRY                                             
 SET NOCOUNT ON                                              
 DECLARE @dt_sistema SMALLDATETIME        
                 
  
 SELECT @dt_sistema = dt_operacional                        
   FROM parametro_geral_tb (NOLOCK)                     
                             
   SELECT SEGA9169_processar_tb.sega9169_processar_id ,  
    SEGA9169_processar_tb.processar_id ,                   
    '40001172904' num_contrato,             
    0 versao_contrato,                   
    movimento_assistencia_vida_atual_tb.apolice_id ,                   
    ISNULL(CONVERT(VARCHAR(15),movimento_assistencia_vida_atual_tb.proposta_bb),'') proposta_externa,    
    CASE movimento_assistencia_vida_atual_tb.tp_movimentacao_id WHEN 1 THEN 'I' ELSE 'E' END tp_operacao,    
    movimento_assistencia_vida_atual_tb.nome_segurado COLLATE sql_latin1_general_cp1251_ci_as nome_segurado,     
    ISNULL(CAST(CONVERT(char,movimento_assistencia_vida_atual_tb.dt_inicio_vigencia ,112) AS VARCHAR),'') dt_inicio_vigencia,        
    ISNULL(CAST(CONVERT(char,movimento_assistencia_vida_atual_tb.dt_fim_vigencia ,112) AS VARCHAR),'') dt_fim_vigencia,       
    (REPLICATE('0', 14 - LEN(movimento_assistencia_vida_atual_tb.cpf_cnpj_estipulante))) + CONVERT(VARCHAR(14), movimento_assistencia_vida_atual_tb.cpf_cnpj_estipulante) cpf_cnpj_estipulante,                   
    (REPLICATE('0', 11 - LEN(movimento_assistencia_vida_atual_tb.cpf_segurado))) + CONVERT(VARCHAR(11), movimento_assistencia_vida_atual_tb.cpf_segurado) cpf_segurado,            
    ISNULL(seguros_db..endereco_corresp_tb.endereco,'') endereco,     
    ISNULL(seguros_db..endereco_corresp_tb.estado,'') estado,                   
    ISNULL(seguros_db..endereco_corresp_tb.municipio,'') municipio,                   
    ISNULL(seguros_db..endereco_corresp_tb.bairro,'') bairro,     
    ISNULL(seguros_db..endereco_corresp_tb.cep,'') cep,                   
    ISNULL(seguros_db..endereco_corresp_tb.ddd + seguros_db..endereco_corresp_tb.telefone,0) telefone,    
    '' endereco_risco,    
    '' uf_risco,     
    '' municipio_risco,    
    '' bairro_risco,     
    '' cep_risco,     
    0 telefone_risco,    
    '' placa_veiculo,    
    '' chassi_veiculo,     
    '' cor_veiculo,    
    '' ano_fab_veiculo,    
    '' modelo_veiculo,    
    '' marca_veiculo,    
    '' dias_reserva_pparcial,    
    '' dias_reserva_ptotal,    
    '' dias_reserva_roubo,    
    ISNULL(movimento_assistencia_vida_atual_tb.nome_estipulante,'') nome_estipulante,                   
    '' parentesco,     
    cast (0.00 as numeric(15,2)) val_lim_funeral,            
    cast (0.00 as numeric(15,2)) val_lim_dmho,    
    0 qtd_cestas,     
    0 val_cestas,     
    CONVERT(CHAR,ISNULL(seguros_db..pessoa_fisica_tb.dt_nascimento,CONVERT(DATETIME,'1900-01-01')),112) dt_nasc_usuario,                   
    chave_principal = ISNULL(RIGHT('' + CAST( movimento_assistencia_vida_atual_tb.apolice_id AS VARCHAR),9) +      
    RIGHT('0000' + CAST( movimento_assistencia_vida_atual_tb.ramo_id AS VARCHAR),4) +      
    RIGHT('000' + CAST( movimento_assistencia_vida_atual_tb.sub_grupo_id AS VARCHAR),3) +      
    RIGHT('00000000000000' + CAST( movimento_assistencia_vida_atual_tb.cpf_segurado AS VARCHAR),14),0),     
    movimento_assistencia_vida_atual_tb.tp_movimentacao_id,     
    movimento_assistencia_vida_atual_tb.sucursal_seguradora_id,     
    movimento_assistencia_vida_atual_tb.seguradora_cod_susep,      
    movimento_assistencia_vida_atual_tb.ramo_id,    
    movimento_assistencia_vida_atual_tb.sub_grupo_id     
  INTO #temp_SEGA9169_processar_tb    
  FROM interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK)           
  JOIN assistencia_db..movimento_assistencia_vida_atual_tb movimento_assistencia_vida_atual_tb WITH (NOLOCK)    
    ON movimento_assistencia_vida_atual_tb.apolice_id = SEGA9169_processar_tb.apolice_id            
   AND movimento_assistencia_vida_atual_tb.ramo_id = SEGA9169_processar_tb.ramo_id            
   AND movimento_assistencia_vida_atual_tb.sub_grupo_id = SEGA9169_processar_tb.sub_grupo_id            
   AND movimento_assistencia_vida_atual_tb.prop_cliente_id = SEGA9169_processar_tb.prop_cliente_id
   AND movimento_assistencia_vida_atual_tb.tp_movimentacao_id = SEGA9169_processar_tb.tp_movimento_id            
  JOIN seguros_db..pessoa_fisica_tb WITH (NOLOCK)    
    ON seguros_db..pessoa_fisica_tb.pf_cliente_id = movimento_assistencia_vida_atual_tb.prop_cliente_id                       
  JOIN seguros_db..endereco_corresp_tb WITH (NOLOCK)    
    ON seguros_db..endereco_corresp_tb.proposta_id = movimento_assistencia_vida_atual_tb.proposta_id     
 WHERE movimento_assistencia_vida_atual_tb.plano_assistencia_id = 2904 --assistencia escolar             
   AND movimento_assistencia_vida_atual_tb.enviar = 'S'            
   AND movimento_assistencia_vida_atual_tb.layout_id is null
   AND movimento_assistencia_vida_atual_tb.versao is null                    

  CREATE INDEX AK001_temp_SEGA9169_processar_tb on #temp_SEGA9169_processar_tb (tp_movimentacao_id)    
    
  UPDATE tmp    
     SET tmp.tp_operacao = mov.tp_operacao    
    FROM #temp_SEGA9169_processar_tb tmp    
    JOIN assistencia_db..tp_movimento_tb mov WITH (NOLOCK)    
   ON mov.tp_movimento_id = tmp.tp_movimentacao_id     
    
  CREATE INDEX AK002_temp_SEGA9169_processar_tb on #temp_SEGA9169_processar_tb (apolice_id, sucursal_seguradora_id, seguradora_cod_susep, ramo_id, sub_grupo_id)    
     
  CREATE TABLE #temp_apolice
  (  
  apolice_id numeric(9),
  sucursal_seguradora_id numeric(5),
  seguradora_cod_susep numeric(5),
  ramo_id int,
  sub_grupo_id int,
  val_is numeric(15,2) null,  
  perc_basica numeric(9,6) null   
  )  

INSERT INTO #temp_apolice (apolice_id,
           sucursal_seguradora_id,
           seguradora_cod_susep,
           ramo_id,
           sub_grupo_id)  
    SELECT apolice_id,
           sucursal_seguradora_id,
           seguradora_cod_susep,
           ramo_id,
           sub_grupo_id
      FROM #temp_SEGA9169_processar_tb   
     GROUP BY apolice_id,
           sucursal_seguradora_id,
           seguradora_cod_susep,
           ramo_id,
           sub_grupo_id

--CALCULANDO O VALOR DA COBERTURA DMHO
  SELECT e.apolice_id, e.sucursal_seguradora_id, e.seguradora_cod_susep, e.ramo_id, e.sub_grupo_id,
  CASE a.tp_is WHEN 'F' THEN e.val_is ELSE 0 END val_is_fixo    
   INTO #temp_dmho_1  
   FROM escolha_sub_grp_tp_cob_comp_tb e  
   JOIN sub_grupo_apolice_tb a  
     ON a.apolice_id = e.apolice_id  
    AND a.sucursal_seguradora_id = e.sucursal_seguradora_id  
    AND a.seguradora_cod_susep = e.seguradora_cod_susep  
    AND a.ramo_id = e.ramo_id  
    AND a.sub_grupo_id = e.sub_grupo_id  
    AND a.dt_inicio_vigencia_sbg = e.dt_inicio_vigencia_sbg  
   JOIN #temp_apolice td
     ON e.apolice_id = td.apolice_id  
    AND e.sucursal_seguradora_id = td.sucursal_seguradora_id  
    AND e.seguradora_cod_susep = td.seguradora_cod_susep  
    AND e.ramo_id = td.ramo_id  
    AND e.sub_grupo_id = td.sub_grupo_id  
  WHERE e.class_tp_cobertura = 'B'
    AND e.val_is > 0  
  
  UPDATE #temp_apolice   
     SET val_is = td.val_is_fixo  
    FROM #temp_dmho_1 td   
   WHERE td.apolice_id = #temp_apolice.apolice_id  
    AND td.sucursal_seguradora_id = #temp_apolice.sucursal_seguradora_id  
    AND td.seguradora_cod_susep = #temp_apolice.seguradora_cod_susep  
    AND td.ramo_id = #temp_apolice.ramo_id  
    AND td.sub_grupo_id = #temp_apolice.sub_grupo_id  

  SELECT e.apolice_id, e.sucursal_seguradora_id, e.seguradora_cod_susep, e.ramo_id, e.sub_grupo_id,
         e.perc_basica
    INTO #temp_dmho_2  
   FROM escolha_sub_grp_tp_cob_comp_tb e  
   JOIN tp_cob_comp_tb c  
     ON e.tp_cob_comp_id = c.tp_cob_comp_id  
   JOIN #temp_apolice td
     ON e.apolice_id = td.apolice_id  
    AND e.sucursal_seguradora_id = td.sucursal_seguradora_id  
    AND e.seguradora_cod_susep = td.seguradora_cod_susep  
    AND e.ramo_id = td.ramo_id  
    AND e.sub_grupo_id = td.sub_grupo_id  
   WHERE c.tp_cobertura_id = 830 -- Cobertura DMHO

  UPDATE #temp_apolice   
     SET perc_basica = td.perc_basica  
    FROM #temp_dmho_2 td   
   WHERE td.apolice_id = #temp_apolice.apolice_id  
    AND td.sucursal_seguradora_id = #temp_apolice.sucursal_seguradora_id  
    AND td.seguradora_cod_susep = #temp_apolice.seguradora_cod_susep  
    AND td.ramo_id = #temp_apolice.ramo_id  
    AND td.sub_grupo_id = #temp_apolice.sub_grupo_id  

  UPDATE #temp_SEGA9169_processar_tb   
     SET val_lim_dmho = cast ( td.val_is * ( isnull(td.perc_basica,0) / 100.00)  as numeric (15,2) )  
    FROM #temp_SEGA9169_processar_tb  
    JOIN #temp_apolice td   
      ON td.apolice_id = #temp_SEGA9169_processar_tb.apolice_id   
    AND td.sucursal_seguradora_id = #temp_SEGA9169_processar_tb.sucursal_seguradora_id  
    AND td.seguradora_cod_susep = #temp_SEGA9169_processar_tb.seguradora_cod_susep  
    AND td.ramo_id = #temp_SEGA9169_processar_tb.ramo_id  
    AND td.sub_grupo_id = #temp_SEGA9169_processar_tb.sub_grupo_id 


--CALCULANDO O VALOR DA COBERTURA FUNERAL
  SELECT e.apolice_id, e.sucursal_seguradora_id, e.seguradora_cod_susep, e.ramo_id, e.sub_grupo_id,
         e.perc_basica, c.val_fixo_cobertura, c.tp_val_cobertura
    INTO #temp_funeral_1  
   FROM escolha_sub_grp_tp_cob_comp_tb e  
   JOIN tp_cob_comp_tb c  
     ON e.tp_cob_comp_id = c.tp_cob_comp_id  
   JOIN #temp_apolice td
     ON e.apolice_id = td.apolice_id  
    AND e.sucursal_seguradora_id = td.sucursal_seguradora_id  
    AND e.seguradora_cod_susep = td.seguradora_cod_susep  
    AND e.ramo_id = td.ramo_id  
    AND e.sub_grupo_id = td.sub_grupo_id  
  WHERE c.tp_cobertura_id IN (313,819) -- Cobertura Auxilio funeral

  UPDATE #temp_apolice   
     SET perc_basica = td.perc_basica  
    FROM #temp_funeral_1 td   
   WHERE td.apolice_id = #temp_apolice.apolice_id  
    AND td.sucursal_seguradora_id = #temp_apolice.sucursal_seguradora_id  
    AND td.seguradora_cod_susep = #temp_apolice.seguradora_cod_susep  
    AND td.ramo_id = #temp_apolice.ramo_id  
    AND td.sub_grupo_id = #temp_apolice.sub_grupo_id  

  UPDATE #temp_SEGA9169_processar_tb   
     SET val_lim_funeral = CASE WHEN isnull (td.tp_val_cobertura, 'P') = 'F' THEN td.val_fixo_cobertura ELSE cast ( ta.val_is * ( isnull(ta.perc_basica,0) / 100.00)  as numeric (15,2) )  END
    FROM #temp_SEGA9169_processar_tb  
    JOIN #temp_funeral_1 td   
      ON td.apolice_id = #temp_SEGA9169_processar_tb.apolice_id   
    AND td.sucursal_seguradora_id = #temp_SEGA9169_processar_tb.sucursal_seguradora_id  
    AND td.seguradora_cod_susep = #temp_SEGA9169_processar_tb.seguradora_cod_susep  
    AND td.ramo_id = #temp_SEGA9169_processar_tb.ramo_id  
    AND td.sub_grupo_id = #temp_SEGA9169_processar_tb.sub_grupo_id 
    JOIN #temp_apolice ta   
      ON ta.apolice_id = #temp_SEGA9169_processar_tb.apolice_id   
    AND ta.sucursal_seguradora_id = #temp_SEGA9169_processar_tb.sucursal_seguradora_id  
    AND ta.seguradora_cod_susep = #temp_SEGA9169_processar_tb.seguradora_cod_susep  
    AND ta.ramo_id = #temp_SEGA9169_processar_tb.ramo_id  
    AND ta.sub_grupo_id = #temp_SEGA9169_processar_tb.sub_grupo_id 
       

INSERT INTO interface_dados_db..SEGA9169_10_processar_tb      
      (sega9169_processar_id,  
       processar_id,                  
       num_contrato ,                   
       versao_contrato ,                  
       apolice_id ,                   
       proposta_externa ,                   
       tp_movimento ,                  
       nome_usuario ,                   
       dt_ini_vigencia ,                   
       dt_fim_vigencia ,                  
       cnpj ,                   
       cpf ,                   
       endereco_usuario ,                   
       uf_usuario ,                   
       municipio_usuario ,                  
       bairro_usuario ,                  
       cep_usuario ,                  
       telefone_usuario ,                  
       endereco_risco ,                  
       uf_risco ,                  
       municipio_risco ,                  
       bairro_risco ,                  
       cep_risco ,                  
       telefone_risco ,                  
       placa_veiculo ,                  
       chassi_veiculo ,                  
       cor_veiculo ,                  
       ano_fab_veiculo ,                  
       modelo_veiculo ,                  
       marca_veiculo ,                  
       dias_reserva_pparcial ,                  
       dias_reserva_ptotal ,                  
       dias_reserva_roubo ,                  
       nome_estipulante ,                  
       parentesco ,                  
       val_lim_funeral ,                  
       val_lim_dmho ,                  
       qtd_cestas ,                  
       val_cestas ,                  
       dt_nasc_usuario ,                  
       dt_inclusao ,                  
       dt_alteracao ,                  
       usuario,      
       chave_principal )        
    SELECT sega9169_processar_id,  
        processar_id ,                   
        num_contrato,             
        versao_contrato,                   
        apolice_id ,                   
        proposta_externa,    
        tp_operacao,    
        nome_segurado ,     
        dt_inicio_vigencia,        
        dt_fim_vigencia,        
        cpf_cnpj_estipulante,                   
        cpf_segurado,            
        endereco,    
        estado,    
        municipio,    
        bairro,    
        cep,    
        telefone,    
        endereco_risco,    
        uf_risco,     
        municipio_risco,    
        bairro_risco,     
        cep_risco,     
        telefone_risco,    
        placa_veiculo,    
        chassi_veiculo,     
        cor_veiculo,    
        ano_fab_veiculo,    
        modelo_veiculo,    
        marca_veiculo,    
        dias_reserva_pparcial,    
        dias_reserva_ptotal,    
        dias_reserva_roubo,    
        nome_estipulante,                   
        parentesco,     
        val_lim_funeral,            
        val_lim_dmho,    
        qtd_cestas,     
        val_cestas,     
        dt_nasc_usuario,                   
        @dt_sistema,                    
        NULL,                   
        @usuario,      
        chave_principal    
     FROM #temp_SEGA9169_processar_tb    
    
	-- alterações 18720075 - Alterações produto 1206
select SEGA9169_processar_tb.sega9169_processar_id ,  
    SEGA9169_processar_tb.processar_id ,                   
    '40000053479' as num_contrato,             
    0 as versao_contrato,                   
	apolice_tb.apolice_id,
	RIGHT('00000000' + CAST( ISNULL(CONVERT(VARCHAR(15),proposta_fechadA_tb.proposta_bb),'') AS VARCHAR),8)   as proposta_externa,
    CASE SEGA9169_processar_tb.tp_movimento_id	WHEN 1 THEN 'I' WHEN 3 THEN 'A'	ELSE 'E' END tp_operacao,    
    cliente_tb.nome COLLATE sql_latin1_general_cp1251_ci_as nome_segurado,     
	apolice_tb.dt_inicio_vigencia, 
	ISNULL(CAST(CONVERT(char,apolice_tb.dt_fim_vigencia ,112) AS VARCHAR),'') dt_fim_vigencia,
	cliente_tb.cpf_cnpj as cpf_cnpj_estipulante,
	'00000000000' as cpf_segurado,
    ISNULL(endereco_corresp_tb.endereco,'') endereco,   
    ISNULL(endereco_corresp_tb.estado,'') estado,                 
    ISNULL(endereco_corresp_tb.municipio,'') municipio,                 
    ISNULL(endereco_corresp_tb.bairro,'') bairro,   
    ISNULL(endereco_corresp_tb.cep,'') cep,                 
     ISNULL(rtrim(ltrim(endereco_corresp_tb.ddd)) + rtrim(ltrim(endereco_corresp_tb.telefone)),0) telefone,  
    '' endereco_risco,  
    '' uf_risco,   
    '' municipio_risco,  
    '' bairro_risco,   
    '' cep_risco,   
    0 telefone_risco,  
    '' placa_veiculo,  
    '' chassi_veiculo,   
    '' cor_veiculo,  
    '' ano_fab_veiculo,  
    '' modelo_veiculo,  
    '' marca_veiculo,  
    '' dias_reserva_pparcial,  
    '' dias_reserva_ptotal,  
    '' dias_reserva_roubo,  
    cliente_tb.nome as nome_estipulante, 
    '' parentesco,   
    0 val_lim_funeral,          
    cast (0.00 as numeric(15,2)) val_lim_dmho,  
    0 qtd_cestas,   
    0 val_cestas,   
    '' as dt_nasc_usuario, 
	getdate() as dt_inclusao,
	NULL as dt_alteracao,
	@usuario as usuario,
	 ISNULL(RIGHT('000000000' + CAST( apolice_tb.apolice_id AS VARCHAR),9) + RIGHT('0000' + CAST( apolice_tb.ramo_id AS VARCHAR),4) 
		+ '000' + '000000000',0)  as chave_principal
  INTO #temp_SEGA9169_10_processar_tb_3479  
FROM interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK) 
	join assistencia_db..proposta_assistencia_atual_tb with (nolock)
		on SEGA9169_processar_tb.proposta_id = proposta_assistencia_atual_tb.proposta_id                     
  JOIN seguros_db..endereco_corresp_tb WITH (NOLOCK)  
    ON seguros_db..endereco_corresp_tb.proposta_id = SEGA9169_processar_tb.proposta_id   
	join seguros_db..proposta_tb with (nolock)
		on proposta_tb.proposta_id = SEGA9169_processar_tb.proposta_id
	join seguros_db..apolice_tb with (nolock)
		on apolice_tb.proposta_id = SEGA9169_processar_tb.proposta_id
	join seguros_db..proposta_fechada_tb with (nolock)
		on proposta_fechada_tb.proposta_id = SEGA9169_processar_tb.proposta_id
	join seguros_db..cliente_tb with (nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_id
	where SEGA9169_processar_tb.produto_id = 1206 
		and SEGA9169_processar_tb.plano_assistencia_id in (3479)
		
		
		
		select SEGA9169_processar_tb.sega9169_processar_id ,  
    SEGA9169_processar_tb.processar_id ,                   
    '40000043481' as num_contrato,             
    0 as versao_contrato,                   
	apolice_tb.apolice_id,
	RIGHT('00000000' + CAST( ISNULL(CONVERT(VARCHAR(15),proposta_fechadA_tb.proposta_bb),'') AS VARCHAR),8)   as proposta_externa,
    CASE SEGA9169_processar_tb.tp_movimento_id	WHEN 1 THEN 'I' WHEN 3 THEN 'A'	ELSE 'E' END tp_operacao,    
    cliente_tb.nome COLLATE sql_latin1_general_cp1251_ci_as nome_segurado,     
	apolice_tb.dt_inicio_vigencia, 
	ISNULL(CAST(CONVERT(char,apolice_tb.dt_fim_vigencia ,112) AS VARCHAR),'') dt_fim_vigencia,
	cliente_tb.cpf_cnpj as cpf_cnpj_estipulante,
	'00000000000' as cpf_segurado,
    ISNULL(endereco_corresp_tb.endereco,'') endereco,   
    ISNULL(endereco_corresp_tb.estado,'') estado,                 
    ISNULL(endereco_corresp_tb.municipio,'') municipio,                 
    ISNULL(endereco_corresp_tb.bairro,'') bairro,   
    ISNULL(endereco_corresp_tb.cep,'') cep,                 
     ISNULL(rtrim(ltrim(endereco_corresp_tb.ddd)) + rtrim(ltrim(endereco_corresp_tb.telefone)),0) telefone,  
    '' endereco_risco,  
    '' uf_risco,   
    '' municipio_risco,  
    '' bairro_risco,   
    '' cep_risco,   
    0 telefone_risco,  
    '' placa_veiculo,  
    '' chassi_veiculo,   
    '' cor_veiculo,  
    '' ano_fab_veiculo,  
    '' modelo_veiculo,  
    '' marca_veiculo,  
    '' dias_reserva_pparcial,  
    '' dias_reserva_ptotal,  
    '' dias_reserva_roubo,  
    cliente_tb.nome as nome_estipulante, 
    '' parentesco,   
    0 val_lim_funeral,          
    cast (0.00 as numeric(15,2)) val_lim_dmho,  
    0 qtd_cestas,   
    0 val_cestas,   
    '' as dt_nasc_usuario, 
	getdate() as dt_inclusao,
	NULL as dt_alteracao,
	@usuario as usuario,
	 ISNULL(RIGHT('000000000' + CAST( apolice_tb.apolice_id AS VARCHAR),9) + RIGHT('0000' + CAST( apolice_tb.ramo_id AS VARCHAR),4) 
		+ '000' + '000000000',0)  as chave_principal
  INTO #temp_SEGA9169_10_processar_tb_3481  
FROM interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK) 
	join assistencia_db..proposta_assistencia_atual_tb with (nolock)
		on SEGA9169_processar_tb.proposta_id = proposta_assistencia_atual_tb.proposta_id                     
  JOIN seguros_db..endereco_corresp_tb WITH (NOLOCK)  
    ON seguros_db..endereco_corresp_tb.proposta_id = SEGA9169_processar_tb.proposta_id   
	join seguros_db..proposta_tb with (nolock)
		on proposta_tb.proposta_id = SEGA9169_processar_tb.proposta_id
	join seguros_db..apolice_tb with (nolock)
		on apolice_tb.proposta_id = SEGA9169_processar_tb.proposta_id
	join seguros_db..proposta_fechada_tb with (nolock)
		on proposta_fechada_tb.proposta_id = SEGA9169_processar_tb.proposta_id
	join seguros_db..cliente_tb with (nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_id
	where SEGA9169_processar_tb.produto_id = 1206 
		and SEGA9169_processar_tb.plano_assistencia_id in (3481)
		

INSERT INTO interface_dados_db..SEGA9169_10_processar_tb  
      (sega9169_processar_id,  
       processar_id,                  
       num_contrato ,                   
       versao_contrato ,                  
       apolice_id ,                   
       proposta_externa ,                   
       tp_movimento ,                  
       nome_usuario ,                   
       dt_ini_vigencia ,                   
       dt_fim_vigencia ,                  
       cnpj ,                   
       cpf ,                   
       endereco_usuario ,                   
       uf_usuario ,                   
       municipio_usuario ,                  
       bairro_usuario ,                  
       cep_usuario ,                  
       telefone_usuario ,                  
       endereco_risco ,                  
       uf_risco ,                  
       municipio_risco ,                  
       bairro_risco ,                  
       cep_risco ,                  
       telefone_risco ,                  
       placa_veiculo ,                  
       chassi_veiculo ,                  
       cor_veiculo ,                  
       ano_fab_veiculo ,                  
       modelo_veiculo ,                  
       marca_veiculo ,                  
       dias_reserva_pparcial ,                  
       dias_reserva_ptotal ,                  
       dias_reserva_roubo ,                  
       nome_estipulante ,                  
       parentesco ,                  
       val_lim_funeral ,                  
       val_lim_dmho ,                  
       qtd_cestas ,                  
       val_cestas ,                  
       dt_nasc_usuario ,                  
       dt_inclusao ,                  
       dt_alteracao ,                  
       usuario,      
       chave_principal )        
    SELECT sega9169_processar_id,  
        processar_id ,                   
        num_contrato,             
        versao_contrato,                   
        apolice_id ,                   
        proposta_externa,    
        tp_operacao,    
        nome_segurado ,     
        dt_inicio_vigencia,        
        dt_fim_vigencia,        
        cpf_cnpj_estipulante,                   
        cpf_segurado,            
        endereco,    
        estado,    
        municipio,    
        bairro,    
        cep,    
        telefone,    
        endereco_risco,    
        uf_risco,     
        municipio_risco,    
        bairro_risco,     
        cep_risco,     
        telefone_risco,    
        placa_veiculo,    
        chassi_veiculo,     
        cor_veiculo,    
        ano_fab_veiculo,    
        modelo_veiculo,    
        marca_veiculo,    
        dias_reserva_pparcial,    
        dias_reserva_ptotal,    
        dias_reserva_roubo,    
        nome_estipulante,                   
        parentesco,     
        val_lim_funeral,            
        val_lim_dmho,    
        qtd_cestas,     
        val_cestas,     
        dt_nasc_usuario,                   
        @dt_sistema,                    
        NULL,                   
        @usuario,      
        chave_principal    
     FROM #temp_SEGA9169_10_processar_tb_3479
union
	 SELECT sega9169_processar_id,  
        processar_id ,                   
        num_contrato,             
        versao_contrato,                   
        apolice_id ,                   
        proposta_externa,    
        tp_operacao,    
        nome_segurado ,     
        dt_inicio_vigencia,        
        dt_fim_vigencia,        
        cpf_cnpj_estipulante,                   
        cpf_segurado,            
        endereco,    
        estado,    
        municipio,    
        bairro,    
        cep,    
        telefone,    
        endereco_risco,    
        uf_risco,     
        municipio_risco,    
        bairro_risco,     
        cep_risco,     
        telefone_risco,    
        placa_veiculo,    
        chassi_veiculo,     
        cor_veiculo,    
        ano_fab_veiculo,    
        modelo_veiculo,    
        marca_veiculo,    
        dias_reserva_pparcial,    
        dias_reserva_ptotal,    
        dias_reserva_roubo,    
        nome_estipulante,                   
        parentesco,     
        val_lim_funeral,            
        val_lim_dmho,    
        qtd_cestas,     
        val_cestas,     
        dt_nasc_usuario,                   
        @dt_sistema,                    
        NULL,                   
        @usuario,      
        chave_principal    
     FROM #temp_SEGA9169_10_processar_tb_3481
	 
--------------------------------------------------------------------------------------------     
-- inicio alterações assistencias para os produtos 1235, 1236 e 1237 - Demanda 18234489   
-------------------------------------------------------------------------------------------- 

--obtendo planos de assistencias dos produtos 1235, 1236 e 1237
SELECT plano_assistencia_id, tipo_assistencia_id
  INTO #plano_assistencia
  FROM assistencia_db..plano_assistencia_tb WITH (NOLOCK)  
 WHERE tipo_assistencia_id IN (74, 75, 76, 77, 78, 79, 80)
   AND (dt_fim_vigencia IS NULL OR dt_fim_vigencia >= @dt_sistema)	
 
select SEGA9169_processar_tb.sega9169_processar_id ,  
       SEGA9169_processar_tb.processar_id ,                   
       CASE WHEN #plano_assistencia.tipo_assistencia_id = 74 --PROD_1235 - PLANO 1
            THEN '40000053479' 
            WHEN #plano_assistencia.tipo_assistencia_id = 75 --PROD_1235 - PLANO 2
            THEN '40000053479' 
            WHEN #plano_assistencia.tipo_assistencia_id = 76 --PROD_1235 - PLANO 3
            THEN '40000053479' 
            WHEN #plano_assistencia.tipo_assistencia_id = 77 --PROD_1236 - PLANO 1
            THEN '40000053479' 
            WHEN #plano_assistencia.tipo_assistencia_id = 78 --PROD_1237 - PLANO 1
            THEN '40000053479'       
            WHEN #plano_assistencia.tipo_assistencia_id = 79 --PROD_1237 - PLANO 2 
            THEN '40000053479'      
            WHEN #plano_assistencia.tipo_assistencia_id = 80 --PROD_1237 - PLANO 3
            THEN '40000053479'
            ELSE '99999999999'                                                    
       END AS num_contrato,                    
       0 AS versao_contrato,                   
       apolice_tb.apolice_id,
       RIGHT('00000000' + CAST(ISNULL(CONVERT(VARCHAR(15), proposta_fechada_tb.proposta_bb), '') AS VARCHAR),8) as proposta_externa,
       CASE WHEN SEGA9169_processar_tb.tp_movimento_id = 1 
            THEN 'I' 
            WHEN SEGA9169_processar_tb.tp_movimento_id = 3 
            THEN 'A'
            ELSE 'E' 
       END AS tp_operacao,    
       cliente_tb.nome COLLATE sql_latin1_general_cp1251_ci_as AS nome_segurado,     
       apolice_tb.dt_inicio_vigencia, 
       ISNULL(CAST(CONVERT(CHAR, apolice_tb.dt_fim_vigencia, 112) AS VARCHAR), '') dt_fim_vigencia,
       cliente_tb.cpf_cnpj AS cpf_cnpj_estipulante,
       '00000000000' AS cpf_segurado,
       ISNULL(endereco_corresp_tb.endereco, '') endereco,   
       ISNULL(endereco_corresp_tb.estado, '') estado,                 
       ISNULL(endereco_corresp_tb.municipio, '') municipio,                 
       ISNULL(endereco_corresp_tb.bairro, '') bairro,   
       ISNULL(endereco_corresp_tb.cep, '') cep,                 
       ISNULL(RTRIM(LTRIM(endereco_corresp_tb.ddd)) + RTRIM(LTRIM(endereco_corresp_tb.telefone)),0) telefone,  
       '' endereco_risco,  
       '' uf_risco,   
       '' municipio_risco,  
       '' bairro_risco,   
       '' cep_risco,   
       0 telefone_risco,  
       '' placa_veiculo,  
       '' chassi_veiculo,   
       '' cor_veiculo,  
       '' ano_fab_veiculo,  
       '' modelo_veiculo,  
       '' marca_veiculo,  
       '' dias_reserva_pparcial,  
       '' dias_reserva_ptotal,  
       '' dias_reserva_roubo,  
       cliente_tb.nome AS nome_estipulante, 
       '' parentesco,   
       0 val_lim_funeral,          
       CAST(0.00 AS NUMERIC(15,2)) val_lim_dmho,  
       0 qtd_cestas,   
       0 val_cestas,   
       '' AS dt_nasc_usuario, 
       GETDATE() as dt_inclusao,
       NULL as dt_alteracao,
       @usuario as usuario,
       ISNULL(RIGHT('000000000' + CAST(apolice_tb.apolice_id AS VARCHAR), 9) + RIGHT('0000' + CAST(apolice_tb.ramo_id AS VARCHAR), 4) + '000' + '000000000', 0) AS chave_principal
  INTO #temp_SEGA9169_10_processar_tb_novos_produtos  
  FROM interface_dados_db..SEGA9169_processar_tb SEGA9169_processar_tb WITH (NOLOCK) 
 INNER JOIN assistencia_db..proposta_assistencia_atual_tb WITH (NOLOCK)
    ON SEGA9169_processar_tb.proposta_id = proposta_assistencia_atual_tb.proposta_id                     
 INNER JOIN seguros_db..endereco_corresp_tb WITH (NOLOCK)  
    ON seguros_db..endereco_corresp_tb.proposta_id = SEGA9169_processar_tb.proposta_id   
 INNER JOIN seguros_db..proposta_tb WITH (NOLOCK)
    ON proposta_tb.proposta_id = SEGA9169_processar_tb.proposta_id
 INNER JOIN seguros_db..apolice_tb WITH (NOLOCK)
    ON apolice_tb.proposta_id = SEGA9169_processar_tb.proposta_id
 INNER JOIN seguros_db..proposta_fechada_tb WITH (NOLOCK)
    ON proposta_fechada_tb.proposta_id = SEGA9169_processar_tb.proposta_id
 INNER JOIN seguros_db..cliente_tb WITH (NOLOCK)
    ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id
 INNER JOIN #plano_assistencia
    ON #plano_assistencia.plano_assistencia_id = SEGA9169_processar_tb.plano_assistencia_id			
 WHERE SEGA9169_processar_tb.produto_id IN (1235, 1236, 1237) 

INSERT INTO interface_dados_db..SEGA9169_10_processar_tb  
      (sega9169_processar_id,  
       processar_id,                  
       num_contrato ,                   
       versao_contrato ,                  
       apolice_id ,                   
       proposta_externa ,                   
       tp_movimento ,                  
       nome_usuario ,                   
       dt_ini_vigencia ,                   
       dt_fim_vigencia ,                  
       cnpj ,                   
       cpf ,                   
       endereco_usuario ,                   
       uf_usuario ,                   
       municipio_usuario ,                  
       bairro_usuario ,                  
       cep_usuario ,                  
       telefone_usuario ,                  
       endereco_risco ,                  
       uf_risco ,                  
       municipio_risco ,                  
       bairro_risco ,                  
       cep_risco ,                  
       telefone_risco ,                  
       placa_veiculo ,                  
       chassi_veiculo ,                  
       cor_veiculo ,                  
       ano_fab_veiculo ,                  
       modelo_veiculo ,                  
       marca_veiculo ,                  
       dias_reserva_pparcial ,                  
       dias_reserva_ptotal ,                  
       dias_reserva_roubo ,                  
       nome_estipulante ,                  
       parentesco ,                  
       val_lim_funeral ,                  
       val_lim_dmho ,                  
       qtd_cestas ,                  
       val_cestas ,                  
       dt_nasc_usuario ,                  
       dt_inclusao ,                  
       dt_alteracao ,                  
       usuario,      
       chave_principal )        
SELECT sega9169_processar_id,  
       processar_id ,                   
       num_contrato,             
       versao_contrato,                   
       apolice_id ,                   
       proposta_externa,    
       tp_operacao,    
       nome_segurado ,     
       dt_inicio_vigencia,        
       dt_fim_vigencia,        
       cpf_cnpj_estipulante,                   
       cpf_segurado,            
       endereco,    
       estado,    
       municipio,    
       bairro,    
       cep,    
       telefone,    
       endereco_risco,    
       uf_risco,     
       municipio_risco,    
       bairro_risco,     
       cep_risco,     
       telefone_risco,    
       placa_veiculo,    
       chassi_veiculo,     
       cor_veiculo,    
       ano_fab_veiculo,    
       modelo_veiculo,    
       marca_veiculo,    
       dias_reserva_pparcial,    
       dias_reserva_ptotal,    
       dias_reserva_roubo,    
       nome_estipulante,                   
       parentesco,     
       val_lim_funeral,            
       val_lim_dmho,    
       qtd_cestas,     
       val_cestas,     
       dt_nasc_usuario,                   
       dt_inclusao,                    
       dt_alteracao,                   
       usuario,      
       chave_principal    
  FROM #temp_SEGA9169_10_processar_tb_novos_produtos

--------------------------------------------------------------------------------------------    
-- fim alteração assistencias para os produtos 1235, 1236 e 1237 - Demanda 18234489   
--------------------------------------------------------------------------------------------
	
	update assistencia_db..movimento_assistencia_atual_tb
		set tp_movimento_id = 1
	where proposta_id in (select proposta_id from interface_dados_db..sega9169_processar_tb)
	
                            
  SET NOCOUNT OFF                                                
  RETURN                                                
END TRY                                                 
BEGIN CATCH    
    
  DECLARE @ErrorMessage NVARCHAR(4000)    
  DECLARE @ErrorSeverity INT    
  DECLARE @ErrorState INT    
    
  SELECT     
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),    
      @ErrorSeverity = ERROR_SEVERITY(),    
      @ErrorState = ERROR_STATE()    
    
      -- Use RAISERROR inside the CATCH block to return error    
      -- information about the original error that caused    
      -- execution to jump to the CATCH block.    
      RAISERROR (@ErrorMessage, -- Message text.    
                 @ErrorSeverity, -- Severity.    
                 @ErrorState -- State.    
                 )    
    
END CATCH


