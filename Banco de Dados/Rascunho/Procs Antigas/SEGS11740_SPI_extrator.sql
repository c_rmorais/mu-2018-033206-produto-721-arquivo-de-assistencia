CREATE PROCEDURE SEGS11740_SPI  
 @usuario VARCHAR(20),      
 @dt_sistema SMALLDATETIME = NULL      
    
AS      
    
SET NOCOUNT ON    
    
/*      
DECLARE @usuario VARCHAR(20)      
DECLARE @dt_sistema SMALLDATETIME      
      
SET @usuario = 'producao2'      
SET @dt_sistema = null      
*/    
    
DECLARE @TOTAL AS INT      
SET @TOTAL = 0      
    
 DECLARE @TOTAL_1206 AS INT  
 SET @TOTAL_1206 = 0  
    
IF @dt_sistema IS NULL      
BEGIN      
SELECT @dt_sistema = DT_OPERACIONAL      
  FROM PARAMETRO_GERAL_TB WITH (NOLOCK)      
END      
    
-- Selecionando todas as movimenta��es de assist�ncia escolar (2094)    
SELECT RANK() OVER (PARTITION BY proposta_id, sub_grupo_id, prop_cliente_id, tp_componente_id    
                    ORDER BY dt_inclusao DESC) AS ordem,    
       apolice_id,    
       sucursal_seguradora_id,    
       seguradora_cod_susep,    
       ramo_id,    
       sub_grupo_id,    
       proposta_id,    
       prop_cliente_id,    
       cpf_segurado,    
       ISNULL(tp_componente_id, 1) AS tp_componente_id,    
       tp_movimentacao_id,    
       layout_id    
  INTO #movimentacoes_anteriores    
  FROM assistencia_db.dbo.movimento_assistencia_vida_atual_tb m    
 WHERE plano_assistencia_id = 2904    
   -- Retirar casos do layout 1560 - SEGA9131 - MOVIMENTO DI�RIO DA ASSIST�NCIA DE VIDA EM GRUPO - AB PARA BRASIL ASSIST�NCIA    
   AND (layout_id IS NULL OR layout_id <> 1560)    
    
-- Selecionando as �ltimas movimenta��es de cada proposta    
SELECT *    
  INTO #ultimas_movimentacoes    
  FROM #movimentacoes_anteriores    
 WHERE ordem = 1    
    
-- Selecionando inclus�es de assist�ncia    
SELECT sv.apolice_id        
      ,sv.sucursal_seguradora_id        
      ,sv.seguradora_cod_susep            
      ,sv.ramo_id        
      ,sv.sub_grupo_id        
      ,sv.proposta_id         
      ,sv.prop_cliente_id        
      ,sv.tp_componente_id        
      ,sv.dt_inicio_vigencia_seg        
      ,sv.dt_inicio_vigencia_sbg        
      ,sv.seq_canc_endosso_seg        
      ,cprop.cpf_cnpj AS cpf_cnpj_estipulante        
      ,CASE      
          WHEN sg.dt_ini_assist_sbg > sv.dt_inicio_vigencia_seg      
          THEN sg.dt_ini_assist_sbg      
          ELSE sv.dt_inicio_vigencia_seg      
       END AS dt_inicio_vigencia        
      ,NULL AS dt_fim_vigencia      
      ,pe.cpf AS cpf_segurado        
      ,sv.certificado_id        
      ,pf.proposta_bb      
      ,cvida.nome AS nome_segurado      
      ,cprop.nome AS nome_estipulante      
      ,1 AS tp_movimentacao_id    
  INTO #assistencia_inclusao    
  FROM proposta_tb p WITH (NOLOCK)      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = p.proposta_id      
  JOIN cliente_tb cprop WITH (NOLOCK)      
    ON cprop.cliente_id = p.prop_cliente_id      
  JOIN apolice_tb a WITH (NOLOCK)      
    ON a.proposta_id = p.proposta_id      
   AND (a.dt_fim_vigencia IS NULL    
    OR (a.dt_fim_vigencia IS NOT NULL      
   AND a.dt_fim_vigencia >= @dt_sistema)) -- Apolice Vigente      
  JOIN sub_grupo_apolice_tb sa WITH (NOLOCK)      
    ON sa.seguradora_cod_susep = a.seguradora_cod_susep      
   AND sa.sucursal_seguradora_id = a.sucursal_seguradora_id      
   AND sa.ramo_id = a.ramo_id      
   AND sa.apolice_id = a.apolice_id      
   AND sa.dt_fim_vigencia_sbg IS NULL -- Subgrupos Ativos      
  JOIN assistencia_db..sub_grupo_assistencia_atual_tb sg WITH (NOLOCK)      
    ON sg.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sg.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sg.ramo_id = sa.ramo_id      
   AND sg.apolice_id = sa.apolice_id      
   AND sg.sub_grupo_id = sa.sub_grupo_id      
   AND sg.plano_assistencia_id = 2904 -- Assistencia Escolar      
   AND sg.dt_ini_assist_sbg IS NOT NULL      
   AND sg.dt_ini_assist_sbg <= @dt_sistema -- Vigente      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.seguradora_cod_susep = sa.seguradora_cod_susep        
   AND sv.sucursal_seguradora_id = sa.sucursal_seguradora_id        
   AND sv.ramo_id = sa.ramo_id      
   AND sv.apolice_id = sa.apolice_id      
   AND sv.sub_grupo_id = sa.sub_grupo_id      
   AND sv.dt_fim_vigencia_sbg IS NULL -- Vida Ativa      
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                    FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                     WHERE sv2.apolice_id = sv.apolice_id      
                                       AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                       AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                       AND sv2.ramo_id = sv.ramo_id      
                                       AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                       AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                       AND sv2.proposta_id = sv.proposta_id      
                                       AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                       AND sv2.tp_componente_id = sv.tp_componente_id      
                                       AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                     ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)      
    ON pe.pf_cliente_id = cvida.cliente_id   
  JOIN escolha_sub_grp_tp_cob_comp_tb es  
 ON es.apolice_id = sa.apolice_id    
   AND es.sucursal_seguradora_id = sa.sucursal_seguradora_id    
   AND es.seguradora_cod_susep = sa.seguradora_cod_susep    
   AND es.ramo_id = sa.ramo_id    
   AND es.sub_grupo_id = sa.sub_grupo_id   
  JOIN tp_cob_comp_tb cc WITH (NOLOCK)      
    ON cc.tp_cob_comp_id = es.tp_cob_comp_id    
   AND cc.tp_cobertura_id = 830 -- Cobertura DMHO    
 WHERE p.produto_id in (115,123,150)      
   AND p.ramo_id = 82      
   AND p.situacao = 'i'      
    
  
  
-- Retirando as vidas que j� foram inclu�das    
DELETE a    
  FROM #assistencia_inclusao a    
 WHERE EXISTS (SELECT 1    
                 FROM #ultimas_movimentacoes u    
                WHERE u.proposta_id = a.proposta_id    
                  AND u.sub_grupo_id = a.sub_grupo_id    
                  AND u.prop_cliente_id = a.prop_cliente_id    
                  AND u.tp_componente_id = a.tp_componente_id    
                  -- Inclus�o de assist�ncia    
                  AND (u.layout_id IS NULL    
                   OR (u.layout_id IS NOT NULL AND u.tp_movimentacao_id = 1)))    
    
-- Selecionando exclus�es de assist�ncia    
-- 1) Vidas n�o mais vigentes    
SELECT sv.apolice_id        
      ,sv.sucursal_seguradora_id        
      ,sv.seguradora_cod_susep            
      ,sv.ramo_id        
      ,sv.sub_grupo_id        
      ,sv.proposta_id         
      ,sv.prop_cliente_id        
      ,sv.tp_componente_id        
      ,sv.dt_inicio_vigencia_seg        
      ,sv.dt_inicio_vigencia_sbg        
      ,sv.seq_canc_endosso_seg        
      ,cprop.cpf_cnpj AS cpf_cnpj_estipulante        
      ,DATEADD(DAY, 1, sv.dt_fim_vigencia_sbg) AS dt_inicio_vigencia        
      ,NULL AS dt_fim_vigencia      
      ,pe.cpf AS cpf_segurado        
      ,sv.certificado_id        
      ,pf.proposta_bb      
      ,cvida.nome AS nome_segurado      
      ,cprop.nome AS nome_estipulante    
      ,2 AS tp_movimentacao_id    
  INTO #assistencia_exclusao    
  FROM #ultimas_movimentacoes u    
  JOIN proposta_tb p WITH (NOLOCK)      
    ON p.proposta_id = u.proposta_id      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = u.proposta_id    
  JOIN cliente_tb cprop WITH (NOLOCK)      
    ON cprop.cliente_id = p.prop_cliente_id      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.apolice_id = u.apolice_id    
   AND sv.ramo_id = u.ramo_id    
   AND sv.sub_grupo_id = u.sub_grupo_id    
   AND sv.tp_componente_id = u.tp_componente_id      
   AND sv.prop_cliente_id = u.prop_cliente_id    
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                                       FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                      WHERE sv2.apolice_id = sv.apolice_id      
                                        AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                        AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                        AND sv2.ramo_id = sv.ramo_id      
                                        AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                        AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                        AND sv2.proposta_id = sv.proposta_id      
                                        AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                        AND sv2.tp_componente_id = sv.tp_componente_id      
                                        AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                      ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
   AND sv.dt_fim_vigencia_sbg IS NOT NULL -- Vida Inativa    
   AND sv.dt_fim_vigencia_sbg < @dt_sistema    
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)    
    ON pe.pf_cliente_id = sv.prop_cliente_id    
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
 WHERE u.tp_movimentacao_id = 1    
   AND u.layout_id IS NOT NULL    
    
-- 2) Assist�ncias n�o mais vigentes    
INSERT INTO #assistencia_exclusao    
SELECT sv.apolice_id        
      ,sv.sucursal_seguradora_id        
      ,sv.seguradora_cod_susep            
      ,sv.ramo_id        
      ,sv.sub_grupo_id        
      ,sv.proposta_id         
      ,sv.prop_cliente_id        
      ,sv.tp_componente_id        
      ,sv.dt_inicio_vigencia_seg        
      ,sv.dt_inicio_vigencia_sbg        
      ,sv.seq_canc_endosso_seg        
      ,cprop.cpf_cnpj AS cpf_cnpj_estipulante        
      ,@dt_sistema AS dt_inicio_vigencia        
      ,NULL AS dt_fim_vigencia      
      ,pe.cpf AS cpf_segurado        
      ,sv.certificado_id        
      ,pf.proposta_bb      
      ,cvida.nome AS nome_segurado      
      ,cprop.nome AS nome_estipulante    
      ,2 AS tp_movimentacao_id    
  FROM #ultimas_movimentacoes u    
  JOIN proposta_tb p WITH (NOLOCK)      
    ON p.proposta_id = u.proposta_id      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = u.proposta_id    
  JOIN cliente_tb cprop WITH (NOLOCK)      
    ON cprop.cliente_id = p.prop_cliente_id      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.ramo_id = u.ramo_id      
   AND sv.apolice_id = u.apolice_id      
   AND sv.sub_grupo_id = u.sub_grupo_id      
   AND sv.prop_cliente_id = u.prop_cliente_id    
   AND sv.tp_componente_id = u.tp_componente_id      
   AND sv.dt_fim_vigencia_sbg IS NULL -- Vida Ativa      
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                                      FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                     WHERE sv2.apolice_id = sv.apolice_id      
                                       AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                       AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                       AND sv2.ramo_id = sv.ramo_id      
                                       AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                       AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                       AND sv2.proposta_id = sv.proposta_id      
                                       AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                       AND sv2.tp_componente_id = sv.tp_componente_id      
                                       AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                     ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)    
    ON pe.pf_cliente_id = sv.prop_cliente_id    
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
 WHERE u.tp_movimentacao_id = 1    
   AND u.layout_id IS NOT NULL    
   -- Inexist�ncia da assist�ncia escolar    
   AND NOT EXISTS (SELECT 1    
                     FROM assistencia_db..sub_grupo_assistencia_atual_tb sg WITH (NOLOCK)      
                    WHERE sg.ramo_id = u.ramo_id      
                      AND sg.apolice_id = u.apolice_id      
                      AND sg.sub_grupo_id = u.sub_grupo_id      
                      AND sg.plano_assistencia_id = 2904)    
    
-- 3) Subgrupos n�o mais vigentes    
INSERT INTO #assistencia_exclusao    
SELECT sv.apolice_id        
      ,sv.sucursal_seguradora_id        
      ,sv.seguradora_cod_susep            
      ,sv.ramo_id        
      ,sv.sub_grupo_id        
      ,sv.proposta_id         
      ,sv.prop_cliente_id        
      ,sv.tp_componente_id        
      ,sv.dt_inicio_vigencia_seg        
      ,sv.dt_inicio_vigencia_sbg        
      ,sv.seq_canc_endosso_seg        
      ,cprop.cpf_cnpj AS cpf_cnpj_estipulante        
      ,DATEADD(DAY, 1, sa.dt_fim_vigencia_sbg) AS dt_inicio_vigencia        
      ,NULL AS dt_fim_vigencia      
      ,pe.cpf AS cpf_segurado        
      ,sv.certificado_id        
      ,pf.proposta_bb      
      ,cvida.nome AS nome_segurado      
      ,cprop.nome AS nome_estipulante    
      ,2 AS tp_movimentacao_id    
  FROM #ultimas_movimentacoes u    
  JOIN proposta_tb p WITH (NOLOCK)      
    ON p.proposta_id = u.proposta_id      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = u.proposta_id    
  JOIN cliente_tb cprop WITH (NOLOCK)      
    ON cprop.cliente_id = p.prop_cliente_id      
  JOIN sub_grupo_apolice_tb sa WITH (NOLOCK)      
    ON sa.ramo_id = u.ramo_id      
   AND sa.apolice_id = u.apolice_id      
   AND sa.sub_grupo_id = u.sub_grupo_id      
   AND sa.dt_fim_vigencia_sbg IS NOT NULL    
   AND sa.dt_fim_vigencia_sbg < @dt_sistema -- Subgrupos Inativos      
  JOIN assistencia_db..sub_grupo_assistencia_atual_tb sg WITH (NOLOCK)      
    ON sg.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sg.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sg.ramo_id = sa.ramo_id      
   AND sg.apolice_id = sa.apolice_id      
   AND sg.sub_grupo_id = sa.sub_grupo_id      
   AND sg.plano_assistencia_id = 2904 -- Assistencia Escolar      
   AND sg.dt_ini_assist_sbg IS NOT NULL      
   AND sg.dt_ini_assist_sbg <= @dt_sistema -- Vigente      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.ramo_id = u.ramo_id      
   AND sv.apolice_id = u.apolice_id      
   AND sv.sub_grupo_id = u.sub_grupo_id      
   AND sv.prop_cliente_id = u.prop_cliente_id    
   AND sv.tp_componente_id = u.tp_componente_id      
   AND sv.dt_fim_vigencia_sbg IS NULL -- Vida Ativa      
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                                      FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                     WHERE sv2.apolice_id = sv.apolice_id      
                                       AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                       AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                       AND sv2.ramo_id = sv.ramo_id      
                                       AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                       AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                       AND sv2.proposta_id = sv.proposta_id      
                                       AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                       AND sv2.tp_componente_id = sv.tp_componente_id      
                                       AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                     ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)    
    ON pe.pf_cliente_id = sv.prop_cliente_id    
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
 WHERE u.tp_movimentacao_id = 1    
   AND u.layout_id IS NOT NULL    
    
-- 4) Ap�lices n�o mais vigentes    
INSERT INTO #assistencia_exclusao    
SELECT sv.apolice_id        
      ,sv.sucursal_seguradora_id        
      ,sv.seguradora_cod_susep            
      ,sv.ramo_id        
      ,sv.sub_grupo_id        
      ,sv.proposta_id         
      ,sv.prop_cliente_id        
      ,sv.tp_componente_id        
      ,sv.dt_inicio_vigencia_seg        
      ,sv.dt_inicio_vigencia_sbg        
      ,sv.seq_canc_endosso_seg        
      ,cprop.cpf_cnpj AS cpf_cnpj_estipulante        
      ,DATEADD(DAY, 1, a.dt_fim_vigencia) AS dt_inicio_vigencia        
      ,NULL AS dt_fim_vigencia          ,pe.cpf AS cpf_segurado        
      ,sv.certificado_id        
      ,pf.proposta_bb      
      ,cvida.nome AS nome_segurado      
      ,cprop.nome AS nome_estipulante    
      ,2 AS tp_movimentacao_id    
  FROM #ultimas_movimentacoes u    
  JOIN proposta_tb p WITH (NOLOCK)      
    ON p.proposta_id = u.proposta_id      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = u.proposta_id    
  JOIN cliente_tb cprop WITH (NOLOCK)    
    ON cprop.cliente_id = p.prop_cliente_id    
  JOIN apolice_tb a WITH (NOLOCK)    
    ON a.proposta_id = u.proposta_id    
   AND a.dt_fim_vigencia IS NOT NULL    
   AND a.dt_fim_vigencia < @dt_sistema -- Apolice finalizada    
  JOIN sub_grupo_apolice_tb sa WITH (NOLOCK)      
    ON sa.ramo_id = u.ramo_id      
   AND sa.apolice_id = u.apolice_id    
   AND sa.sub_grupo_id = u.sub_grupo_id    
   AND sa.dt_fim_vigencia_sbg IS NULL -- Subgrupos Ativos      
  JOIN assistencia_db..sub_grupo_assistencia_atual_tb sg WITH (NOLOCK)      
    ON sg.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sg.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sg.ramo_id = sa.ramo_id      
   AND sg.apolice_id = sa.apolice_id      
   AND sg.sub_grupo_id = sa.sub_grupo_id      
   AND sg.plano_assistencia_id = 2904 -- Assistencia Escolar      
   AND sg.dt_ini_assist_sbg IS NOT NULL      
   AND sg.dt_ini_assist_sbg <= @dt_sistema -- Vigente      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sv.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sv.ramo_id = sa.ramo_id      
   AND sv.apolice_id = sa.apolice_id      
   AND sv.sub_grupo_id = sa.sub_grupo_id      
   AND sv.prop_cliente_id = u.prop_cliente_id    
   AND sv.tp_componente_id = u.tp_componente_id      
   AND sv.dt_fim_vigencia_sbg IS NULL -- Vida Ativa      
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                                      FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                     WHERE sv2.apolice_id = sv.apolice_id      
                                       AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                       AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                       AND sv2.ramo_id = sv.ramo_id      
                                       AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                       AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                       AND sv2.proposta_id = sv.proposta_id      
                                       AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                       AND sv2.tp_componente_id = sv.tp_componente_id      
                                       AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                     ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)    
    ON pe.pf_cliente_id = sv.prop_cliente_id    
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
 WHERE u.tp_movimentacao_id = 1    
   AND u.layout_id IS NOT NULL    
    
-- 5) Propostas canceladas    
INSERT INTO #assistencia_exclusao    
SELECT sv.apolice_id        
      ,sv.sucursal_seguradora_id        
      ,sv.seguradora_cod_susep            
      ,sv.ramo_id        
      ,sv.sub_grupo_id        
      ,sv.proposta_id         
      ,sv.prop_cliente_id        
      ,sv.tp_componente_id        
      ,sv.dt_inicio_vigencia_seg        
      ,sv.dt_inicio_vigencia_sbg        
      ,sv.seq_canc_endosso_seg        
      ,cprop.cpf_cnpj AS cpf_cnpj_estipulante        
      ,c.dt_inicio_cancelamento AS dt_inicio_vigencia        
      ,NULL AS dt_fim_vigencia      
      ,pe.cpf AS cpf_segurado        
      ,sv.certificado_id        
      ,pf.proposta_bb      
      ,cvida.nome AS nome_segurado      
      ,cprop.nome AS nome_estipulante    
      ,2 AS tp_movimentacao_id    
  FROM #ultimas_movimentacoes u    
  JOIN proposta_tb p WITH (NOLOCK)      
    ON p.proposta_id = u.proposta_id      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = u.proposta_id    
  JOIN cliente_tb cprop WITH (NOLOCK)    
    ON cprop.cliente_id = p.prop_cliente_id    
  JOIN apolice_tb a WITH (NOLOCK)      
    ON a.proposta_id = u.proposta_id      
   AND (a.dt_fim_vigencia IS NULL    
    OR (a.dt_fim_vigencia IS NOT NULL      
   AND a.dt_fim_vigencia >= @dt_sistema)) -- Apolice Vigente      
  JOIN sub_grupo_apolice_tb sa WITH (NOLOCK)      
    ON sa.ramo_id = u.ramo_id      
   AND sa.apolice_id = u.apolice_id    
   AND sa.sub_grupo_id = u.sub_grupo_id    
   AND sa.dt_fim_vigencia_sbg IS NULL -- Subgrupos Ativos      
  JOIN assistencia_db..sub_grupo_assistencia_atual_tb sg WITH (NOLOCK)      
    ON sg.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sg.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sg.ramo_id = sa.ramo_id      
   AND sg.apolice_id = sa.apolice_id      
   AND sg.sub_grupo_id = sa.sub_grupo_id    
   AND sg.plano_assistencia_id = 2904 -- Assistencia Escolar      
   AND sg.dt_ini_assist_sbg IS NOT NULL      
   AND sg.dt_ini_assist_sbg <= @dt_sistema -- Vigente      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sv.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sv.ramo_id = sa.ramo_id      
   AND sv.apolice_id = sa.apolice_id      
   AND sv.sub_grupo_id = sa.sub_grupo_id    
   AND sv.prop_cliente_id = u.prop_cliente_id    
   AND sv.tp_componente_id = u.tp_componente_id      
   AND sv.dt_fim_vigencia_sbg IS NULL -- Vida Ativa      
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                                      FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                     WHERE sv2.apolice_id = sv.apolice_id      
                                       AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                       AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                       AND sv2.ramo_id = sv.ramo_id      
                                       AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                       AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                       AND sv2.proposta_id = sv.proposta_id      
                                       AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                       AND sv2.tp_componente_id = sv.tp_componente_id      
       AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                     ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)    
    ON pe.pf_cliente_id = sv.prop_cliente_id    
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
  left JOIN cancelamento_proposta_tb c WITH (NOLOCK)      
    ON c.proposta_id = p.proposta_id    
   AND c.dt_fim_cancelamento IS NULL    
 WHERE u.tp_movimentacao_id = 1    
   AND u.layout_id IS NOT NULL    
   AND p.situacao = 'c'    
    
-- Juntando todas as movimenta��es    
SELECT *    
  INTO #assistencia_movimentacao    
  FROM #assistencia_inclusao    
UNION ALL    
SELECT *    
  FROM #assistencia_exclusao    
    
-- Somando todas as movimenta��es    
SELECT @TOTAL = COUNT(1)    
  FROM #assistencia_movimentacao    
    
-- Incluindo as movimenta��es na tabela final    
INSERT INTO assistencia_db.dbo.movimento_assistencia_vida_atual_tb      
           (apolice_id,      
            sucursal_seguradora_id,      
            seguradora_cod_susep,      
            ramo_id,      
            sub_grupo_id,      
            proposta_id,      
            prop_cliente_id,      
            tp_componente_id,      
            dt_inicio_vigencia_seg,      
            dt_inicio_vigencia_sbg,      
            seq_canc_endosso_seg,      
            tp_movimentacao_id,      
            cpf_segurado,      
            nome_segurado,      
            proposta_bb,      
      certificado_id,      
            cpf_cnpj_estipulante,      
            nome_estipulante,      
            plano_assistencia_id,      
            dt_inicio_vigencia,      
            dt_fim_vigencia,      
            enviar,      
            layout_id,      
            versao,      
            dt_inclusao,      
            usuario)      
     SELECT apolice_id,      
            sucursal_seguradora_id,      
            seguradora_cod_susep,      
            ramo_id,      
            sub_grupo_id,      
            proposta_id,      
            prop_cliente_id,      
            tp_componente_id,      
            dt_inicio_vigencia_seg,      
            dt_inicio_vigencia_sbg,      
            seq_canc_endosso_seg,      
            tp_movimentacao_id,      
            cpf_segurado,      
            nome_segurado,      
            proposta_bb,      
            certificado_id,      
            cpf_cnpj_estipulante,      
            nome_estipulante,      
            2904,      
            dt_inicio_vigencia,      
            dt_fim_vigencia,      
            'S',      
            NULL,      
            NULL,      
            GETDATE(),      
            @usuario      
       FROM #assistencia_movimentacao      
    
-- inicio altera��es assistencias produto 1206 - 18720075  
  
-- verifica��o das propostas j� enviadas  
SELECT RANK() OVER (PARTITION BY m.proposta_id, m.cliente_id, p.plano_assistencia_id      
                    ORDER BY m.dt_inclusao DESC) AS ordem,      
    m.cliente_id      
    ,m.proposta_id         
    ,1 as  tipo_assistencia_id  
    ,m.tp_movimento_id  
    ,m.ult_retorno_assistencia_id  
    ,m.ult_movimento_assistencia_id  
    ,m.dt_envio  
    ,m.dt_retorno  
    ,m.dt_registro_movimento  
       ,m.ult_envio_assistencia_id  
       ,m.nr_pontos  
    ,m.dt_inclusao  
    ,m.dt_alteracao  
    ,m.usuario  
    ,m.produto_id   
    ,p.plano_assistencia_id    
 INTO #movimentacoes_anteriores_1206      
  FROM assistencia_db.dbo.movimento_assistencia_atual_Tb m with (nolock)  
  join assistencia_db.dbo.proposta_assistencia_atual_tb p  
  on p.proposta_id = m.proposta_id     
 WHERE p.plano_assistencia_id in  (3479, 3481)    
 and m.produto_id =  1206    
  
  
  
-- Selecionando as �ltimas movimenta��es de cada proposta      
SELECT *      
  INTO #ultimas_movimentacoes_1206  
  FROM #movimentacoes_anteriores_1206      
 WHERE ordem = 1      
  
  
select   
    c.cliente_id      
    ,paa.proposta_id         
    ,pa.tipo_assistencia_id  
    ,1 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,NULL as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,null as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id   
    , paa.plano_assistencia_id    
into #temp_movimento_assistencia_inclusao_1206    
from assistencia_db..proposta_assistencia_atual_tb paa with (nolock)   
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_juridica_tb pe with (nolock)  
   on pe.pj_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
     AND (a.dt_fim_vigencia IS NULL      
   OR (a.dt_fim_vigencia IS NOT NULL   
     AND a.dt_fim_vigencia >= @dt_sistema))           
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 join assistencia_db..plano_assistencia_tb pa with (nolock)  
  on pa.plano_assistencia_id = paa.plano_assistencia_id  
 JOIN seguros_db..PROPOSTA_BASICA_TB WITH (NOLOCK)  
  ON PROPOSTA_BASICA_TB.PROPOSTA_ID = PAA.PROPOSTA_ID  
 where  p.produto_id =  1206   
  and paa.plano_assistencia_id in  (3479, 3481)   
  and a.dt_fim_vigencia >= @dt_sistema  
  and p.situacao = 'i'  
  
  
DELETE a      
  FROM #temp_movimento_Assistencia_inclusao_1206 a      
 WHERE EXISTS (SELECT 1      
                 FROM #ultimas_movimentacoes_1206 u      
                WHERE u.proposta_id = a.proposta_id      
                  AND u.cliente_id = a.cliente_id      
                   AND u.tp_movimento_id = 1)   
  
  
   -- verifica��o de cancelamento de proposta  
select   
    c.cliente_id      
    ,paa.proposta_id         
    ,pa.tipo_assistencia_id  
    ,2 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,NULL as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,null as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id  
into #temp_movimento_assistencia_exclusao_1206   
from #ultimas_movimentacoes_1206 u  
 join assistencia_db..proposta_assistencia_atual_tb paa with (nolock)  --> ASSISTENCIA_DB  
   on u.proposta_id = paa.proposta_id   
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_fisica_tb pe with (nolock)  
   on pe.pf_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 join assistencia_db..plano_assistencia_tb pa with (nolock)  
  on pa.plano_assistencia_id = paa.plano_assistencia_id  
 join seguros_db..cancelamento_proposta_tb pc with (nolock)  
  on pc.proposta_id = p.proposta_id  
 where   p.produto_id =   1206   
  and paa.plano_assistencia_id in  (3479, 3481)    
  
  
-- verifica�ao de termino de vigencia para exclus�o da proposta se @data_sistema > dt_fim_vigencia (apolice_tb)  
insert into #temp_movimento_assistencia_exclusao_1206  
(      cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id  )  
select   
    c.cliente_id      
    ,paa.proposta_id         
    ,pa.tipo_assistencia_id  
    ,2 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,NULL as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,null as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id   
from #ultimas_movimentacoes_1206 u  
 join assistencia_db..proposta_assistencia_atual_tb paa with (nolock)  
      on u.proposta_id = paa.proposta_id  
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_juridica_tb pe with (nolock)  
   on pe.pj_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 join assistencia_db..plano_assistencia_tb pa with (nolock)  
  on pa.plano_assistencia_id = paa.plano_assistencia_id  
 where a.dt_fim_vigencia < @dt_sistema  
  and p.produto_id =  1206   
  and paa.plano_assistencia_id in  (3479, 3481)     
  
  
  
   -- verifica��o de altera��o na proposta endosso 51  
select   
    c.cliente_id      
    ,paa.proposta_id         
    ,pa.tipo_assistencia_id  
    ,3 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,getdate() as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,NULL as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id   
into #temp_movimento_assistencia_alteracao_1206  
from #ultimas_movimentacoes_1206 u  
 join assistencia_db..proposta_assistencia_atual_tb paa with (nolock)    
   on paa.proposta_id = u.proposta_id  
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_juridica_tb pe with (nolock)  
   on pe.pj_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 join assistencia_db..plano_assistencia_tb pa with (nolock)  
  on pa.plano_assistencia_id = paa.plano_assistencia_id  
 join seguros_db..endosso_Tb e with (nolock)  
  on e.proposta_id = p.proposta_id  
 where  e.endosso_id = 51   
  and p.produto_id = 1206    
  and paa.plano_assistencia_id in (3479, 3481)  
   
  
  
-- Juntando todas as movimenta��es      
SELECT   
     cliente_id  
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id      
  INTO #assistencia_movimentacao_1206  
  FROM #temp_movimento_assistencia_inclusao_1206     
UNION ALL      
SELECT   
      cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id       
  FROM #temp_movimento_assistencia_exclusao_1206  
UNION ALL   
SELECT   
      cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id       
  FROM #temp_movimento_assistencia_alteracao_1206  
   
  
-- incluindo na tabela de movimenta��es  
insert into assistencia_db..movimento_assistencia_atual_tb    
 (  
     cliente_id      
    ,proposta_id         
    ,tp_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id    
)  
select  cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id  
 from #temp_movimento_assistencia_inclusao_1206  
    
 -- update das propostas a serem alteradas  
 update assistencia_db..movimento_assistencia_atual_tb  
  set dt_alteracao = getdate(),  
   tp_movimento_id = 3  
  where proposta_id in (select proposta_id from #temp_movimento_assistencia_alteracao_1206 )  
    
 update assistencia_db..movimento_assistencia_atual_tb  
  set dt_alteracao = getdate(),  
   tp_movimento_id = 2  
  where proposta_id in (select proposta_id from #temp_movimento_assistencia_exclusao_1206)  
  
  
 -- fim altera��o assistencias produto 1206  
  
  
 SELECT @TOTAL_1206 = COUNT(1)    
  FROM #assistencia_movimentacao_1206    
  
  set @TOTAL = @TOTAL + @TOTAL_1206  
      
SELECT 'Movimenta��es', @TOTAL  
--SELECT 'Movimenta��es de assist�ncia escolar e do produto BB seguro empresa Flex', (@TOTAL + @TOTAL_1206)  
    
RETURN    
    
  
  
  