CREATE PROCEDURE SEGS7073_SPI  
  
 @plano_assistencia_id AS INTEGER,  
 @dt_inicio_vigencia AS SMALLDATETIME,  
 @dt_fim_vigencia AS SMALLDATETIME,  
 @val_custo_assistencia AS NUMERIC(19,2),  
 @usuario AS UD_usuario,  
 @tipo_assistencia_id AS INTEGER  
  
AS  
  
 INSERT INTO plano_assistencia_tb(  
   plano_assistencia_id,  
   dt_inicio_vigencia,  
   dt_fim_vigencia,  
   val_custo_assistencia,  
   usuario,  
   tipo_assistencia_id,  
   dt_inclusao)  
  VALUES(  
   @plano_assistencia_id,  
   @dt_inicio_vigencia,  
   @dt_fim_vigencia,  
   @val_custo_assistencia,  
   @usuario,  
   @tipo_assistencia_id,  
   GETDATE())   
   
select * from tp_endosso_tb

select * from tipo_assistencia_tb where tipo_assistencia_id >= 74 order by tipo_assistencia_id
select * from plano_assistencia_tb where tipo_assistencia_id in (74, 75, 76, 77, 78, 79, 80)order by plano_assistencia_id
select * from assistencia_servico_tb where plano_assistencia_id in (3482,3484,3487,3489,3485,3488,3483,3486,3487) order by plano_assistencia_id, servico_id
select * from tp_abrangencia_tb
select * from servico_tb where servico_id in (134)
  
--74	PROD_1235 - PLANO 1
--75	PROD_1235 - PLANO 2
--76	PROD_1235 - PLANO 3
--77	PROD_1236 - PLANO 1
--78	PROD_1237 - PLANO 1
--79	PROD_1237 - PLANO 2
--80	PROD_1237 - PLANO 3

select plano_assistencia_id, tipo_assistencia_id
  --into #plano_assistencia
  from assistencia_db..plano_assistencia_tb with (nolock)  
 where tipo_assistencia_id in (74, 75, 76, 77, 78, 79, 80)
   and (dt_fim_vigencia is null or dt_fim_vigencia >= GETDATE())


  