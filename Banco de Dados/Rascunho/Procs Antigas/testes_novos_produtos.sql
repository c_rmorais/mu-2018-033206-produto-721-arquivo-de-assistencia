-- inicio alterações assistencias produto 1235, 1236, 1237
  
-- verificação das propostas já enviadas  

declare @dt_sistema smalldatetime
select @dt_sistema = dt_operacional from seguros_db..parametro_geral_tb

select plano_assistencia_id, tipo_assistencia_id
  into #plano_assistencia
  from assistencia_db..plano_assistencia_tb with (nolock)  
 where tipo_assistencia_id in (74, 75, 76, 77, 78, 79, 80)
   and (dt_fim_vigencia is null or dt_fim_vigencia > @dt_sistema)
 order by plano_assistencia_id

SELECT RANK() OVER (PARTITION BY m.proposta_id, m.cliente_id, p.plano_assistencia_id      
                    ORDER BY m.dt_inclusao DESC) AS ordem,      
    m.cliente_id      
    ,m.proposta_id         
    ,1 as  tipo_assistencia_id  
    ,m.tp_movimento_id  
    ,m.ult_retorno_assistencia_id  
    ,m.ult_movimento_assistencia_id  
    ,m.dt_envio  
    ,m.dt_retorno  
    ,m.dt_registro_movimento  
       ,m.ult_envio_assistencia_id  
       ,m.nr_pontos  
    ,m.dt_inclusao  
    ,m.dt_alteracao  
    ,m.usuario  
    ,m.produto_id   
    ,p.plano_assistencia_id    
  INTO #movimentacoes_anteriores_novos_produtos      
  FROM assistencia_db.dbo.movimento_assistencia_atual_Tb m with (nolock)  
  join assistencia_db.dbo.proposta_assistencia_atual_tb p with (nolock)    
  on p.proposta_id = m.proposta_id
  join #plano_assistencia
    on #plano_assistencia.plano_assistencia_id = p.plano_assistencia_id
 WHERE m.produto_id in (1235, 1236, 1237)    
 
 -- Selecionando as últimas movimentações de cada proposta      
SELECT *      
  INTO #ultimas_movimentacoes_novos_produtos  
  FROM #movimentacoes_anteriores_novos_produtos      
 WHERE ordem = 1      
    
select c.cliente_id      
    ,paa.proposta_id         
    ,pa.tipo_assistencia_id  
    ,1 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,NULL as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,null as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id   
    , paa.plano_assistencia_id    
into #temp_movimento_assistencia_inclusao_novos_produtos    
from assistencia_db..proposta_assistencia_atual_tb paa with (nolock)   
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_juridica_tb pe with (nolock)  
   on pe.pj_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
     AND (a.dt_fim_vigencia IS NULL      
   OR (a.dt_fim_vigencia IS NOT NULL   
     AND a.dt_fim_vigencia >= @dt_sistema))           
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 join assistencia_db..plano_assistencia_tb pa with (nolock)  
  on pa.plano_assistencia_id = paa.plano_assistencia_id  
 JOIN seguros_db..PROPOSTA_BASICA_TB WITH (NOLOCK)  
  ON PROPOSTA_BASICA_TB.PROPOSTA_ID = PAA.PROPOSTA_ID  
 JOIN #plano_assistencia
  ON #plano_assistencia.plano_assistencia_id = paa.plano_assistencia_id
 where  p.produto_id in (1235, 1236, 1237)   
  and a.dt_fim_vigencia >= @dt_sistema  
  and p.situacao = 'i'  
  
  DELETE a      
  FROM #temp_movimento_assistencia_inclusao_novos_produtos a      
 WHERE EXISTS (SELECT 1      
                 FROM #ultimas_movimentacoes_novos_produtos u      
                WHERE u.proposta_id = a.proposta_id      
                  AND u.cliente_id = a.cliente_id      
                   AND u.tp_movimento_id = 1)   
  
   -- verificação de cancelamento de proposta  
select   
    c.cliente_id      
    ,paa.proposta_id         
    ,pa.tipo_assistencia_id  
    ,2 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,NULL as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,null as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id  
into #temp_movimento_assistencia_exclusao_novos_produtos   
from #ultimas_movimentacoes_novos_produtos u  
 join assistencia_db..proposta_assistencia_atual_tb paa with (nolock)  --> ASSISTENCIA_DB  
   on u.proposta_id = paa.proposta_id   
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_fisica_tb pe with (nolock)  
   on pe.pf_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 join assistencia_db..plano_assistencia_tb pa with (nolock)  
  on pa.plano_assistencia_id = paa.plano_assistencia_id  
 join seguros_db..cancelamento_proposta_tb pc with (nolock)  
  on pc.proposta_id = p.proposta_id 
 JOIN #plano_assistencia
  ON #plano_assistencia.plano_assistencia_id = paa.plano_assistencia_id 
 where   p.produto_id in (1235, 1236, 1237)  
  
-- verificaçao de termino de vigencia para exclusão da proposta se @data_sistema > dt_fim_vigencia (apolice_tb)  
insert into #temp_movimento_assistencia_exclusao_novos_produtos  
(      cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id  )  
select   
    c.cliente_id      
    ,paa.proposta_id         
    ,#plano_assistencia.tipo_assistencia_id  
    ,2 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,NULL as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,null as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id   
from #ultimas_movimentacoes_novos_produtos u  
 join assistencia_db..proposta_assistencia_atual_tb paa with (nolock)  
      on u.proposta_id = paa.proposta_id  
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_juridica_tb pe with (nolock)  
   on pe.pj_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 JOIN #plano_assistencia
  ON #plano_assistencia.plano_assistencia_id = paa.plano_assistencia_id 
 where a.dt_fim_vigencia < @dt_sistema  
  and p.produto_id in (1235, 1236, 1237)   
  
   -- verificação de alteração na proposta endosso 51  
select   
    c.cliente_id      
    ,paa.proposta_id         
    ,pa.tipo_assistencia_id  
    ,3 as tp_movimento_id  
    ,NULL as ult_retorno_assistencia_id  
    ,NULL as ult_movimento_assistencia_id  
    ,getdate() as dt_envio  
    ,NULL as dt_retorno  
    ,NULL  as dt_registro_movimento  
       ,NULL  as ult_envio_assistencia_id  
       ,NULL as nr_pontos  
    ,getdate() as dt_inclusao  
    ,NULL as dt_alteracao  
    ,@usuario as usuario  
    ,p.produto_id   
into #temp_movimento_assistencia_alteracao_novos_produtos  
from #ultimas_movimentacoes_novos_produtos u  
 join assistencia_db..proposta_assistencia_atual_tb paa with (nolock)    
   on paa.proposta_id = u.proposta_id  
 join seguros_db..proposta_tb p  with (nolock)  
   on paa.proposta_id = p.proposta_id  
 join seguros_db..proposta_processo_susep_tb pps  with (nolock)  
   on pps.proposta_id = paa.proposta_id  
    join seguros_db..cliente_tb c  with (nolock)  
   on c.cliente_id = p.prop_cliente_id  
 join seguros_db..pessoa_juridica_tb pe with (nolock)  
   on pe.pj_cliente_id = c.cliente_id  
 join seguros_db..apolice_tb a with (nolock)  
   on a.proposta_id = p.proposta_id  
 join seguros_db..proposta_fechada_tb pf with (nolock)  
   on pf.proposta_id = p.proposta_id  
 join assistencia_db..plano_assistencia_tb pa with (nolock)  
  on pa.plano_assistencia_id = paa.plano_assistencia_id  
 join seguros_db..endosso_Tb e with (nolock)  
  on e.proposta_id = p.proposta_id
 JOIN #plano_assistencia
  ON #plano_assistencia.plano_assistencia_id = paa.plano_assistencia_id     
 where  e.endosso_id = 51   
  and p.produto_id in (1235, 1236, 1237)      
  
-- Juntando todas as movimentações      
SELECT   
     cliente_id  
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id      
  INTO #assistencia_movimentacao_novos_produtos  
  FROM #temp_movimento_assistencia_inclusao_novos_produtos    
UNION ALL      
SELECT   
      cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id       
  FROM #temp_movimento_assistencia_exclusao_novos_produtos 
UNION ALL   
SELECT   
      cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id       
  FROM #temp_movimento_assistencia_alteracao_novos_produtos  
   
  
-- incluindo na tabela de movimentações  
insert into assistencia_db..movimento_assistencia_atual_tb    
 (  
     cliente_id      
    ,proposta_id         
    ,tp_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id    
)  
select  cliente_id      
    ,proposta_id         
    ,tipo_assistencia_id  
    ,tp_movimento_id  
    ,ult_retorno_assistencia_id  
    ,ult_movimento_assistencia_id  
    ,dt_envio  
    ,dt_retorno  
    ,dt_registro_movimento  
       ,ult_envio_assistencia_id  
       ,nr_pontos  
    ,dt_inclusao  
    ,dt_alteracao  
    ,usuario  
    ,produto_id  
 from #temp_movimento_assistencia_inclusao_novos_produtos 
    
 -- update das propostas a serem alteradas  
 update assistencia_db..movimento_assistencia_atual_tb  
  set dt_alteracao = getdate(),  
   tp_movimento_id = 3  
  where proposta_id in (select proposta_id from #temp_movimento_assistencia_alteracao_novos_produtos )  
    
 update assistencia_db..movimento_assistencia_atual_tb  
  set dt_alteracao = getdate(),  
   tp_movimento_id = 2  
  where proposta_id in (select proposta_id from #temp_movimento_assistencia_exclusao_novos_produtos)  
  
  
 -- fim alteração assistencias produto 1206  