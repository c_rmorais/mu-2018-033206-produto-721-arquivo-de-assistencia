CREATE PROCEDURE dbo.SEGS11715_SPI  
            
    @layout_id INT,                  
    @produto_id INT,                  
    @usuario VARCHAR(20),                  
    @producao CHAR(1) = 'N'                  
                  
AS                  
                  
--------------------------------------------------------                  
-- Procedure principal de gera��o do arquivo SEGA9169 --                  
--------------------------------------------------------                  
                  
SET NOCOUNT ON                  

BEGIN TRY                  
	DECLARE @dt_sistema SMALLDATETIME                  
	                  
	SELECT @dt_sistema = dt_operacional                  
	  FROM parametro_geral_tb (NOLOCK)                  
	                  
	INSERT INTO interface_dados_db..SEGA9169_processar_tb                  
		(layout_id  ,            
	 proposta_bb ,             
	 proposta_id ,            
	 prop_cliente_id ,             
	 certificado_id ,             
	 apolice_id ,            
	 sub_grupo_id ,             
	 produto_id ,             
	 ramo_id ,            
	 plano_assistencia_id ,             
	 tp_movimento_id ,             
	 usuario ,             
	 dt_inclusao ,             
	 dt_alteracao,
	 num_solicitacao)            
	 select             
	  @layout_id , --layout_id            
	  assistencia_db..movimento_assistencia_vida_atual_tb.proposta_bb ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.proposta_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.prop_cliente_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.certificado_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.apolice_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.sub_grupo_id ,            
	  seguros_db..proposta_tb.produto_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.ramo_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.plano_assistencia_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.tp_movimentacao_id ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.usuario ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.dt_inclusao ,            
	  assistencia_db..movimento_assistencia_vida_atual_tb.dt_alteracao,
	  0	            
	 from assistencia_db..movimento_assistencia_vida_atual_tb with (nolock)            
	 inner join seguros_db..proposta_tb with (nolock) on assistencia_db..movimento_assistencia_vida_atual_tb.proposta_id = seguros_db..proposta_tb.proposta_id            
	 inner join seguros_db..produto_tb with (nolock) on seguros_db..proposta_tb.produto_id = seguros_db..produto_tb.produto_id            
	 where assistencia_db..movimento_assistencia_vida_atual_tb.plano_assistencia_id = 2904 --Assistencia Escolar          
	 --and seguros_db..proposta_tb.produto_id in (115,123,150)       
	 --and assistencia_db..movimento_assistencia_vida_atual_tb.layout_id = @layout_id            
	 and assistencia_db..movimento_assistencia_vida_atual_tb.enviar = 'S'            
	and assistencia_db..movimento_assistencia_vida_atual_tb.layout_id is null
	and assistencia_db..movimento_assistencia_vida_atual_tb.versao is null                    
	        
			
	INSERT INTO interface_dados_db..SEGA9169_processar_tb 
	(layout_id  ,            
	 proposta_bb ,             
	 proposta_id ,            
	 prop_cliente_id ,             
	 certificado_id ,             
	 apolice_id ,            
	 sub_grupo_id ,             
	 produto_id ,             
	 ramo_id ,            
	 plano_assistencia_id ,             
	 tp_movimento_id ,             
	 usuario ,             
	 dt_inclusao ,             
	 dt_alteracao,
	 num_solicitacao)  
		select 
			@layout_id,
			pf.proposta_bb,
			maa.proposta_id,
			maa.cliente_id,
			NULL as certificado_id,
			a.apolice_id,
			0 as sub_grupo_id,
			maa.produto_id,
			a.ramo_id,
			paa.plano_assistencia_id,
			maa.tp_movimento_id,
			maa.usuario,
			maa.dt_inclusao,
			maa.dt_alteracao,
			0 as num_solicitacao
		from assistencia_db..movimento_Assistencia_atual_tb maa with (nolock)
			join assistencia_db..proposta_assistencia_atual_Tb paa with (nolock)
				on paa.proposta_id = maa.proposta_id
			join seguros_db..proposta_tb p with (nolock)
				on maa.proposta_id = p.proposta_id
			join seguros_db..proposta_fechada_tb pf with (nolock)
				on maa.proposta_id = pf.proposta_id
			join seguros_db..apolice_tb a with (nolock)
				on a.proposta_id = maa.proposta_id
		where maa.produto_id = 1206 
			and paa.plano_assistencia_id in (3479, 3481) 
			and maa.dt_envio is null 
		union
			select 
				@layout_id,
				pf.proposta_bb,
				maa.proposta_id,
				maa.cliente_id,
				NULL as certificado_id,
				a.apolice_id,
				0 as sub_grupo_id,
				maa.produto_id,
				a.ramo_id,
				paa.plano_assistencia_id,
				maa.tp_movimento_id,
				maa.usuario,
				maa.dt_inclusao,
				maa.dt_alteracao,
				0 as num_solicitacao
			from assistencia_db..movimento_Assistencia_atual_tb maa with (nolock)
				join assistencia_db..proposta_assistencia_atual_Tb paa with (nolock)
					on paa.proposta_id = maa.proposta_id
				join seguros_db..proposta_tb p with (nolock)
					on maa.proposta_id = p.proposta_id
				join seguros_db..proposta_fechada_tb pf with (nolock)
					on maa.proposta_id = pf.proposta_id
				join seguros_db..apolice_tb a with (nolock)
					on a.proposta_id = maa.proposta_id
			where maa.produto_id = 1206 
				and paa.plano_assistencia_id in (3479, 3481) 
				and maa.dt_envio is not null
				and maa.tp_movimento_id in (2, 3)
				--and maa.dt_alteracao <> maa.dt_inclusao
				
				
				
	SET NOCOUNT OFF                    
	RETURN
END TRY                    
BEGIN CATCH

  DECLARE @ErrorMessage NVARCHAR(4000)
  DECLARE @ErrorSeverity INT
  DECLARE @ErrorState INT

  SELECT 
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE()

      -- Use RAISERROR inside the CATCH block to return error
      -- information about the original error that caused
      -- execution to jump to the CATCH block.
      RAISERROR (@ErrorMessage, -- Message text.
                 @ErrorSeverity, -- Severity.
                 @ErrorState -- State.
                 )

END CATCH
        



