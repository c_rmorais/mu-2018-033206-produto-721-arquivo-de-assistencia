CREATE PROCEDURE dbo.SEGS11742_SPI  
              
    @layout_id INT,                    
    @produto_id INT,                    
    @usuario VARCHAR(20),                    
    @producao CHAR(1) = 'N'                    
                    
AS 

--------------------------------------------------------                    
-- PARAMETROS PARA TESTE
-- DECLARE  
--  @layout_id INT,                    
--  @produto_id INT,                    
--  @usuario VARCHAR(20),                    
--  @producao CHAR(1)
--
-- SET @layout_id = 1692                    
-- SET @produto_id = NULL                   
-- SET @usuario = 'teste'
-- SET @producao = 'N'                
--------------------------------------------------------                    


--------------------------------------------------------                    
-- Procedure principal de geraÃ§Ã£o do arquivo sega9196 --                    
--------------------------------------------------------                    
                    
SET NOCOUNT ON                    
                    
DECLARE @dt_sistema SMALLDATETIME                    
DECLARE @msg VARCHAR(100)                     
                    
 SELECT @dt_sistema = dt_operacional                    
   FROM parametro_geral_tb (NOLOCK)                    
    
    
BEGIN TRY      
	    
		 INSERT INTO interface_dados_db..sega9196_processar_tb                    
	   (layout_id  ,              
	   proposta_bb ,               
	   proposta_id ,              
	   prop_cliente_id ,               
	   certificado_id ,               
	   apolice_id ,              
	   sub_grupo_id ,               
	   produto_id ,             
	   ramo_id ,              
	   plano_assistencia_id ,--               
	   tp_movimento_id ,  --             
	   usuario ,               
	   dt_inclusao ,               
	   dt_alteracao)              
SELECT
	   @layout_id --layout_id    
	   ,pf.proposta_bb       
	   ,sv.proposta_id 	
	   ,sv.prop_cliente_id 
	   ,sv.certificado_id       
	   ,sv.apolice_id    
	   ,sv.sub_grupo_id 
	   ,p.produto_id  
	   ,sv.ramo_id 
	   ,2904 --plano_assistencia_id 
	   ,1 AS tp_movimentacao_id  
	   ,@usuario
	   ,GETDATE()             
	   ,NULL
  FROM proposta_tb p WITH (NOLOCK)      
  JOIN proposta_fechada_tb pf WITH (NOLOCK)      
    ON pf.proposta_id = p.proposta_id      
  JOIN cliente_tb cprop WITH (NOLOCK)      
    ON cprop.cliente_id = p.prop_cliente_id      
  JOIN apolice_tb a WITH (NOLOCK)      
    ON a.proposta_id = p.proposta_id      
   AND (a.dt_fim_vigencia IS NULL    
    OR (a.dt_fim_vigencia IS NOT NULL      
   AND a.dt_fim_vigencia >= @dt_sistema)) -- Apolice Vigente      
  JOIN sub_grupo_apolice_tb sa WITH (NOLOCK)      
    ON sa.seguradora_cod_susep = a.seguradora_cod_susep      
   AND sa.sucursal_seguradora_id = a.sucursal_seguradora_id      
   AND sa.ramo_id = a.ramo_id      
   AND sa.apolice_id = a.apolice_id      
   AND sa.dt_fim_vigencia_sbg IS NULL -- Subgrupos Ativos      
  JOIN assistencia_db..sub_grupo_assistencia_atual_tb sg WITH (NOLOCK)      
    ON sg.seguradora_cod_susep = sa.seguradora_cod_susep      
   AND sg.sucursal_seguradora_id = sa.sucursal_seguradora_id      
   AND sg.ramo_id = sa.ramo_id      
   AND sg.apolice_id = sa.apolice_id      
   AND sg.sub_grupo_id = sa.sub_grupo_id      
   AND sg.plano_assistencia_id = 2904 -- Assistencia Escolar      
   AND sg.dt_ini_assist_sbg IS NOT NULL      
   AND sg.dt_ini_assist_sbg <= @dt_sistema -- Vigente      
  JOIN seguro_vida_sub_grupo_tb sv WITH (NOLOCK)      
    ON sv.seguradora_cod_susep = sa.seguradora_cod_susep        
   AND sv.sucursal_seguradora_id = sa.sucursal_seguradora_id        
   AND sv.ramo_id = sa.ramo_id      
   AND sv.apolice_id = sa.apolice_id      
   AND sv.sub_grupo_id = sa.sub_grupo_id      
   AND sv.dt_fim_vigencia_sbg IS NULL -- Vida Ativa      
   AND sv.dt_inicio_vigencia_sbg = (SELECT TOP 1 sv2.dt_inicio_vigencia_sbg      
                    FROM seguro_vida_sub_grupo_tb sv2 WITH (NOLOCK)      
                                     WHERE sv2.apolice_id = sv.apolice_id      
                                       AND sv2.sucursal_seguradora_id = sv.sucursal_seguradora_id      
                                       AND sv2.seguradora_cod_susep = sv.seguradora_cod_susep      
                                       AND sv2.ramo_id = sv.ramo_id      
                                       AND sv2.sub_grupo_id = sv.sub_grupo_id      
                                       AND sv2.dt_inicio_vigencia_apol_sbg = sv.dt_inicio_vigencia_apol_sbg      
                                       AND sv2.proposta_id = sv.proposta_id      
                                       AND sv2.prop_cliente_id = sv.prop_cliente_id      
                                       AND sv2.tp_componente_id = sv.tp_componente_id      
                                       AND sv2.seq_canc_endosso_seg = sv.seq_canc_endosso_seg      
                                     ORDER BY sv2.dt_inicio_vigencia_sbg DESC)      
  JOIN cliente_tb cvida WITH (NOLOCK)      
    ON cvida.cliente_id = sv.prop_cliente_id      
  JOIN pessoa_fisica_tb pe WITH (NOLOCK)      
    ON pe.pf_cliente_id = cvida.cliente_id   
  JOIN escolha_sub_grp_tp_cob_comp_tb es  
 ON es.apolice_id = sa.apolice_id    
   AND es.sucursal_seguradora_id = sa.sucursal_seguradora_id    
   AND es.seguradora_cod_susep = sa.seguradora_cod_susep    
   AND es.ramo_id = sa.ramo_id    
   AND es.sub_grupo_id = sa.sub_grupo_id   
  JOIN tp_cob_comp_tb cc WITH (NOLOCK)      
    ON cc.tp_cob_comp_id = es.tp_cob_comp_id    
   AND cc.tp_cobertura_id = 830 -- Cobertura DMHO       
 WHERE p.produto_id in (115,123,150)      
   AND p.ramo_id = 82      
   AND p.situacao = 'i'     
 
 
 
 -- alterações 18720075
INSERT INTO interface_dados_db..SEGA9196_processar_tb -- delete from desenv_db..sega9169_processar_tb
	(layout_id  ,            
	 proposta_bb ,             
	 proposta_id ,            
	 prop_cliente_id ,             
	 certificado_id ,             
	 apolice_id ,            
	 sub_grupo_id ,             
	 produto_id ,             
	 ramo_id ,            
	 plano_assistencia_id ,             
	 tp_movimento_id ,             
	 usuario ,             
	 dt_inclusao ,             
	 dt_alteracao,
	 num_solicitacao) 
	select 
			@layout_id,
			pf.proposta_bb,
			paa.proposta_id,
			p.prop_cliente_id as cliente_id,
			NULL as certificado_id,
			a.apolice_id,
			0 as sub_grupo_id,
			p.produto_id,
			a.ramo_id,
			paa.plano_assistencia_id,
			1 as tp_movimento_id,
			paa.usuario,
			getdate() as dt_inclusao,
			null  as dt_alteracao,
			0 as num_solicitacao
		from assistencia_db..proposta_assistencia_atual_tb paa with (nolock) --> ASSISTENCIA_DB
		join seguros_db..proposta_tb p  with (nolock)
			on paa.proposta_id = p.proposta_id
	join seguros_db..proposta_processo_susep_tb pps  with (nolock)
			on pps.proposta_id = paa.proposta_id
    join seguros_db..cliente_tb c  with (nolock)
			on c.cliente_id = p.prop_cliente_id
	join seguros_db..pessoa_juridica_tb pe with (nolock)
			on pe.pj_cliente_id = c.cliente_id
	join seguros_db..apolice_tb a with (nolock)
			on a.proposta_id = p.proposta_id
		   AND (a.dt_fim_vigencia IS NULL    
			OR (a.dt_fim_vigencia IS NOT NULL 
		   AND a.dt_fim_vigencia >= @dt_sistema)) -- Apolice Vigente        
	join seguros_db..proposta_fechada_tb pf with (nolock)
			on pf.proposta_id = p.proposta_id
	join assistencia_db..plano_assistencia_tb pa with (nolock)
		on pa.plano_assistencia_id = paa.plano_assistencia_id
	JOIN seguros_db..PROPOSTA_BASICA_TB WITH (NOLOCK)
		ON PROPOSTA_BASICA_TB.PROPOSTA_ID = PAA.PROPOSTA_ID
	where  p.produto_id =  1206
	 and paa.plano_assistencia_id in (3479, 3481)
	 and a.dt_fim_vigencia >= @dt_sistema
	 and p.situacao = 'i'	

	SET NOCOUNT OFF                      
	RETURN                      
                      
END TRY                                             
BEGIN CATCH

  DECLARE @ErrorMessage NVARCHAR(4000)
  DECLARE @ErrorSeverity INT
  DECLARE @ErrorState INT

  SELECT 
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE()

      -- Use RAISERROR inside the CATCH block to return error
      -- information about the original error that caused
      -- execution to jump to the CATCH block.
      RAISERROR (@ErrorMessage, -- Message text.
                 @ErrorSeverity, -- Severity.
                 @ErrorState -- State.
                 )

END CATCH  
  


